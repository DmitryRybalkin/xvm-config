﻿/**
 * Параметры альтернативного представления экрана загрузки боя
 * если в настойках игра стоит загрузка с миникартой
 * Конфиг: "dar"
 */
{
  "battleLoadingTips": {
    "darkenNotReadyIcon": false,
    "removeRankBadgeIcon": true,
    "removeVehicleLevel": true,
    "removeVehicleTypeIcon": true,
    "squadIconOffsetXLeft": -53,
    "squadIconOffsetXRight": -53,
    "nameFieldOffsetXLeft": 44,
    "nameFieldOffsetXRight": 46,
    "vehicleFieldOffsetXLeft": -17,
    "vehicleFieldOffsetXRight": -17,

    "vehicleFieldWidthDeltaLeft": 100,
    "vehicleFieldWidthDeltaRight": 0,

    "vehicleIconOffsetXLeft": -9,
    "vehicleIconOffsetXRight": -9,
    "formatLeftNick": "",
    "formatRightNick": "",
    "formatLeftVehicle": "",
    "formatRightVehicle": "",
    "extraFieldsLeft": [
      ${ "def.nickNameClanLeft" },
      ${ "def.statisticsLeft" },
      ${ "def.statisticsKeyLeft" },
      ${ "def.clanIcon" },
      ${ "def.vehicleName" },
      ${ "def.level" }
    ],
    "extraFieldsRight": [
      ${ "def.nickNameClanRight" },
      ${ "def.statisticsRight" },
      ${ "def.statisticsKeyRight" },
      ${ "def.clanIcon" },
      ${ "def.vehicleName" },
      ${ "def.level" }
    ]
  },
  
  "def": {
    "defaultItem": {
      "alpha": "{{alive?100|50}}", "bindToIcon": true,
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "textFormat": { "color": "#{{tk?00EAFF|{{player?FFB964|{{squad?FFB964|F4EFE8}}}}}}", "font": "$FieldFont", "size": 13 }
    },

    "rankBadgeIcon": {
      "$ref": { "path": "def.defaultItem" },
      "x": "{{xvm-stat?-456|-374}}", "y": -1, "width": 30, "height": 30,
      "format": "<img src='img://gui/maps/icons/library/badges/24x24/badge_{{rankBadgeId}}.png' width='24' height='24'>"
    },

    "flagIcon": {
      "$ref": { "path": "def.defaultItem" },
      "x": -404, "y": 4, "width": 24, "height": 24,
      "format": "<img src='xvm://res/icons/flags/{{flag}}.png' width='16' height='13'>"
    },

    "xvmUserIcon": {
      //"$ref": { "path": "def.defaultItem" },
      "x": -383, "y": 8, "width": 14, "height": 14,
      "format": "<img src='xvm://res/icons/xvm/xvm-user-{{xvm-user}}.png' width='8' height='8'>"
    },

    "nickNameClanLeft": {
      "$ref": { "path": "def.defaultItem" },
      "x": -220, "y": 2, "width": 200, "height": 24,
      "format": "<font face='$FieldFont' color='#E5E5E5' alpha='{{alive?{{ready?#FF|#FF}}|#80}}'><font size='14' color='{{player?#FFCC66|{{squad?#FFB964|{{tk?#00EAFF}}}}}}'>{{.texts.nicknames.{{name}}|{{name%.18s~..}}}}  </font><font size='12' color='{{topclan?#FFCC66|#E5D39A}}'>{{clannb}}</font></font>"
    },
    "nickNameClanRight": {
      "$ref": { "path": "def.nickNameClanLeft" },
      "format": "<font face='$FieldFont' color='#E5E5E5' alpha='{{alive?{{ready?#FF|#80}}|#80}}'><font size='12' color='{{topclan?#FFCC66|#E5D39A}}'>{{clannb}}</font><font size='13'>  {{.texts.nicknames.{{name}}|{{name%.18s~..}}}}</font></font>"
    },


    "statisticsLeft": {
      "$ref": { "path": "def.defaultItem" },
      "x": -255, 
      "y": 3, 
      "width": 250, 
      "height": 24, 
      "hotKeyCode": 56, 
      "onHold": true, 
      "visibleOnHotKey": false,
      "antiAliasType": "normal",
      "textFormat": { 
        "font": "Mono",
        "bold": true,
        "align": "right", 
        "color": "0xA6A6A6", 
        "size": "{{xvm-stat?14|0}}" 
      },
      "format": "<font color='{{c:xr}}'>{{r_size=2?{{r%s|--}}|{{r>9999?XXXX|{{r%4d|--}}}}}}</font> <font color='{{c:winrate}}'>{{winrate%2d|--}}%</font>"
    },

    "statisticsRight": {
      "$ref": { "path": "def.statisticsLeft" },
      "x": -254,
      "textFormat": { 
        "font": "Mono", 
        "align": "left", 
        "color": "0xA6A6A6", 
        "size": "{{xvm-stat?14|0}}" 
      },
      "format": "<font color='{{c:winrate}}'>{{winrate%2d|--}}%</font> <font color='{{c:xr}}'>{{r_size=2?{{r%s|--}}|{{r>9999?XXXX|{{r%4d|--}}}}}}</font>"
    },

    "statisticsKeyLeft": {
      "$ref": { "path": "def.statisticsLeft" },
      "visibleOnHotKey": true,
      "format": "<font color='{{c:t-winrate}}'>{{t-winrate%2d|--}}%</font>  <font color='{{c:kb}}'>{{kb>99?{{kb%3d}}|{{kb%2d|--}}k}}</font>  <font color='{{c:tdb}}'>{{tdb%4d|----}}</font>  <font color='{{c:avglvl}}'>{{avglvl%2d|--}}&nbsp;</font>"
    },

    "statisticsKeyRight": {
      "$ref": { "path": "def.statisticsKeyLeft" },
      "format": "<font color='{{c:avglvl}}'>{{avglvl%2d|--}}&nbsp;</font>  <font color='{{c:tdb}}'>{{tdb%4d|----}}</font>  <font color='{{c:kb}}'>{{kb>99?{{kb%3d}}|{{kb%2d|--}}k}}</font>  <font color='{{c:t-winrate}}'>{{t-winrate%2d|--}}%</font>"
    },










    // Название танка

    "vehicleName": {
      "bindToIcon": true,
      "x": "{{ally?4|-29}}",
      "y": 1, 
      "width": 105, 
      "height": 24,
      "antiAliasType": "normal",
      "alpha": "{{alive?90|30}}",
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 13,
        "color": "0xFFFFFF",
        "align": "left",
        "valign": "center"
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{vehicle}}"
    },

    // Уровень техники
    "level": {
      "bindToIcon": true,
      "x": "{{ally?78|78}}",
      "y": -5, 
      "width": 24, 
      "height": 24,
      "alpha": "{{alive?100|30}}",
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 11,
        "color": "{{level=10?0xFF00FE|{{level=9?0x197AFF|{{level=8?0x01FFCD|{{level=7?0x65FF00|{{level=6?0xFEFF01|0xFE0000}}}}}}}}}}",
        "align": "{{ally?left|right}}",
        "valign": "center"
      },
      "format": "{{level}}"
    },


    "vehicleType": {
      "$ref": { "path": "def.defaultItem" },
      "x": -19, "y": -1, "width": 24, "height": 24, "alpha": "{{alive?80|40}}",
      "format": "<font face='xvm' size='21'>{{vtype}}</font>"
    },

    "clanIcon": {
      "$ref": { "path": "def.defaultItem" },
      "x": "{{ally?-263|-263}}", 
      "y": -4, 
      "width": 30, 
      "height": 30,
      "format": "{{topclan}}"
    }
  }
}