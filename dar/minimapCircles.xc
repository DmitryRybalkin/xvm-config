﻿/**
 * Круги на миникарте.
 * Конфиг: H_E_K_P_O_M_A_H_T_
 */
{
 "circles": {
    "view": [
        /** Окружность границы максимальной отрисовки юнитов. */
        { "enabled": "{{my-vtype-key=SPG?false|true}}", "distance": 564, "scale": 1, "thickness": 0.7, "alpha": 40, "color": "0xFFFFFF" },
        /** Основные круги: */
        { "enabled": true,  "distance": "blindarea", "scale": 1, "thickness": 0.75, "alpha": 80, "color": "0x3EB5F1" },
        { "enabled": true,  "distance": 445,         "scale": 1, "thickness": 1.1,  "alpha": 45, "color": "0xFFCC66" },
        /** Дополнительные круги: */
        { "enabled": true,  "distance": 50,          "scale": 1, "thickness": 0.75, "alpha": 60, "color": "0xFFFFFF" },
        { "enabled": false, "distance": "standing",  "scale": 1, "thickness": 1.0,  "alpha": 60, "color": "0xFF0000" },
        { "enabled": false, "distance": "motion",    "scale": 1, "thickness": 1.0,  "alpha": 60, "color": "0x0000FF" },
        { "enabled": false, "distance": "dynamic",   "scale": 1, "thickness": 1.0,  "alpha": 60, "color": "0x3EB5F1" }
    ],
    /** Максимальная дальность стрельбы для артиллерии. */
    "artillery": { "enabled": true, "alpha": 55, "color": "0xFF6666", "thickness": 0.5 },
    /** Максимальная дальность полета снаряда для пулеметных танков. */
    "shell": { "enabled": true, "alpha": 55, "color": "0xFF6666", "thickness": 0.5 },
    /** Специальные круги, зависящие от модели техники. */
    "special": [ ]
    }
}