﻿/**
 * Параметры экрана логина
 * Конфиг: "dar"
 */
{
  "login": {
    "saveLastServer": true,
    "autologin": true,
    "confirmOldReplays": true,
    "disabledServers": [ ],
    "pingServers": {
      "enabled": true, 
      "updateInterval": 10000, 
      "x": 0, 
      "y": -112, 
      "hAlign": "center", 
      "vAlign": "bottom",
      "alpha": 90, 
      "bgImage": null, 
      "delimiter": ":\n", 
      "maxRows": 2, 
      "columnGap": 6, 
      "leading": 0,
      "showTitle": false, 
      "showServerName": true,
       "minimalNameLength": 0, 
       "minimalValueLength": 0, 
       "errorString": "", 
       "ignoredServers": [ ],
      "fontStyle": { 
        "name": "$TextFont",
        "size": 12, 
        "color": { "great": "0xFFCC66", "good": "0x8C8C7E", "poor": "0x8C8C7E", "bad": "0xD64D4D" }, 
        "serverColor": "0x8C8C7E" },
      "threshold": { "great": 35, "good": 60, "poor": 100 },
      "shadow": { "enabled": false }
    },
    "onlineServers": {
      "$ref": { "path": "login.pingServers" }, 
      "y": -97, 
      "leading": 15, 
      "threshold": { "great": 30000, "good": 10000, "poor": 3000 }
    },
    "widgets": ${ "widgets.xc": "widgets.login" }
  }
}