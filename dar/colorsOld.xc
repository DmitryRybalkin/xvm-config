﻿/**
 * Настройки цветов
 * Конфиг: "dar"
#F7BD60     голда 

#96FF00     союзник
#FFB964     взводный
#00EAFF     тимкиллер
#F50800     противник
#FFB01B     игрок
#1dca00     squadman color     / цвет урона совзводного	

#FF4B40     very bad  / очень плохо
#FF9B42     bad       / плохо
#FAF740     normal    / средне
#88FF40     good      / хорошо
#42D7C6     very good / очень хорошо
#DC72F6     unique    / уникально

 */
{
  "def": {
    "al": "0x96FF00",                 // ally       / союзник
    "sq": "0xFFB964",                 // squadman   / взводный
    "tk": "0x00EAFF",                 // teamKiller / тимкиллер
    "en": "0xF50800",                 // enemy      / противник
    "pl": "0xFFB01B",                 // player     / игрок
    "sqc": "0x1dca00",				        // squadman color     / цвет урона совзводного	

    // Динамический цвет по различным статистическим показателям.
    "colorRating": {
      "very_bad":     "0xFF4B40",     // very bad  / очень плохо
      "bad":          "0xFF9B42",     // bad       / плохо
      "normal":       "0xFAF740",     // normal    / средне
      "good":         "0x88FF40",     // good      / хорошо
      "very_good":    "0x42D7C6",     // very good / очень хорошо
      "unique":       "0xDC72F6"      // unique    / уникально
    },
    // Dynamic color by remaining health points.
    // Динамический цвет по оставшемуся запасу прочности.
    "colorHP": {
      "very_low":         "0xFF0000", // very low      / очень низкий
      "low":              "0xDD4444", // low           / низкий
      "average":          "0xFFCC22", // average       / средний
      "above_average":    "0xFCFCFC"  // above-average / выше среднего
    }
  },

  "colors": {
    "damage": {
      "ally_ally_hit": "0x00EAFF",
      "ally_ally_kill": "0x065E65",
      "ally_ally_blowup": "0x065E67",
      "ally_squadman_hit": "0x00EAFF",
      "ally_squadman_kill": "0x00EAFF",
      "ally_squadman_blowup": "0x0FEBFF",
      "ally_enemy_hit": "0xFFE5E5",
      "ally_enemy_kill": "0x620300",
      "ally_enemy_blowup": "0x620301",
      "ally_allytk_hit": "0x00EAFF",
      "ally_allytk_kill": "0x065E68",
      "ally_allytk_blowup": "0x065E69",
      "ally_enemytk_hit": "0xE5E5E5",
      "ally_enemytk_kill": "0x620300",
      "ally_enemytk_blowup": "0x620301",
      "squadman_ally_hit": "0x00EAFF",
      "squadman_ally_kill": "0x00E8FF",
      "squadman_ally_blowup": "0x00E9FF",
      "squadman_squadman_hit": "0x00EAFF",
      "squadman_squadman_kill": "0x055860",
      "squadman_squadman_blowup": "0x055861",
      "squadman_enemy_hit": "0xFFB964",
      "squadman_enemy_kill": "0x4C371E",
      "squadman_enemy_blowup": "0x4E371D",
      "squadman_allytk_hit": "0xFFB964",
      "squadman_allytk_kill": "0x4FEBFF",
      "squadman_allytk_blowup": "0x5FEBFF",
      "squadman_enemytk_hit": "0xFFB964",
      "squadman_enemytk_kill": "0x4C371E",
      "squadman_enemytk_blowup": "0x4E371D",
      "enemy_ally_hit": "0xE5E5E5",
      "enemy_ally_kill": "0x2D4C00",
      "enemy_ally_blowup": "0x2D4C01",
      "enemy_squadman_hit": "0xE5E5E5",
      "enemy_squadman_kill": "0x4E381D",
      "enemy_squadman_blowup": "0x4E382D",
      "enemy_enemy_hit": "0x00EAFF",
      "enemy_enemy_kill": "0x035B60",
      "enemy_enemy_blowup": "0x035B61",
      "enemy_allytk_hit": "0xE5E5E5",
      "enemy_allytk_kill": "0x02444A",
      "enemy_allytk_blowup": "0x03444A",
      "enemy_enemytk_hit": "0x00EAFF",
      "enemy_enemytk_kill": "0x035B60",
      "enemy_enemytk_blowup": "0x035B61",
      "unknown_ally_hit": "0xE5E5E5",
      "unknown_ally_kill": "0x2D4C00",
      "unknown_ally_blowup": "0x2D4C01",
      "unknown_squadman_hit": "0xE5E5E5",
      "unknown_squadman_kill": "0x4E381D",
      "unknown_squadman_blowup": "0x4E382D",
      "unknown_enemy_hit": "0xE5E5E5",
      "unknown_enemy_kill": "0x620300",
      "unknown_enemy_blowup": "0x620301",
      "unknown_allytk_hit": "0xE5E5E5",
      "unknown_allytk_kill": "0x02444A",
      "unknown_allytk_blowup": "0x03444A",
      "unknown_enemytk_hit": "0xE5E5E5",
      "unknown_enemytk_kill": "0x620300",
      "unknown_enemytk_blowup": "0x620301",
      "player_ally_hit": "0x00EAFF",
      "player_ally_kill": "0x00E8FF",
      "player_ally_blowup": "0x00E9FF",
      "player_squadman_hit": "0x00EAFF",
      "player_squadman_kill": "0x00E8FF",
      "player_squadman_blowup": "0x00E9FF",
      "player_enemy_hit": "0xFFB01B",
      "player_enemy_kill": "0xFFB01B",
      "player_enemy_blowup": "0xFFB01C",
      "player_allytk_hit": "0xFFB01B",
      "player_allytk_kill": "0x4FEBFF",
      "player_allytk_blowup": "0x5FEBFF",
      "player_enemytk_hit": "0xFFB01B",
      "player_enemytk_kill": "0xFFB01B",
      "player_enemytk_blowup": "0xFFB01C"
      /* "ally_ally_hit":              ${"def.tk"},
      "ally_ally_kill":             ${"def.tk"},
      "ally_ally_blowup":           ${"def.tk"},
      "ally_squadman_hit":          ${"def.tk"},
      "ally_squadman_kill":         ${"def.tk"},
      "ally_squadman_blowup":       ${"def.tk"},
      "ally_enemy_hit":             ${"def.en"},
      "ally_enemy_kill":            ${"def.en"},
      "ally_enemy_blowup":          ${"def.en"},
      "ally_allytk_hit":            ${"def.tk"},
      "ally_allytk_kill":           ${"def.tk"},
      "ally_allytk_blowup":         ${"def.tk"},
      "ally_enemytk_hit":           ${"def.en"},
      "ally_enemytk_kill":          ${"def.en"},
      "ally_enemytk_blowup":        ${"def.en"},
      "enemy_ally_hit":             ${"def.al"},
      "enemy_ally_kill":            ${"def.al"},
      "enemy_ally_blowup":          ${"def.al"},
      "enemy_squadman_hit":         ${"def.al"},
      "enemy_squadman_kill":        ${"def.al"},
      "enemy_squadman_blowup":      ${"def.al"},
      "enemy_enemy_hit":            ${"def.en"},
      "enemy_enemy_kill":           ${"def.en"},
      "enemy_enemy_blowup":         ${"def.en"},
      "enemy_allytk_hit":           ${"def.al"},
      "enemy_allytk_kill":          ${"def.al"},
      "enemy_allytk_blowup":        ${"def.al"},
      "enemy_enemytk_hit":          ${"def.en"},
      "enemy_enemytk_kill":         ${"def.en"},
      "enemy_enemytk_blowup":       ${"def.en"},
      "unknown_ally_hit":           ${"def.al"},
      "unknown_ally_kill":          ${"def.al"},
      "unknown_ally_blowup":        ${"def.al"},
      "unknown_squadman_hit":       ${"def.al"},
      "unknown_squadman_kill":      ${"def.al"},
      "unknown_squadman_blowup":    ${"def.al"},
      "unknown_enemy_hit":          ${"def.en"},
      "unknown_enemy_kill":         ${"def.en"},
      "unknown_enemy_blowup":       ${"def.en"},
      "unknown_allytk_hit":         ${"def.al"},
      "unknown_allytk_kill":        ${"def.al"},
      "unknown_allytk_blowup":      ${"def.al"},
      "unknown_enemytk_hit":        ${"def.en"},
      "unknown_enemytk_kill":       ${"def.en"},
      "unknown_enemytk_blowup":     ${"def.en"},
      "squadman_ally_hit":          ${"def.sq"},
      "squadman_ally_kill":         ${"def.sq"},
      "squadman_ally_blowup":       ${"def.sq"},
      "squadman_squadman_hit":      ${"def.sq"},
      "squadman_squadman_kill":     ${"def.sq"},
      "squadman_squadman_blowup":   ${"def.sq"},
      "squadman_enemy_hit":         ${"def.sq"},
      "squadman_enemy_kill":        ${"def.sq"},
      "squadman_enemy_blowup":      ${"def.sq"},
      "squadman_allytk_hit":        ${"def.sq"},
      "squadman_allytk_kill":       ${"def.sq"},
      "squadman_allytk_blowup":     ${"def.sq"},
      "squadman_enemytk_hit":       ${"def.sq"},
      "squadman_enemytk_kill":      ${"def.sq"},
      "squadman_enemytk_blowup":    ${"def.sq"},
      "player_ally_hit":            ${"def.pl"},
      "player_ally_kill":           ${"def.pl"},
      "player_ally_blowup":         ${"def.pl"},
      "player_squadman_hit":        ${"def.pl"},
      "player_squadman_kill":       ${"def.pl"},
      "player_squadman_blowup":     ${"def.pl"},
      "player_enemy_hit":           ${"def.pl"},
      "player_enemy_kill":          ${"def.pl"},
      "player_enemy_blowup":        ${"def.pl"},
      "player_allytk_hit":          ${"def.pl"},
      "player_allytk_kill":         ${"def.pl"},
      "player_allytk_blowup":       ${"def.pl"},
      "player_enemytk_hit":         ${"def.pl"},
      "player_enemytk_kill":        ${"def.pl"},
      "player_enemytk_blowup":      ${"def.pl"} */
    },
    "dmg_kind": {
      "shot": "0xF4EFE2",
      "fire": "0xF4EFE4",
      "ramming": "0xF4EFE2",
      "world_collision": "0xF4EFE8",
      "drowning": "0xF4EFE8",
      "other": "0xF4EFE8"
    },
    "spotted": {
      "neverSeen": "0x000000",
      "lost": "0xc35e5e",
      "spotted": "0xc35e5e",
      "dead": "0xFFFFFF",
      "neverSeen_arty": "0x000000",
      "lost_arty": "0xc35e5e",
      "spotted_arty": "0xc35e5e",
      "dead_arty": "0xFFFFFF"
    },

    "system": {
      "ally_alive": "0x96FF00",
      "ally_dead": "0x96FF00",
      "ally_blowedup": "0x2D4C02",
      "squadman_alive": "0xFFB964",
      "squadman_dead": "0x50382B",
      "squadman_blowedup": "0x50382B",
      "teamKiller_alive": "0x00EAFF",
      "teamKiller_dead": "0x065E64",
      "teamKiller_blowedup": "0x065E64",
      "enemy_alive": "0xF50800",
      "enemy_dead": "0xF50800",
      "enemy_blowedup": "0x620302"
    },

    "vtype": {
      "LT": "0xA2FF9A",
      "MT": "0xFFF198",
      "HT": "0xF4EFE8",
      "SPG": "0xEFAEFF",
      "TD": "0xA0CFFF",
      "premium": "0xFFCC66",
      "usePremiumColor": false
    },

    "winrate": [
      { "value": 46.49, "color": ${"def.colorRating.very_bad" } }, //  0   - 46.5  - very bad  (20% of players)
      { "value": 48.49, "color": ${"def.colorRating.bad"      } }, // 46.5 - 48.5  - bad       (better than 20% of players)
      { "value": 52.49, "color": ${"def.colorRating.normal"   } }, // 48.5 - 52.5  - normal    (better than 60% of players)
      { "value": 57.49, "color": ${"def.colorRating.good"     } }, // 52.5 - 57.5  - good      (better than 90% of players)
      { "value": 63.49, "color": ${"def.colorRating.very_good"} }, // 57.5 - 63.5  - very good (better than 99% of players)
      { "value": 100,   "color": ${"def.colorRating.unique"   } }  // 63.5 - 100   - unique    (better than 99.9% of players)
    ],

    "kb": [
      { "value": 2,   "color": ${"def.colorRating.very_bad" } },  //  0 - 2
      { "value": 6,   "color": ${"def.colorRating.bad"      } },  //  3 - 6
      { "value": 16,  "color": ${"def.colorRating.normal"   } },  //  7 - 16
      { "value": 30,  "color": ${"def.colorRating.good"     } },  // 17 - 30
      { "value": 43,  "color": ${"def.colorRating.very_good"} },  // 31 - 43
      { "value": 999, "color": ${"def.colorRating.unique"   } }   // 44 - *
    ],

    "damageRating": [
      { "value": 64.99, "color": ${"def.colorRating.very_bad"} }, // 0-64.99
      { "value": 84.99, "color": ${"def.colorRating.normal"  } }, // 65-84.99
      { "value": 94.99, "color": ${"def.colorRating.good"    } }, // 85-94.99
      { "value": 100,   "color": ${"def.colorRating.unique"  } }  // 95-*
    ],

    // Динамический цвет по шкале XVM.
    // https://kr.cm/f/t/2625/
    "x": [
      { "value": 16.4, "color": ${"def.colorRating.very_bad" } }, // 00 - 16 - very bad  (20% of players)
      { "value": 33.4, "color": ${"def.colorRating.bad"      } }, // 17 - 33 - bad       (better than 20% of players)
      { "value": 52.4, "color": ${"def.colorRating.normal"   } }, // 34 - 52 - normal    (better than 60% of players)
      { "value": 75.4, "color": ${"def.colorRating.good"     } }, // 53 - 75 - good      (better than 90% of players)
      { "value": 92.4, "color": ${"def.colorRating.very_good"} }, // 76 - 92 - very good (better than 99% of players)
      { "value": 999,  "color": ${"def.colorRating.unique"   } }  // 93 - XX - unique    (better than 99.9% of players)
    ]
  }
}