﻿#####################################################################
# imports

from xfw import overrideMethod
from xvm_main.python.xvm import l10n
import xvm_main.python.config as config
from gui.Scaleform.daapi.view.meta.NotificationsListMeta import NotificationsListMeta

##

def _getL10n(text):
    if text.find('{{l10n:') > -1:
        return l10n(text)
    return text

#####################################################################
# handlers

def hideMessagePatterns(message):
    for element in config.get('hangar/hideMessagePatterns', []):
        element = _getL10n(element)
        element = element.decode('utf-8').lower().encode('utf-8')
        if message.decode('utf-8').lower().encode('utf-8').find(element) != -1:
            return False
        else:
            continue
    return True

@overrideMethod(NotificationsListMeta, 'as_setMessagesListS')
def as_setMessagesListS(base, self, value):
    if len(config.get('hangar/hideMessagePatterns', [])):
        formed = []
        for item in value['messages']:
            text = item['message']['message']
            if hideMessagePatterns(text):
                formed.append(item)
                value['messages'] = formed
    base(self, value)