﻿/**
 * Настройки графических элементов
 * Конфиг: "dar"
 */
 /**
 * GUI elements settings (experts only)
 * Настройки графических элементов (только для экспертов!)
 * https://kr.cm/f/t/1761/
 *
 * TODO: doc
 *
 * commands:
 *   "$log": 1,              // команда $log используется для вывода значений в лог, число - уровень вложенности
 *   "$delay": 1,            // запуск сниппета с задержкой, число - задержка в мсек
 *   "$interval": 1,         // периодический запуск сниппета с интервалом, число - интервал в мсек
 *   "$textFormat": {        // подстановка для формата текста
 *     //"$log": 1,          // тоже можно логгировать
 *     "size": 30,           // размер шрифта
 *     "font": "$TitleFont", // шрифт пожирнее
 *     "align": "center"     // выравнивание
 *   }
 *   playerMessangersPanel - сообщения кто кого уничтожил, справа над картой
 */
  // Interface elements.
  // Элементы интерфейса.
{
  "elements": {
    "battleTimer":        { "$delay": 100, "alpha": 0 },
    "debugPanel":         { "$delay": 100, "alpha": 0 },
    "endWarningPanel":    { "$interval": 100, "y": 53 },
    "fragCorrelationBar": { "$delay": 100, "alpha": 0 },
    "prebattleTimer":     { "$delay": 100, "alpha": "0" },

    // Панель: Информирование в бою - Прогресс ЛБЗ
    "questProgressTopAnimContainer": { "$interval": 100, "y": 96 },

    // "questProgressTopView" - Панель: Информирование в бою - Прогресс ЛБЗ (анимация)
    "questProgressTopView": {
      "$interval": 100,
      "x": "{{battletype-key=epic_random?{{.options.fragCorShowMarkers=true?{{py:xvm.screenHCenter}}|{{py:xvm.screenWidth<1650?{{py:xvm.screenHCenter}}|{{py:sub({{py:xvm.screenWidth}},368)}}}}}}|{{pp.mode=4?{{py:xvm.screenWidth<1900?{{py:xvm.screenHCenter}}|{{py:sub({{py:xvm.screenWidth}},493)}}}}|{{py:xvm.screenWidth<1650?{{py:xvm.screenHCenter}}|{{py:sub({{py:xvm.screenWidth}},468)}}}}}}}}",
      "y": "{{battletype-key=epic_random?{{.options.fragCorShowMarkers=true?104|{{py:xvm.screenWidth<1650?104|39}}}}|{{pp.mode=4?{{py:xvm.screenWidth<1900?92|39}}|{{py:xvm.screenWidth<1650?92|39}}}}}}" },

    /* // Панель: Информирование в бою - Прогресс ЛБЗ
    "questProgressTopAnimContainer": { "$interval": 100, "y": 96 },

    // "questProgressTopView" - Панель: Информирование в бою - Прогресс ЛБЗ (анимация)
    "questProgressTopView": {
      "$interval": 100,

      "x":"{{battletype-key=epic_random?{{py:xvm.screenWidth<1650?{{py:xvm.screenHCenter}}|{{py:sub({py:xvm.screenWidth}},368)}}}}|{{pp.mode=4?{{py:xvm.screenWidth<1900?{{py:xvm.screenHCenter}}|{{py:sub({{py:xvm.screenWidth}},93)}}}}|{{py:xvm.screenWidth<1650?{{py:xvm.screenHCenter}}|{{py:sub({{py:xvm.screenWidth}},368)}}}}}}}}",

      "y": "{{battletype-key=epic_random?{{.options.fragCorShowMarkers=true?104|{{py:xvm.screenWidth<1650?104|9}}}}|{{pp.mode=4?{{py:xvm.screenWidth<1900?92|39}}|{{py:xvm.screenWidth<1650?92|39}}}}}}"
    }, */

    // Ленты боевой эффективности
    "ribbonsPanel": {
      "$interval": 1000,
      "x": "{{py:sum({{py:xvm.screenHCenter}},-31)}}",
      "y": "{{py:sum({{py:xvm.screenVCenter}},91)}}"
    }
  }
}