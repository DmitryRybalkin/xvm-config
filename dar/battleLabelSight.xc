// Прицел (без сведения)

{
  "def": {
    "defaultItem": {
      "antiAliasType": "normal",
      "alpha": "100",
      "x": 0,
      "y": 0,
      "width": 100,
      "height": 40,
      "screenHAlign": "center",
      "screenVAlign": "center",
      "shadow": {
        "color": "0xFFFFE3",
        "alpha": 70,
        "blur": 2,
        "strength": 2,
        "distance": 0,
        "angle": 0 },
      "textFormat": {
        "font": "$FieldFont",
        "size": 18,
        "bold": "true",
        "align": "center",
        "valign": "top",
        "color": "0xF8BC60"
      }
    },

    // Сетка прицела
    "grid": {
      "enabled": true,
      "updateEvent": "PY(ON_PLAYER_HEALTH), PY(ON_AIM_MODE), PY(ON_MARKER_POSITION), PY(ON_TARGET), PY(ON_CALC_ARMOR)",
      "x": "-2",
      "y": "{{py:aim.y(-2)}}",
      "width": "{{py:aim.mode=str?1801|621}}",
      "height": "{{py:aim.mode=str?1023|261}}",
      //"borderColor": "0xFF0000", //!!!
      "screenHAlign": "center",
      "screenVAlign": "center",
      "format": "<img src='cfg://dar/img/aim/{{py:aim.mode=str?spg|{{py:sight.c_piercingChance}}}}aim.png' width='{{py:aim.mode=str?1801|621}}' height='{{py:aim.mode=str?1023|261}}'>"
    },

    // Дальность полета снаряда (в метрах).
    "distance": {
      "$ref": { "path": "def.defaultItem" },
      "updateEvent": "PY(ON_AIM_MODE), PY(ON_TARGET), PY(ON_MARKER_POSITION)",
      "y": "{{py:aim.y(-143)}}",
      "alpha": "100",
      "textFormat": {
        "font": "$FieldFont",
        "size": 18,
        "bold": "true",
        "align": "center",
        "valign": "bottom"
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "<font color='{{py:sight.distanceTarget?{{py:sight.distanceTarget>{{py:sight.visionRadiusTarget}}?#C7FF1F|#FFFFE3}}|#FFFFE3}}'>{{py:isBattle?{{alive?{{py:aim.mode=arc?{{py:sight.distanceTarget%.0f}}|{{py:sight.distance%.0f}}}}}}}}</font>"
    },

    // Время полета снарядов
    "timeFlight": {
      "$ref": { "path": "def.defaultItem" },
      "updateEvent": "PY(ON_AIM_MODE), PY(ON_TARGET), PY(ON_MARKER_POSITION)",
      "y": "{{py:aim.y(-109)}}",
      "alpha": "100",
      "textFormat": {
        "font": "$FieldFont",
        "size": 18,
        "bold": "true",
        "align": "center",
        "valign": "bottom"
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "<font color='#FFFFE3'>{{py:isBattle?{{alive?{{py:aim.mode=str?{{py:sight.timeFlight%.1f}} c}}}}}}</font>"
    },

    // Время сведения
    "timeAIM": {
      "$ref": { "path": "def.defaultItem" },
      "updateEvent": "PY(ON_AIM_MODE), PY(ON_TARGET), PY(ON_MARKER_POSITION)",
      "x": "115",
      "y": "{{py:aim.y(-8)}}",
      "alpha": "100",
      "textFormat": {
        "font": "$FieldFont",
        "size": 15,
        "bold": "true",
        "align": "left",
        "valign": "top"
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "<font color='#FFFFE3'>{{py:isBattle?{{alive?{{py:aim.mode=str?{{py:sight.timeAIM%3.1f}} c}}}}}}</font>"
    },

    // Свои бронепробиваемость и урон
    "damage": {
      "$ref": { "path": "def.defaultItem" },
      "enabled": true,
      "flags": [ "alive" ],
      "alpha": "{{py:isBattle=battle?100|0}}",
      "updateEvent": "PY(ON_BEGIN_BATTLE), PY(ON_AIM_MODE), PY(ON_RELOAD)",
      "x": "245",
      "y": "{{py:aim.y(-8)}}",
      "textFormat": {
        "font": "$FieldFont",
        "size": 15,
        "bold": "true",
        "align": "right",
        "valign": "top",
        "color": "{{py:sight.goldShell?0xFFCC66|0xFFFFE3}}"
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{py:sight.shellType}}, {{py:sight.piercingShell}}мм / {{py:sight.damageShell}}"
    },

    // Количество ХП
    "myHp": {
            "$ref": { "path": "def.defaultItem" },
            "enabled": true,
            //"flags": [ "alive" ],
            "alpha": "{{py:isBattle=battle?100|0}}",
            "updateEvent": "PY(ON_MY_HP), PY(ON_MARKER_POSITION)",
            "x": "-244",
            "y": "{{py:aim.y(26)}}",
            "textFormat": {
              "font": "$FieldFont",
              "size": 15,
              "bold": "true",
              "align": "left",
              "valign": "top",
              "color": "0xFFFFE3"
            },
            "shadow": ${ "@xvm.xc": "mainShadow" },
            "format": "{{py:my_hp.health}}"
          },

    // Полоса ХП
    "myHpBar": {
      "enabled": true,
      "updateEvent": "PY(ON_PLAYER_HEALTH), PY(ON_AIM_MODE), PY(ON_MARKER_POSITION)",
      "x": -1020,
      "y": "{{py:aim.y(0)}}",
      "screenHAlign": "right",
      "screenVAlign": "center",
      "alpha": 100,
      "width": "{{py:my_hp.health(239)}}",
      /* "width": "{{py:isBattle=battle?{{py:sight.leftTime(239}}|239}}", */
      "height": 5,
      /* "borderColor": "0xFFFFE3", //!!! */
      //"borderColor": "{{py:sight.c_piercingChance?py:sight.c_piercingChance|0xc0f789}}", //!!!

      "bgColor": "#659A03"
    },

    // Приведенная броня в точке прицеливания
    "penetration": {
      "enabled": true,
      "updateEvent": "PY(ON_PLAYER_HEALTH), PY(ON_AIM_MODE), PY(ON_MARKER_POSITION), PY(ON_TARGET), PY(ON_CALC_ARMOR)",
      "x": -115,
      "y": "{{py:aim.y(23)}}",
      "width": 100,
      "height": 40,
      //"borderColor": "0xFF0000", //!!!
      "screenHAlign": "center",
      "screenVAlign": "center",
      "alpha" : "{{py:sight.armorActual?100|0}}",
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "textFormat": { "font": "$FieldFont", "align": "right", "color": "0xd4ffaa" },
      "format": "<font color='{{py:sight.c_piercingChance}}' size='22'>{{py:sight.armorActual}}</font>"
    },

    // Перезарядка осталось
    "reloadTime": {
      "$ref": { "path": "def.defaultItem" },
      "flags": [ "alive" ],
      "updateEvent": "PY(ON_RELOAD), PY(ON_AIM_MODE), PY(ON_MARKER_POSITION)",
      "x": "115",
      "y": "{{py:aim.y(23)}}",
      //"borderColor": "0xFF0000", //!!!
      "textFormat": {
        "font": "$FieldFont",
        "size": 22,
        "bold": "true",
        "align": "left",
        "valign": "top",
        "color": "#FB0100"
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{py:sight.leftTime=0?{{py:sight.quantityInClipShells>0?<font color-'#FFFFE3'>{{py:sight.reloadTimeClip%3.1f}}</font>}}|{{py:sight.leftTime%3.1f}}}}"
    },

    // Полное время перезарядки (в секундах).
    "baseTimeReload": {
      "$ref": { "path": "def.defaultItem" },
      "flags": [ "alive" ],
      "updateEvent": "PY(ON_BEGIN_BATTLE), PY(ON_AIM_MODE), PY(ON_MARKER_POSITION), PY(ON_TARGET), PY(ON_CALC_ARMOR)",
      "x": "246",
      "y": "{{py:aim.y(26)}}",
      "textFormat": {
        "font": "$FieldFont",
        "size": 15,
        "bold": "true",
        "align": "right",
        "valign": "top",
        "color": "0xFFFFE3"
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{py:sight.reloadTime%3.1f}}"
    },

    // Полоса перезарядки
    "reloadBar": {
      "enabled": true,
      "updateEvent": "PY(ON_PLAYER_HEALTH), PY(ON_AIM_MODE), PY(ON_RELOAD)",
      "x": 1021,
      "y": "{{py:aim.y(0)}}",
      "screenHAlign": "left",
      "screenVAlign": "center",
      "alpha": 100,
      "width": "{{py:isBattle=battle?{{py:sub(239,{{py:sight.leftTime(239)}})}}|239}}",
      "height": 5,
      //"borderColor": "{{py:sight.c_piercingChance!=''?{{py:sight.c_piercingChance}}|#c0f789}}", //!!!
      "bgColor": "#FB0100"
    },

    // Снаряды в барабане, пока не доделаны и не подключены
    "clipReload": {
      "enabled": true,
      "updateEvent": "PY(ON_RELOAD)",
      "x": "45",
      "y": "{{py:aim.y(38)}}",
      "width": 74,
      "height": 21,
      //"borderColor": "0xFF0000", //!!!
      "screenHAlign": "center",
      "screenVAlign": "center",
      "format": "<img src='cfg://dar/img/reload/{{py:sight.quantityInClipShells}}-{{py:sight.quantityInClipShellsMax}}.png' width='72' height='19'>"
    }
  }
}