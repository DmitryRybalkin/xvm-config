{
  "sight": {
    // true - включиь моды
    // false - отключить моды
    "enabled": true,
    // время отображения макросов {{py:....Target}} после потери цели
    "delayHideTarget": 1,
    // Сфера разлета осколков осколочно-фугасных снарядов
    "sphereDispersion": {
      // true - включить
      "enabled": true,
      // Для каких типов техники будет отображаться сфера. Возможные значения ["LT", "MT", "HT", "TD", "SPG"]
      "vtype": ["SPG"],
      // отображение сферы по нажатию клавиши
      "hotkey": { 
        // true - включить
        "enabled": true,
        // коды клавиш можно посмотреть в файле hotkeys.xc.
        "keyCode": 29,
        // onHold: false - произвести действие по разовому нажатию клавиши. true - по удержанию.
        "onHold": true }
    },
    // true - не отображать прицел после смерти
    "hideSightAfterDeath": false,    
    // true - не отображать сетку прицела
    "removeIndicator": false,
    // true - не отображать центральный маркер
    "removeCentralMarker": false,
    // true - не отображать полосу перезарядки
    "removeLoad": false,
    // true - не отображать полосу прочности 
    "removeCondition": false,   
    // true - не отображать кассеты 
    "removeContainers": false,
    // true - не отображать таймер перезарядки 
    "removeLoadingTimer": false,
    // true - не отображать индикатор кратности прицела 
    "removeZoomIndicator": false,
    // true - не отображать дистанцию до цели 
    "removeDistance": true,
    // true - не отображать количество снарядов
    "removeQuantityShells": false,
    // опция управляет отображением спидометра (значение по умолчанию - "wheels")
    //"none" - скрыть для любой техники, "all" - показывать для любой техники, "wheels" - показывать для колёсной техники
    "showSpeedometer": "wheels",
    // Тип снаряда (макрос {{py:sight.shellType}}).
    "shellType": {
      "armor_piercing":    "{{l10n:armor_piercing}}",    // armor piercing / бронебойный
      "high_explosive":    "{{l10n:high_explosive}}",    // high explosive / осколочно-фугасный
      "armor_piercing_cr": "{{l10n:armor_piercing_cr}}", // armor piercing composite rigid / бронебойный подкалиберный
      "armor_piercing_he": "{{l10n:armor_piercing_he}}", // armor piercing high explosive / бронебойно-фугасный
      "hollow_charge":     "{{l10n:hollow_charge}}",     // high explosive anti-tank / кумулятивный
      "not_shell":         "Netu"
    },
    //цвет шанса на пробитие ({{py:sight.c_piercingChance}})
    "c_piercingChance": {
      "not_pierced":    "#F70000",
      "little_pierced": "#F77500",
      "great_pierced":  "#3FF301",
      /* "not_pierced":    "#F50800",
      "little_pierced": "#FFB01B",
      "great_pierced":  "#96FF00", */
      "not_target":     ""
    },
    // Выделение цели находящейся в автозахвате
    "autoAim": {
      "enabled": false,
      // Способ выделения техники. Возможные значения: "arrow", "cylinder"
      // "arrow"    - стрелка над техникой 
      // "cylinder" - свечение вокруг техники
      // ""         - отключить выделение техники
      "markerType": "cylinder",
      // опция управляет отображением перекрестия при захвате цели (значение по умолчанию - "wheels")
      //"none" - скрыть для любой техники, "all" - показывать для любой техники, "wheels" - показывать для колёсной техники      
      "showAutoAimMarker": "wheels"
    }
  }
}