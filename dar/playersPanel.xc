﻿/**
 * Параметры панелей игроков ("ушей").
 */
{
  // Параметры панелей игроков ("ушей").
  "playersPanel": {
    "enabled": true,
    "alpha": 250,
    "iconAlpha": 100,
    "expandAreaWidth": 0,
    "removeSelectedBackground": true,
    "removePanelsModeSwitcher": true,
    "startMode": "medium",
    "altMode": "medium2",
    "none": { "enabled": true },
    "large": { "enabled": false },

    "short": {
      // false - disable (отключить)
      "enabled": true,
      // Отображаемые стандартные поля в данном режиме, и их порядок.
      // Допустимые названия: "frags", "badge", "nick", "vehicle".
      "standardFields": [ "frags" ],

      // Ширина области переключения в режим "large" при наведении мыши.
      "expandAreaWidth": 230,

      "removeSquadIcon": false,
      "squadIconAlpha": 100,

      // Смещение координаты X для иконки танка.
      "vehicleIconOffsetXLeft": 0,
      "vehicleIconOffsetXRight": 0,
      // Смещение координаты X для уровня танка.
      "vehicleLevelOffsetXLeft": 0,
      "vehicleLevelOffsetXRight": 0,

      // Прозрачность уровня танка.
      "vehicleLevelAlpha": 0,
      // Offset of X value for frags column.
      // Смещение координаты X для поля фрагов.
      "fragsOffsetXLeft": 0,
      "fragsOffsetXRight": 0,
      // Width of the frags column. Default is 24.
      // Ширина поля фрагов. По умолчанию: 24.
      "fragsWidth": 24,
      // Display format for frags (macros allowed, see macros.txt).
      // Формат отображения фрагов (допускаются макроподстановки, см. macros.txt).
      "fragsFormatLeft": "{{frags}}",
      "fragsFormatRight": "{{frags}}",
      // Shadow for frags field (default null = no shadow, as in vanillas client).
      // Тень для поля фрагов (по умолчанию null = без тени, как в чистом клиенте).
      "fragsShadowLeft": null,
      "fragsShadowRight": null,
      // Offset of X value for rank badge column.
      // Смещение координаты X для поля бейджа ранга.
      "rankBadgeOffsetXLeft": 0,
      "rankBadgeOffsetXRight": 0,
      // Width of the rank badge column. Default is 24.
      // Ширина поля бейджа ранга. По умолчанию: 24.
      "rankBadgeWidth": 24,
      // Transparency of the rank badge.
      // Прозрачность бейджа ранга.
      "rankBadgeAlpha": "{{alive?100|70}}",
      // Offset of X value for player name column.
      // Смещение координаты X для поля имени игрока.
      "nickOffsetXLeft": 0,
      "nickOffsetXRight": 0,
      // Minimum width of the player name column. Default is 46.
      // Минимальная ширина поля имени игрока. По умолчанию: 46.
      "nickMinWidth": 46,

      // Максимальная ширина поля имени игрока. По умолчанию: 158.
      "nickMaxWidth": 158,

      // Формат отображения имени игрока (допускаются макроподстановки, см. macros.txt).
      "nickFormatLeft": "<font face='mono' size='{{xvm-stat?13|0}}' color='{{c:xr}}' alpha='{{alive?#FF|#80}}'>{{r}}</font> {{name%.15s~..}}<font alpha='#A0'>{{clan}}</font>",
      "nickFormatRight": "<font alpha='#A0'>{{clan}}</font>{{name%.15s~..}} <font face='mono' size='{{xvm-stat?13|0}}' color='{{c:xr}}' alpha='{{alive?#FF|#80}}'>{{r}}</font>",

      // Тень для поля имени игрока (по умолчанию null = без тени, как в чистом клиенте).
      "nickShadowLeft": null,
      "nickShadowRight": null,

      // Смещение координаты X для поля названия танка.
      "vehicleOffsetXLeft": 0,
      "vehicleOffsetXRight": 0,

      // Ширина поля названия танка. По умолчанию: 72.
      "vehicleWidth": 72,

      // Формат отображения названия танка (допускаются макроподстановки, см. macros.txt).
      "vehicleFormatLeft": "{{vehicle}}",
      "vehicleFormatRight": "{{vehicle}}",

      // Тень для поля названия танка (по умолчанию null = без тени, как в чистом клиенте).
      "vehicleShadowLeft": null,
      "vehicleShadowRight": null,

      // true - не изменять позиции игроков при уничтожении (по умолчанию false).
      "fixedPosition": false,
      // Extra fields. Each field have default size 350x25.
      // Fields are placed one above the other.
      // Дополнительные поля. Каждое поле имеет размер по умолчанию 350x25.
      // Поля располагаются друг над другом.
      // Set of formats for left panel (extended format supported, see above).
      // Набор форматов для левой панели (поддерживается расширенный формат, см. выше).
      "extraFieldsLeft": [
        ${ "playersPanelTemplates.xc": "def.vehicleName"},
        ${ "playersPanelTemplates.xc": "def.level"},
        ${ "playersPanelTemplates.xc": "def.mark"}
      ],
      // Set of formats for right panel (extended format supported, see above).
      // Набор форматов для правой панели (поддерживается расширенный формат, см. выше).
      "extraFieldsRight": [
        ${ "playersPanelTemplates.xc": "def.vehicleName"},
        ${ "playersPanelTemplates.xc": "def.level"},
        ${ "playersPanelTemplates.xc": "def.mark"}
      ]
    },

    "medium": {
      "enabled": true,
      // Отображаемые стандартные поля в данном режиме, и их порядок.
      // Допустимые названия: "frags", "badge", "nick", "vehicle".
      "standardFields": [ "frags", "nick" ],
      "expandAreaWidth": 0,           // Ширина области переключения при наведении мыши
      "removeSquadIcon": false,       // true - убрать отображение иконок взвода
      "squadIconAlpha": 100,          // прозрачность иконки взвода

      "vehicleIconOffsetXLeft": 0,
      "vehicleIconOffsetXRight": 0,

      "vehicleLevelXOffsetLeft": 0,   // Смещение координаты X для уровня танка.
      "vehicleLevelXOffsetRight": 0,
      "vehicleLevelAlpha": 0,         // прозрачность уровня танка
      "fragsXOffsetLeft": 0,          // Смещение координаты X для поля фрагов.
      "fragsXOffsetRight": 0,
      "fragsWidth": 24,               // Ширина поля фрагов. По умолчанию: 24.
      "fragsFormatLeft": "",          // Формат отображения фрагов
      "fragsFormatRight": "",
      "fragsShadowLeft": null,        // Тень для поля фрагов
      "fragsShadowRight": null,
      "rankBadgeXOffsetLeft": 0,      // Смещение координаты X для поля бейджа ранга.
      "rankBadgeXOffsetRight": 0,
      "rankBadgeWidth": 24,           // Ширина поля бейджа ранга. По умолчанию: 24.
      "nickXOffsetLeft": 0,           // Смещение координаты X для поля имени игрока.
      "nickXOffsetRight": 0,
      "nickMinWidth": 138,            // Минимальная ширина поля имени игрока. По умолчанию:   46.
      "nickMaxWidth": 138,            // Максимальная ширина поля имени игрока. По умолчанию:   158.

      "nickFormatLeft": "",
      "nickFormatRight": "",

      "nickShadowLeft": null,         // Тень для поля имени игрока
      "nickShadowRight": null,
      "vehicleXOffsetLeft": 0,        // Смещение координаты X для поля названия танка.
      "vehicleXOffsetRight": 0,
      "vehicleWidth": 54,             // Ширина поля названия танка. По умолчанию: 72.

      "vehicleFormatLeft": "",
      "vehicleFormatRight": "",

      "vehicleShadowLeft": null,      // Тень для поля названия танка
      "vehicleShadowRight": null,

      "fixedPosition": false,         // true - не изменять позиции игроков при уничтожении

      // Дополнительные поля. Каждое поле имеет размер 350x25. Поля располагаются друг над   другом.
      // Набор форматов для левой панели (поддерживается расширенный формат, см. выше)
      "extraFieldsLeft": [
        ${ "playersPanelTemplates.xc": "def.hpBar"},
        ${ "playersPanelTemplates.xc": "def.hp"},
        ${ "playersPanelTemplates.xc": "def.topClan"},
        ${ "playersPanelTemplates.xc": "def.frags"},
        ${ "playersPanelTemplates.xc": "def.nick"},
        ${ "playersPanelTemplates.xc": "def.vehicleName"},
        ${ "playersPanelTemplates.xc": "def.level"},
        ${ "playersPanelTemplates.xc": "def.mark"}
      ],
      "extraFieldsRight": [
        ${ "playersPanelTemplates.xc": "def.hpBar"},
        ${ "playersPanelTemplates.xc": "def.hp"},
        ${ "playersPanelTemplates.xc": "def.topClan"},
        ${ "playersPanelTemplates.xc": "def.frags"},
        ${ "playersPanelTemplates.xc": "def.nick"},
        ${ "playersPanelTemplates.xc": "def.vehicleName"},
        ${ "playersPanelTemplates.xc": "def.level"},
        ${ "playersPanelTemplates.xc": "def.mark"}
      ]
    },

    "medium2": {
      "enabled": true,
      // Отображаемые стандартные поля в данном режиме, и их порядок.
      // Допустимые названия: "frags", "badge", "nick", "vehicle".
      "standardFields": [ "frags", "nick" ],
      "expandAreaWidth": 0,           // Ширина области переключения при наведении мыши
      "removeSquadIcon": false,       // true - убрать отображение иконок взвода
      "squadIconAlpha": 100,          // прозрачность иконки взвода
      "vehicleIconXOffsetLeft": 0,    // Смещение координаты X для иконки танка.
      "vehicleIconXOffsetRight": 0,
      "vehicleLevelXOffsetLeft": 0,   // Смещение координаты X для уровня танка.
      "vehicleLevelXOffsetRight": 0,
      "vehicleLevelAlpha": 0,        // прозрачность уровня танка
      "fragsXOffsetLeft": 0,          // Смещение координаты X для поля фрагов.
      "fragsXOffsetRight": 0,
      "fragsWidth": 24,               // Ширина поля фрагов. По умолчанию: 24.
      "fragsFormatLeft": "", // Формат отображения фрагов
      "fragsFormatRight": "",
      "fragsShadowLeft": null,        // Тень для поля фрагов
      "fragsShadowRight": null,
      "rankBadgeXOffsetLeft": 0,      // Смещение координаты X для поля бейджа ранга.
      "rankBadgeXOffsetRight": 0,
      "rankBadgeWidth": 24,           // Ширина поля бейджа ранга. По умолчанию: 24.
      "nickXOffsetLeft": 0,           // Смещение координаты X для поля имени игрока.
      "nickXOffsetRight": 0,
      "nickMinWidth": 138,            // Минимальная ширина поля имени игрока. По умолчанию:   46.
      "nickMaxWidth": 138,            // Максимальная ширина поля имени игрока. По умолчанию:   158.

      "nickFormatLeft": "",
      "nickFormatRight": "",

      "nickShadowLeft": null,         // Тень для поля имени игрока
      "nickShadowRight": null,
      "vehicleXOffsetLeft": 0,        // Смещение координаты X для поля названия танка.
      "vehicleXOffsetRight": 0,
      "vehicleWidth": 54,             // Ширина поля названия танка. По умолчанию: 72.

      "vehicleFormatLeft": "",
      "vehicleFormatRight": "",

      "vehicleShadowLeft": null,      // Тень для поля названия танка
      "vehicleShadowRight": null,

      "fixedPosition": false,         // true - не изменять позиции игроков при уничтожении

      // Дополнительные поля. Каждое поле имеет размер 350x25. Поля располагаются друг над   другом.
      // Набор форматов для левой панели (поддерживается расширенный формат, см. выше)
      "extraFieldsLeft": [
        ${ "playersPanelTemplates.xc": "def.hpBar"},
        ${ "playersPanelTemplates.xc": "def.hp"},
        ${ "playersPanelTemplates.xc": "def.topClan"},
        ${ "playersPanelTemplates.xc": "def.frags"},
        ${ "playersPanelTemplates.xc": "def.winrate"},
        ${ "playersPanelTemplates.xc": "def.vehicleReload"},
        ${ "playersPanelTemplates.xc": "def.vehicleDamage"},
        ${ "playersPanelTemplates.xc": "def.level"}
        
      ],
      "extraFieldsRight": [
        ${ "playersPanelTemplates.xc": "def.hpBar"},
        ${ "playersPanelTemplates.xc": "def.hp"},
        ${ "playersPanelTemplates.xc": "def.topClan"},
        ${ "playersPanelTemplates.xc": "def.frags"},
        ${ "playersPanelTemplates.xc": "def.winrate"},
        ${ "playersPanelTemplates.xc": "def.vehicleReload"},
        ${ "playersPanelTemplates.xc": "def.vehicleDamage"},
        ${ "playersPanelTemplates.xc": "def.level"}
      ]
    }
  }
}
