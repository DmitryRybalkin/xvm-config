﻿/**
 * Общие параметры боевого интерфейса
 * Конфиг: "dar"
 */
{
  "battle": {
    // false - disable tank icon mirroring (good for alternative icons).
    // false - отключить зеркалирования иконок танков (полезно для альтернативных иконок).
    "mirroredVehicleIcons": false,
    // false - disable pop-up panel at the bottom after death.
    // false - отключить всплывающую внизу панель после смерти.
    "showPostmortemTips": false,
    // false - disable highlighting of own vehicle icon and squad.
    // false - отключить подсветку иконки своего танка и взвода.
    "highlightVehicleIcon": false,
    // Format of clock on the Debug Panel (near FPS).
    // Формат часов на экране панели отладки (возле FPS).
    // http://php.net/date
    "clockFormat": "",
    "sixthSenseIcon": "cfg://dar/img/SixthSense.png",
    "sixthSenseDuration": 11000,
    "battleHint": {
      // true - hide the tips aiming mode changing in strategic mode.
      // true - скрыть подсказку смены режима прицеливания в стратегическом режиме.
      "hideTrajectoryView": true,
      // true - hide the tips about switching to siege mode and changing the driving mode (for wheeled vehicles).
      // true - скрыть подсказку перехода в осадный режим и смены режима движения (для колесной техники).
      "hideSiegeIndicator": true,
      // true - hide the tips about switching to menu Personal Missions.
      // true - скрыть подсказку перехода в меню ЛБЗ.
      "hideQuestProgress": true,
      "hideWheeledMode": false,
      // true - hide the tips of the transition to the window of exploring the features of the machine (for wheeled vehicles).
      // true - скрыть подсказку перехода к окну знакомства с особенностями машины (для колесной техники).
      "hideHelpScreen": true
    },
    "elements": [
      ${ "elements.xc": "elements" }
    ],

    "expertPanel": { 
      // Delay for panel disappear. Original value was 5.
      // Задержка исчезновения панели. Оригинальное значение было 5.
      "delay": 15,
      // Panel scaling. Original value was 100.
      // Увеличение панели. 100 в оригинале.
      "scale": 150 
      },
      // Parameters of the After Battle Screen.
  // Параметры окна послебоевой статистики.
    "camera": ${ "camera.xc": "camera" }
  }
}