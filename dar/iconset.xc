﻿/**
 * Атласы с набором иконок
 * Конфиг: "dar"
 */
{
  "iconset": {
    "battleLoadingLeftAtlas": "../../../../configs/xvm/dar/img/atlases/IconsAtlas",
    "battleLoadingRightAtlas": "../../../../configs/xvm/dar/img/atlases/IconsAtlas",
    "playersPanelLeftAtlas": "../../../../configs/xvm/dar/img/atlases/IconsAtlas",
    "playersPanelRightAtlas": "../../../../configs/xvm/dar/img/atlases/IconsAtlas",
    "fullStatsLeftAtlas": "../../../../configs/xvm/dar/img/atlases/IconsAtlas",
    "fullStatsRightAtlas": "../../../../configs/xvm/dar/img/atlases/IconsAtlas",
    "vehicleMarkerAllyAtlas": "vehicleMarkerAtlas",
    "vehicleMarkerEnemyAtlas": "vehicleMarkerAtlas"
  }
}