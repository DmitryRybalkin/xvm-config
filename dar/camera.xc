﻿/**
 * Настройки камеры
 * Конфиг: "dar"
 */
{
  "camera": {
    "enabled": true,
    "noFlashBang": true,
    "arcade": {
      "distRange": [ 10, 600 ],
      "startDist": null,
      "scrollSensitivity": 8,
      "shotRecoilEffect": false
    },
    "postmortem": {
      "distRange": [ 10, 600 ],
      "startDist": null,
      "scrollSensitivity": 16,
      "shotRecoilEffect": false
    },
    "strategic": {
      "distRange": [ 10, 600 ],
      "shotRecoilEffect": false
    },
    "sniper": {
      "zooms": [ 2, 4, 8, 16, 25 ],
      "startZoom": null,
      "zoomIndicator": {
        "enabled": false,
        "x": 145, "alpha": 100,
        "shadow": { "alpha": 65, "blur": 3, "color": "0x000000", "strength": 6 },
        "format": "<font color='#95E600' face='$FieldFont' size='17'>&#xD7;</b>{{zoom}}</b></font>"
      },
      "startDist": null,
      "shotRecoilEffect": false,
      "noBinoculars": true,
      "noCameraLimit": {
        "enabled": true,
        "mode": "custom"
      }
    }
  }
}