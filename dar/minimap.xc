﻿{
 "minimap": {
    "enabled": true,
    "mapBackgroundImageAlpha": 100,                           // прозрачность изображения карты.
    "selfIconColor": "{{player?#FFFFFF|{{c:system}}}}",       // цвет своей иконки (белая стрелка).
    "selfIconAlpha": 75,                                      // прозрачность своей иконки (белая стрелка).
    "selfIconScale": 1,                                       // масштаб своей иконки (белая стрелка).
    "iconAlpha": 100,                                         // прозрачность иконок типа техники.
    "iconScale": 1,                                           // масштаб иконки техники.
    "directionTriangleAlpha": 100,                            // прозрачность зеленого треугольника направления камеры.
    "directionLineAlpha": 100,                                // прозрачность стандартного луча направления камеры.
    "showDirectionLineAfterDeath": true,                      // отображать линию направления камеры после смерти.
    "showCellClickAnimation": true,	                          // отображать анимацию клика по ячейке.
    "minimapAimIcon": "xvm://res/MinimapAim.png",             // путь к иконке для артиллерийского прицела.
    "minimapAimIconScale": 50,                                // масштаб иконки для артиллерийского прицела (в процентах).
    /** Увеличение миникарты по нажатию кнопки. */
    "zoom": { "index": 4, "centered": false },
    /** Поле размера стороны карты. */
    "mapSize": {
        "enabled": true, "x": 0, "y": 0, "alpha": 70,
        "textFormat": { "font": "$FieldFont", "color": "0x838260", "size": 6, "align": "left", "bold": true },
        "shadow": { "enabled": true, "color": "0x000000", "alpha": 70, "blur": 2, "strength": 3 },
        "format": "{{cellsize}}0 {{l10n:m}}"
    },
    "circlesEnabled": true,                                   // false - использовать стандартные круги.
    "labelsEnabled": true,                                    // false - использовать стандартные надписи.
    "linesEnabled": true,                                     // false - использовать стандартные линии.
    "labels": ${ "minimapLabels.xc": "labels" },              // надписи на миникарте.
    "labelsData": ${ "minimapLabelsData.xc": "labelsData" },  // общие данные для надписей на миникарте.
    "circles": ${ "minimapCircles.xc": "circles" },           // круги на миникарте.
    "lines": ${ "minimapLines.xc": "lines" }                  // линии на миникарте.
    }
}