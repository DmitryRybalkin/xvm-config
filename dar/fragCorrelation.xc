﻿/**
 * Панель счёта в бою
 * Конфиг: "dar"
 */
/*
{{py:fcb.enemyVehicleAlive}} - возвращает строку с количеством и типом техники живых противников.
{{py:fcb.allyVehicleAlive}} - возвращает строку с количеством и типом техники живых союзников.
{{py:fcb.enemyVehicleDead}} - возвращает строку с количеством и типом техники мертвых противников.
{{py:fcb.allyVehicleDead}} - возвращает строку с количеством и типом техники мертвых союзников.
{{py:fcb.aliveVehType(vtype)}} - возвращает строку из секции "vtypeAlive". Параметр vtype может принимать значения "HT", "MT", "TD", "SPG", "LT".
{{py:fcb.deadVehType(vtype)}} - возвращает строку из секции "vtypeDead". Параметр vtype может принимать значения "HT", "MT", "TD", "SPG", "LT".

{{py:fcb.countAllyAlive(vtype)}} - количество живой техники союзников, указанного типа. Параметр vtype может принимать значения "HT", "MT", "TD", "SPG", "LT".

{{py:fcb.countEnemyAlive(vtype)}} - количество живой техники противников, указанного типа. Параметр vtype может принимать значения "HT", "MT", "TD", "SPG", "LT".

{{py:fcb.countAllyDead(vtype)}} - количество разрушенной техники союзников, указанного типа. Параметр vtype может принимать значения "HT", "MT", "TD", "SPG", "LT".

{{py:fcb.countEnemyDead(vtype)}} - количество разрушенной техники противников, указанного типа. Параметр vtype может принимать значения "HT", "MT", "TD", "SPG", "LT".

Макросы обновляются по событию PY(ON_UPDATE_FRAG_COR_BAR).

Значки типа техники задаются в файле battle.xc в секциях "vtypeAlive" и "vtypeDead" секции "fragCorrelation" (смотрите пример ниже).

В секциях "vtypeAlive" и "vtypeDead" поддерживаются макросы:

{{level}} - уровень техники,
{{ally}} - возвращает 'ally' для союзника, иначе пусто.
{{c:wn8}}, {{c:xwn8}}, {{c:wtr}}, {{c:xwtr}}, {{c:eff}}, {{c:xeff}}, {{c:wgr}}, {{c:xwgr}}, {{c:xte}}, {{c:r}}  - динамические цвета соответствующих рейтингов.
 */

{
  "fragCorrelation": {
    //true - две строки с маркерами техники в генеральном сражение
    //false - одна строка с маркерами техники в генеральном сражение
    "twoLineEpicRandom": true,
    "directSortByLevelAllys": false,
    "directSortByLevelEnemies": true,
    "markersAllysOrder": [ "unknown", "LT", "SPG", "TD", "MT", "HT" ],
    "markersEnemiesOrder": [ "HT", "MT", "TD", "SPG", "LT", "unknown" ],
    "vtypeAlive": {
      "LT":      "<font color='#{{ally?60FF00|FF0000}}' face='xvm' size='21'> &#x3A;</font>",
      "MT":      "<font color='#{{ally?60FF00|FF0000}}' face='xvm' size='21'> &#x3B;</font>",
      "HT":      "<font color='#{{ally?60FF00|FF0000}}' face='xvm' size='21'> &#x3F;</font>",
      "SPG":     "<font color='#{{ally?60FF00|FF0000}}' face='xvm' size='21'> &#x2D;</font>",
      "TD":      "<font color='#{{ally?60FF00|FF0000}}' face='xvm' size='21'> &#x2E;</font>",
      "unknown": "<font color='#{{ally?60FF00|FF0000}}' face='xvm' size='21'> &#x44;</font>"
    },
    "vtypeDead": {
      "LT":      "<font alpha='#CC' color='#FFFFFF' face='xvm' size='21'> &#x3A;</font>",
      "MT":      "<font alpha='#CC' color='#FFFFFF' face='xvm' size='21'> &#x3B;</font>",
      "HT":      "<font alpha='#CC' color='#FFFFFF' face='xvm' size='21'> &#x3F;</font>",
      "SPG":     "<font alpha='#CC' color='#FFFFFF' face='xvm' size='21'> &#x2D;</font>",
      "TD":      "<font alpha='#CC' color='#FFFFFF' face='xvm' size='21'> &#x2E;</font>",
      "unknown": "<font alpha='#CC' color='#FFFFFF' face='xvm' size='21'> &#x44;</font>"
    }
  },

  "def": {

    "bg": {
      // false - отключить панель счёта, если используется альтернативная панель
      "enabled": true,
      "x": 0,
      "y": -1,
      "width": 1200,
      "height": 100,
      "alpha": "{{battletype-key=epic_random?0|100}}",
      "screenHAlign": "center",
      "format": "<img src='cfg://dar/img/fragCorrelation/bg/hp.png' width='1024' height='56'>"
    },

    "text": {
      "$ref": { "path": "def.bg" },
      "updateEvent": "PY(ON_TOTAL_EFFICIENCY), PY(ON_UPDATE_HP)",
      "x": 0,
      "y": 0,
      "shadow": {
        "alpha": 30,
        "angle": 90,
        "blur": 3,
        "color": "0x000000",
        "distance": 1,
        "strength": 1
      },
      "format": "<font color='#FFFFFF' face='$FieldFont' size='15'><textformat leading='-21' tabstops='[568,595,610,710]'>\t<font color='#FFFEFE' face='$TitleFont' size='24'>{{py:sp.allyFrags%2d}}\t{{py:sp.signScore=&#61;?:}}\t{{py:sp.enemyFrags}}</font></textformat></font>"
    },

    "textImg": {
      "$ref": { "path": "def.bg" },
      "updateEvent": "PY(ON_UPDATE_HP)",
      "x": 0,
      "y": -1,
      "alpha": 100,
      "shadow": {
        "alpha": 45,
        "angle": 90,
        "blur": 3,
        "color": "0x000000",
        "distance": 1,
        "strength": 1
      },
      "format": "<font color='#FFFFFF' face='mono' size='18'><textformat leading='{{battletype-key=epic_random?{{.options.fragCorShowMarkers=true?1|-6}}|-9}}' leftMargin='{{py:sp.signScore=&gt;?422|569}}'><img src='cfg://dar/img/fragCorrelation/arrow/{{battletype-key=epic_random?|{{py:sp.signScore=&gt;?good|{{py:sp.signScore=&lt;?bad}}}}}}.png' width='205' height='32'></textformat>\n<textformat leftMargin='533'>{{py:xvm.total_hp.ally%5d}}<img src='cfg://dar/img/fragCorrelation/hp/{{py:xvm.total_hp.sign=&gt;?good|{{py:xvm.total_hp.sign=&lt;?bad|neutral}}}}.png' width='42' height='42' vspace='-15'>{{py:xvm.total_hp.enemy}}</textformat></font>"
    },

    "vtype": {
      "$ref": { "path": "def.bg" },
      "updateEvent": "PY(ON_UPDATE_FRAG_COR_BAR)",
      "x": 0, "y": "{{battletype-key=epic_random?39|29}}", "alpha": "{{.options.fragCorShowMarkers=true?100|0}}",
      "shadow": { "alpha": "45", "angle": 90, "blur": 3, "color": "0x000000", "distance": 1, "strength": 1 },
      "format": "<textformat tabstops='[35,278,670]'>{{battletype-key!=epic_random?\t}}\t{{py:fcb.allyVehicleDead}}{{py:fcb.allyVehicleAlive}}{{battletype-key=epic_random?\t}}\t{{py:fcb.enemyVehicleAlive}}{{py:fcb.enemyVehicleDead}}</textformat>"
    }
  }
}