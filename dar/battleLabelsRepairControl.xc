﻿// Ремонт модулей и танкисторв в прицеле

{
  "deviceRepairCtrlItem": {
    "width": 48,
    "height": 48,
    "screenHAlign": "center",
    "screenVAlign": "center",
    "layer": "top",
    //"hotKeyCode": 29,
    //"visibleOnHotKey": true,
    //"onHold": false,
    "shadow": { "distance": 1, "angle": 90, "alpha": 95, "blur": 5, "strength": 4 }
  },
  "crewHealCtrlItem": {
    "width": 36,
    "height": 36,
    "screenHAlign": "center",
    "screenVAlign": "center",
    "layer": "top",
    //"hotKeyCode": 29,
    //"visibleOnHotKey": true,
    //"onHold": true,
    "shadow": { "distance": 1, "angle": 90, "alpha": 95, "blur": 5, "strength": 4 }
  },

  // Досылатель
  "repairCtrlAmmoBay": {
    "$ref": { "path": "deviceRepairCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "ammoBay",
      "mouseOver": "ammoBayOver",
    "mouseOut": "ammoBayOut"
    },
    "updateEvent": "PY(ON_AMMOBAY_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x":50,
    "y": "{{py:getYWOffset(-50,-80)}}",
    "alpha": "{{alive?{{py:ammoBayState=normal?0|{{py:ammoBayState=critical?100|100}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/devices/ammoBay-{{py:ammoBayState}}.png'>"
  },

  // Пушка
  "repairCtrlGun": {
    "$ref": { "path": "deviceRepairCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "gun",
      "mouseOver": "gunOver",
    "mouseOut": "gunOut"
    },
    "updateEvent": "PY(ON_GUN_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": 100,
    "y": "{{py:getYWOffset(-50, -80)}}",
    "alpha": "{{alive?{{py:gunState=normal?0|{{py:gunState=critical?100|{{py:gunState=destroyed?100|100}}}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/devices/gun-{{py:gunState}}.png'>"
   },

  // Двигатель
  "repairCtrlEngine": {
    "$ref": { "path": "deviceRepairCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "engine",
      "mouseOver": "engineOver",
	  "mouseOut": "engineOut"
    },
    "updateEvent": "PY(ON_ENGINE_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": 150,
    "y": "{{py:getYWOffset(-50, -80)}}",
	"alpha": "{{alive?{{py:engineState=normal?0|{{py:engineState=critical?100|{{py:engineState=destroyed?100|100}}}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/devices/engine-{{py:engineState}}.png'>"
  },

  // Башня
  "repairCtrlTurret": {
    "$ref": { "path": "deviceRepairCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "turretRotator",
      "mouseOver": "turretRotatorOver",
    "mouseOut": "turretRotatorOut"
    },
    "updateEvent": "PY(ON_TURRETROTATOR_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": -61,
    "y": "{{py:getYWOffset(0, -45)}}",
    "alpha": "{{alive?{{py:turretState=normal?30|{{py:turretState=critical?100|{{py:turretState=destroyed?100|100}}}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/devices/turret-{{py:turretState}}.png'>"
  },

  // Гусеницы
  "repairCtrlComplex": {
    "$ref": { "path": "deviceRepairCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "complex",
      "mouseOver": "complexOver",
	  "mouseOut": "complexOut"
    },
    "updateEvent": "PY(ON_COMPLEX_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": 61,
    "y": "{{py:getYWOffset(-165, -45)}}",
	"alpha": "{{alive?{{py:complexState=normal?30|{{py:complexState=critical?100|{{py:complexState=destroyed?100|100}}}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/devices/{{py:complexType}}-{{py:complexState}}.png'>"
  },

  // Приборы наблюдения
  "repairCtrlSurveying": {
    "$ref": { "path": "deviceRepairCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "surveyingDevice",
      "mouseOver": "surveyingDeviceOver",
	  "mouseOut": "surveyingDeviceOut"
    },
    "updateEvent": "PY(ON_SURVEYINGDEVICE_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": 98,
    "y": "{{py:getYWOffset(-112, -45)}}",
	"alpha": "{{alive?{{py:surveyingState=normal?100|{{py:surveyingState=critical?100|{{py:surveyingState=destroyed?100|100}}}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/devices/surveying-{{py:surveyingState}}.png'>"
  },

  // Рация
  "repairCtrlRadio": {
    "$ref": { "path": "deviceRepairCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "radio",
      "mouseOver": "radioOver",
	  "mouseOut": "radioOut"
    },
    "updateEvent": "PY(ON_RADIO_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": 98,
    "y": "{{py:getYWOffset(-52, -45)}}",
    "alpha": "{{alive?{{py:radioState=normal?30|{{py:radioState=critical?100|{{py:radioState=destroyed?100|100}}}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/devices/radio-{{py:radioState}}.png'>"
  },

  // Топливные баки
  "repairCtrlFuelTank": {
    "$ref": { "path": "deviceRepairCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "fuelTank",
      "mouseOver": "fuelTankOver",
	  "mouseOut": "fuelTankOut"
    },
    "updateEvent": "PY(ON_FUELTANK_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": 61,
    "y": "{{py:getYWOffset(0, -45)}}",
    "alpha": "{{alive?{{py:fuelTankState=normal?30|{{py:fuelTankState=critical?7|{{py:fuelTankState=destroyed?100|100}}}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/devices/fuelTank-{{py:fuelTankState}}.png'>"
  },


  // Заряжающий
  "healCtrlLoader": {
    "$ref": { "path": "crewHealCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "loader",
      "mouseOver": "loaderOver",
	  "mouseOut": "loaderOut"
    },
    "updateEvent": "PY(ON_LOADER_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": 45,
    "y": "{{py:getYWOffset(-110, -82)}}",
	"alpha": "{{alive?{{py:loaderState=normal?0|{{py:loaderState=destroyed?100|100}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/crew/loader-{{py:loaderState}}.png'>"
  },

  // Водитель
  "healCtrlDriver": {
    "$ref": { "path": "crewHealCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "driver",
      "mouseOver": "driverOver",
      "mouseOut": "driverOut"
    },
    "updateEvent": "PY(ON_DRIVER_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": 77,
    "y": "{{py:getYWOffset(-110, -82)}}",
    "alpha": "{{alive?{{py:driverState=normal?0|{{py:driverState=destroyed?100|100}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/crew/driver-{{py:driverState}}.png'>"
  },

  // Наводчик
  "healCtrlGunner": {
    "$ref": { "path": "crewHealCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "gunner",
      "mouseOver": "gunnerOver",
      "mouseOut": "gunnerOut"
    },
    "updateEvent": "PY(ON_GUNNER_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": 109,
    "y": "{{py:getYWOffset(-110, -82)}}",
	"alpha": "{{alive?{{py:gunnerState=normal?0|{{py:gunnerState=destroyed?100|100}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/crew/gunner-{{py:gunnerState}}.png'>"
  },

  // Командир
  "healCtrlCommander": {
    "$ref": { "path": "crewHealCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "commander",
      "mouseOver": "commanderOver",
	  "mouseOut": "commanderOut"
    },
    "updateEvent": "PY(ON_COMMANDER_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": 141,
    "y": "{{py:getYWOffset(-110, -82)}}",
	  "alpha": "{{alive?{{py:commanderState=normal?0|{{py:commanderState=destroyed?100|100}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/crew/commander-{{py:commanderState}}.png'>"
  },

  // Радист
  "healCtrlRadioman": {
    "$ref": { "path": "crewHealCtrlItem" },
    "enabled": true,
    "mouseEvents": {
      "click": "radioman",
      "mouseOver": "radiomanOver",
	  "mouseOut": "radiomanOut"
    },
    "updateEvent": "PY(ON_RADIOMAN_STATE), PY(ON_VIEW_CHANGED), ON_CURRENT_VEHICLE_DESTROYED",
    "x": -40,
    "y": "{{py:getYWOffset(55, -45)}}",
	"alpha": "{{alive?{{py:radiomanState=normal?30|{{py:radiomanState=destroyed?100|100}}}}|0}}",
    "format": "<img src='cfg://dar/img/RepairControl/crew/radioman-{{py:radiomanState}}.png'>"
  }
}