﻿{
  "infoPanel": {
    // false - Disable info panel.
    // false - Отключить информационную панель.
    "enabled": false,
    // true - Show only for: "ally", "enemy", "all"
    // true - Отображать только для: "ally" - союзников, "enemy" - противников, "all" - всех
    "showFor": "all",
    //true - Show only for alive players.
    //true - Отображать только для живых игроков.
    "aliveOnly": false,
    // Hide panel delay (in seconds)
    // Задержка скрытия панели (в секундах)
    "delay": 5,
    // Key for self vehicle information display.
    // Клавиша для отображения информации о своей технике.
    "altKey": 56,
    // Values for {{py:infoPanel.compareDelim(X, Y)}} and {{py:infoPanel.compareColor(X, Y)}} macros
    // Значения для макросов {{py:infoPanel.compareDelim(X, Y)}} и {{py:infoPanel.compareColor(X, Y)}}
    "compareValues": {
      "moreThan": {
        "delim": "&gt;", // '<'
        "color": "#FF0000"
      },
      "equal": {
        "delim": "=",
        "color": "#FFFFFF"
      },
      "lessThan": {
        "delim": "&lt;", // '>'
        "color": "#00FF00"
      }
    },
    // Block of text formats. Allow multiple formats (https://koreanrandom.com/forum/topic/36811-/).
    // Блок текстовых форматов. Допускается создание нескольких форматов (https://koreanrandom.com/forum/topic/36811-/).
    "formats": [
      "{{shell_power_1}} {{shell_power_2}} {{shell_power_3}}"
    ]
  }
}
