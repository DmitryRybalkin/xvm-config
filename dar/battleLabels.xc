﻿/**
 * Текстовые поля боевого интерфейса
 * Конфиг: "dar"
 */
{
  "labels": {
    "formats": [
      // Таймер шестого чувства
      ${ "battleLabelsTemplates.xc": "def.sixthSenseTimer" },

      // Пинг и фпс
      ${ "battleLabelsTemplates.xc": "def.debugPanelData" },
      // Текущее время
      ${ "battleLabelsTemplates.xc": "def.time" },
      // Счетчик боев и побед за сегодня
      ${ "battleLabelsTemplates.xc": "def.countBattleDay" },
      // Эфективность WN8 и EFF
      ${ "battleLabelsTemplates.xc": "def.efficiency" },
      // Тикающий счетчик секунд под прицелом
      ${ "battleLabelsTemplates.xc": "def.timerSeconds" },

      // Часы в углу экрана и таймер перед боем
      ${ "battleLabelsTemplates.xc": "def.timer.battle" },
      ${ "battleLabelsTemplates.xc": "def.timer.prebattle" },

      // Лог нанесенного урона и ассиста
      ${ "battleLabelsTemplates.xc": "def.hitLog" },

      // Дамаг лог
      ${ "battleLabelsTemplates.xc": "def.damageLog.header" },
      ${ "battleLabelsTemplates.xc": "def.damageLog.history" },
      ${ "battleLabelsTemplates.xc": "def.damageLog.lastHit" },

      // Маркер направления, откуда был выстрел
      ${ "battleLabelsTemplates.xc":"def.damageIndicator" },

      // Таймеры починки модулей в панели повреждений
      ${ "battleLabelsTemplates.xc": "def.repairTime.engine" },
      ${ "battleLabelsTemplates.xc": "def.repairTime.gun" },
      ${ "battleLabelsTemplates.xc": "def.repairTime.turret" },
      ${ "battleLabelsTemplates.xc": "def.repairTime.tracks" },
      ${ "battleLabelsTemplates.xc": "def.repairTime.surveying" },
      ${ "battleLabelsTemplates.xc": "def.repairTime.radio" },
      ${ "battleLabelsTemplates.xc": "def.repairTime.radio" },

      // УГН
      ${ "battleLabelsTemplates.xc":"def.angleAimingLeft"},
      ${ "battleLabelsTemplates.xc":"def.angleAimingRight"},

      // Ремонт модулей и танкисторв в прицеле
      ${ "battleLabelsRepairControl.xc":"repairCtrlEngine" },
      ${ "battleLabelsRepairControl.xc":"repairCtrlAmmoBay" },
      ${ "battleLabelsRepairControl.xc":"repairCtrlGun" },
      ${ "battleLabelsRepairControl.xc":"healCtrlCommander" },
      ${ "battleLabelsRepairControl.xc":"healCtrlDriver" },
      ${ "battleLabelsRepairControl.xc":"healCtrlGunner" },
      ${ "battleLabelsRepairControl.xc":"healCtrlLoader" },

      // Панель счёта в бою
      ${ "fragCorrelation.xc": "def.bg" },
      ${ "fragCorrelation.xc": "def.textImg" },
      ${ "fragCorrelation.xc": "def.vtype" },
      ${ "fragCorrelation.xc": "def.text" },

      // Прицел (без сведения)

      // Время полной перезарядки
      ${ "battleLabelSight.xc": "def.baseTimeReload"},
      ${ "battleLabelSight.xc": "def.distance"},
      ${ "battleLabelSight.xc": "def.damage"},
    //${ "battleLabelSight.xc": "def.clipReload"},
      ${ "battleLabelSight.xc": "def.penetration" },
      ${ "battleLabelSight.xc": "def.grid"},
      ${ "battleLabelSight.xc": "def.reloadBar" },
      ${ "battleLabelSight.xc": "def.reloadTime" },
      ${ "battleLabelSight.xc": "def.myHp"},
      ${ "battleLabelSight.xc": "def.myHpBar"},
      ${ "battleLabelSight.xc": "def.timeFlight"},
      ${ "battleLabelSight.xc": "def.timeAIM"}
    ]
  }
}