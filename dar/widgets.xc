﻿/**
 * Виджеты
 * Конфиг: "dar"
 */
{
  "widgets": {

    // Информация перед загрузкой игры
    "login": [
      {
      "name": "definition",
      "enabled": true, "layer": "normal", "type": "extrafield",
      "formats": [
          {
          "updateEvent": "ON_EVERY_SECOND",
          "x": 14, "y": 28, "width": 300, "height": 24,
          "shadow": { "alpha": 35, "blur": 3, "color": "0x000000", "strength": 1 },
          "format": "<font color='#8C8C7E' face='$FieldFont' size='14'>{{l10n:Configuration}} <font color='#F68700'>XVM</font>: «dar» ({{.definition.date}})</font>"
          }
        ]
      }
    ],

    // Ангар
    "lobby": [

      // Переход на сайт XVM для просмотра статистики
      ${ "widgetsTemplates.xc":"statistics_XVM" },

      // Личные резервы
      ${ "widgetsTemplates.xc":"booster" },

      // Часы
      {
        "name": "clock",
        "enabled": false,
        "layer": "top",
        "type": "extrafield",
        "formats": [
          {
          "updateEvent": "ON_EVERY_SECOND",
          "x": -262,
          "y": 0,
          "width": 250,
          "height": 60,
          "screenHAlign": "center",
          "shadow": {
            "angle": 90,
            "blur": 3,
            "color": "0x000000",
            "distance": 1,
            "strength": 3
            },
          "format": "<font color='#D9D2B9' face='$FieldFont' size='15'><p align='right'><textformat leading='-40' rightMargin='17'><font size='37'>{{py:xvm.formatDate('%H:%M')}}</font></textformat>\n<textformat leading='-3' rightMargin='104'>{{py:xvm.formatDate('%A')}}</textformat>\n<textformat leading='-43' rightMargin='104'>{{py:xvm.formatDate('%d %bl. %Y')}}</textformat>\n<img src='cfg://dar/img/lobby/clock/bg.png' width='237' height='52'></p></font>"
          }
        ]
      },

      // статистика в ангаре по нажатию Alt
      { "name": "statistics",
        "enabled": true,
        "layer": "normal",
        "type": "extrafield",
        "formats": [
          { "x": 0,
            "y": 260,
            "width": 500,
            "height": 250,
            "screenHAlign": "center",
            "hotKeyCode": 56,
            "onHold": true,
            "format": "<img src='cfg://dar/img/lobby/statistics/red.png' width='482' height='222'>"
          },
          { "updateEvent": "ON_MY_STAT_LOADED",
            "x": 0,
            "y": 268,
            "width": 500,
            "height": 400,
            "screenHAlign": "center",
            "hotKeyCode": 56,
            "onHold": true,
            "shadow": {
              "alpha": 35,
              "blur": 3,
              "color": "0x000000",
              "strength": 1
            },
            "format": "<font color='#{{xvm-stat?FFFFFF|666666}}' face='$FieldFont' size='24'><textformat leading='44' tabstops='[20,406]'>\t<font color='#EDE0BB' size='16'>{{l10n:General stats}}</font>\t<font color='#666666' size='13'>{{py:xvm.formatDate('%d.%m.%Y')|&nbsp;}}</font></textformat>\n<textformat leading='3' tabstops='[28,198,333]'>\t<img src='cfg://dar/img/lobby/statistics/battles.png' width='40' height='32' vspace='-6'>{{mystat.battles%'d|0000}}\t<img src='cfg://dar/img/lobby/statistics/avg.png' width='40' height='32' vspace='-6'>{{mystat.avglvl%.2f|0.00}}\t<img src='cfg://dar/img/lobby/statistics/wins.png' width='40' height='32' vspace='-6'>{{mystat.winrate%.2f|00}}%</textformat>\n<textformat leading='34' tabstops='{{l10n:[65,76,218,375]}}'><font color='#7A776D' size='13'>\t\t{{l10n:Fights}}\t{{l10n:Avg level}}\t{{l10n:Wins}}</font>\n\t<font color='#59574F' size='20'>{{l10n:WN8}}:</font> <b>{{mystat.wn8|0000}}</b> <font color='#6F6C62' size='20'>({{mystat.xwn8|00}})</font><img src='cfg://dar/img/lobby/statistics/rating.png' width='90' height='55' vspace='-20'><font color='#6F6C62' size='20'>{{l10n:EFF}}:</font> <b>{{mystat.eff|0000}}</b> <font color='#59574F' size='20'>({{mystat.xeff|00}})</font></textformat></font>"
          }
        ]
      }
    ]
  }
}