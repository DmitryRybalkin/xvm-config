from xfw import *
from xfw_actionscript.python import *

from gui.Scaleform.daapi.view.battle.shared.battle_timers import BattleTimer
from Vehicle import Vehicle
import BigWorld


minutes = 0
seconds = 0
startBattle = 0
arenaPeriod = None


@registerEvent(BattleTimer, 'setTotalTime')
def setTotalTime(self, totalTime):
    global minutes, seconds, startBattle
    minutes, seconds = divmod(int(totalTime), 60)
    startBattle = arenaPeriod.getPeriod() if arenaPeriod is not None else 0
    as_event('ON_BATTLE_TIMER')


@registerEvent(Vehicle, 'onEnterWorld')
def onEnterWorld(self, prereqs):
    global minutes, seconds, startBattle, arenaPeriod
    if self.isPlayerVehicle:
        minutes = 0
        seconds = 0
        startBattle = 0
        player = BigWorld.player()
        arenaPeriod = player.guiSessionProvider.shared.arenaPeriod


@xvm.export('xvm.minutesBT', deterministic=False)
def xvm_minutesBT():
    return minutes if startBattle in [2, 3] else None


@xvm.export('xvm.secondsBT', deterministic=False)
def xvm_secondsBT():
    return seconds if startBattle in [2, 3] else None


@xvm.export('xvm.critTimeBT', deterministic=False)
def xvm_critTimeBT(_critTime=120):
    return '#FF0000' if (_critTime > (minutes * 60 + seconds)) and (startBattle == 3) else None

