﻿/**
 * Текстовые поля боевого интерфейса
 * Конфиг: "dar"
 */
{
  "def": {

    // Таймер шестого чувства
    "sixthSenseTimer": {
      "enabled": true,
      "updateEvent": "PY(ON_SIXTH_SENSE_SHOW)",
      "x": 0,
      "y": 251,
      "width": 100,
      "height": 100,
      "screenHAlign": "center",
      "shadow": {
        "distance": 0,
        "angle": 90,
        "alpha": 100,
        "blur": 10,
        "strength": 0,
        "color": "0xE3BD00"
      },
      "textFormat": {
        "font": "$FieldFont",
        "bold": false,
        "align": "center",
        "color": "0xE3BE02",
        "size": 27
      },
      "format": "{{py:xvm.sixthSenseTimer(10)}}"
    },

    "debugPanelData": {
      "updateEvent": "ON_EVERY_SECOND",
      "x": 19,
      "y": 2,
      "width": 580,
      "height": 50,
      "antiAliasType": "normal",
      "shadow": {
        "$ref": { "file": "@xvm.xc", "path": "mainShadow" }
      },
      "textFormat": {
        "color": "0xF0F0F0",
        "font": "$FieldFont",
        "leading": 2,
        "size": 15
      },
      "format": "<textformat leading='-4' tabstops='[27,57,219]'><img src='cfg://dar/img/debug_panel/condition_{{py:xvm.lag?off|on}}.png' width='24' height='24' vspace='-9'>\tфпс\t<font color='#FFFFFF' face='mono'>{{py:xvm.fps}}</font>\n\tпинг\t<font color='#FFFFFF' face='mono'>{{py:xvm.ping}}</font> мс</textformat>"
    },

    // Текущее время
    "time": {
      "updateEvent": "ON_EVERY_SECOND",
      "x": -40,
      "y": 2,
      "screenHAlign": "right",
      "width": 100,
      "height": 50,
      "shadow": {
        "$ref": { "file": "@xvm.xc", "path": "mainShadow" }
      },
      "textFormat": {
        "color": "0xF0F0F0",
        "font": "$FieldFont",
        "size": 15
      },
      "format": "<font color='#FFFFFF' face='mono'>{{py:xvm.formatDate('%H:%M')}}</font>"
    },

    // Счетчик боев и побед за сегодня
    "countBattleDay": {
      "updateEvent": "PY(ON_STATISTICS)",
      "x": 160,
      "y": 2,
      "width": 580,
      "height": 50,
      "antiAliasType": "normal",
      "shadow": {
        "$ref": { "file": "@xvm.xc", "path": "mainShadow" }
      },
      "textFormat": {
        "color": "0xF0F0F0",
        "font": "$FieldFont",
        "leading": 2,
        "size": 15
      },
      "format": "Сыграно боёв: <font color='#FFB01B'>{{py:xvm.countBattleDay}}</font>\nПобед: <font color='#FFB01B'>{{py:xvm.winsSessionDay}}</font>"
    },

    // Эфективность WN8 и EFF
    "efficiency": {
      "enabled": true,
      "updateEvent": "PY(ON_BATTLE_EFFICIENCY), PY(ON_STATISTICS)",
      "x": 315,
      "y": 2,
      "width": 580,
      "height": 50,
      "antiAliasType": "normal",
      "shadow": {
        "$ref": { "file": "@xvm.xc", "path": "mainShadow" }
      },
      "textFormat": {
        "color": "0xF0F0F0",
        "font": "$FieldFont",
        "leading": 2,
        "size": 15
      },
      "format": "WN8: <font color='{{py:efficiency.wn8Color}}'>{{py:efficiency.wn8}}</font>  EFF: <font color='{{py:efficiency.effColor}}'>{{py:efficiency.eff}}</font>"
    },

    // Часы в углу экрана и таймер перед боем
    "timer": {
      "battle": {
        "updateEvent": "PY(ON_BATTLE_TIMER), PY(ON_BEGIN_BATTLE)",
        "x": 0, "y": -4, "width": 100, "height": 50, "screenHAlign": "right",
        "shadow": { "alpha": 45, "angle": 90, "blur": 4.1, "color": "0x000000", "distance": 1, "strength": 1.4 },
        "textFormat": { "align": "center", "color": "{{py:xvm.critTimeBT|#FFFFFF}}", "font": "$FieldFont", "size": 36 },
        "format": "{{py:isBattle?{{py:xvm.minutesBT%02d|00}}:{{py:xvm.secondsBT%02d|00}}}}"
      },

      "prebattle": {
        "$ref": { "path": "def.timer.battle" },
        "enabled": true,
        "x": 0, "y": 125, "width": 400, "height": 130, "screenHAlign": "center",
        "textFormat": { "align": "center", "color": "0xFFFFFF", "font": "$FieldFont", "size": 51 },
        "format": "{{py:isBattle? |<font size='35'>{{py:xvm.secondsBT=0?{{l10n:Battle starts}}|{{py:xvm.secondsBT>0?{{l10n:Battle starts in}}|{{l10n:Awaiting players}}}}</font>\n{{py:xvm.minutesBT%02d~:}}{{py:xvm.secondsBT%02d}}}}}}"
      }
    },

    // Тикающий счетчик секунд под прицелом
    "timerSeconds": {
      "updateEvent": "PY(ON_EVERY_SECOND), PY(ON_BATTLE_TIMER)",
      "x": 25,
      "y": 62,
      "width": 200,
      "height": 100,
      "screenHAlign": "center",
      "screenVAlign": "center",
      "alpha": 50,
      "shadow": {
        "$ref": { "file": "@xvm.xc", "path": "mainShadow" }
      },
      "textFormat": {
        "align": "left",
        "leading": -12,
        "color": "#FFFFFF",
        "font": "$FieldFont",
        "size": 24
      },
      /* "format": "•••••\n•••••" */
      "format": "{{.texts.seconds.{{py:xvm.formatDate('%S')}}}}"
    },

    // Лог нанесенного урона и ассиста
    "hitLog": {
      "updateEvent": "ON_PANEL_MODE_CHANGED, PY(ON_HIT_LOG), PY(ON_TOTAL_EFFICIENCY), PY(ON_ASSIST_LOG)",
      "x": "{{py:sum({{pp.widthLeft}},65)}}",
      "y": "{{battletype-key=epic_battle?138|66}}",
      "width": 380,
      "height": 800,
      "shadow": {
        "$ref": { "file": "@xvm.xc", "path": "mainShadow" }
      },
      "textFormat": {
        "color": "0xF4EFE8",
        "font": "$FieldFont",
        "leading": -2,
        "size": 13
      },
      "format": "<font face='$TitleFont' size='15'>{{py:xvm.hitLog.log.bg}}</font>\n<font size='7'>&nbsp;</font>\n{{py:xvm.hitLog.log}}\n<font size='20'>&nbsp;</font>\n<font face='$TitleFont' size='15'>{{py:xvm.assistLog_Background}}</font>\n<font size='7'>&nbsp;</font>\n{{py:xvm.assistLog}}"
    },

    // Дамаг лог
    "damageLog": {
      "header": {
        "updateEvent": "PY(ON_IMPACT), PY(ON_TOTAL_EFFICIENCY), PY(ON_PANEL_MODE_CHANGED)",
        "x": 245,
        "y": 852,
        "width": 160,
        "height": 80,
        "screenVAlign": "top",
        "hotKeyCode": 56,
        "visibleOnHotKey": false,
        "onHold": true,
        "alpha": "{{py:xvm.damagesSquad>0?100}}",
        "shadow": {
          "$ref": { "file": "@xvm.xc", "path": "mainShadow" }
        },
        "textFormat": {
          "color": "0xF0F0F0",
          "font": "$TitleFont",
          "leading": 3,
          "size": 15
        },
        "format": "<img src='cfg://dar/img/log/blocked.png' width='16' height='16' vspace='-2'>{{py:xvm.totalBlocked}}"
      },
      "history": {
        "enabled": true,
        "updateEvent": "PY(ON_HIT)",
        "x": 246,
        "y": "{{py:xvm.screenWidth<1600?-39|18}}",
        "width": 380,
        "height": "{{py:xvm.screenWidth<1600?175|225}}",
        "screenVAlign": "bottom",
        "shadow": {
          "$ref": { "file": "@xvm.xc", "path": "mainShadow" }
        },
        "textFormat": {
          "color": "0xF0F0F0",
          "font": "$FieldFont",
          "leading": -2,
          "size": 13
        },
        "format": "{{py:xvm.damageLog.log}}"
      },

      // Последний нанесенный урон
      "lastHit": {
        "enabled":true,
        "updateEvent": "PY(ON_LAST_HIT), PY(ON_MARKER_POSITION), PY(ON_AIM_MODE)",
        "x": -310,
        "y": "{{py:aim.y(148)}}",
        "width": 500,
        "height": 210,
        "screenHAlign": "center",
        "screenVAlign": "center",
        "shadow": {
          "alpha": 45,
          "angle": 90,
          "blur": 4,
          "color": "0x000000",
          "distance": 1,
          "strength": 2
        },
        "textFormat": {
          "align": "right",
          "color": "0xF0F0F0",
          "font": "$FieldFont",
          "bold": true,
          "leading": 0,
          "size": 35
        },
        "format": "{{py:xvm.damageLog.lastHit}}"
      }
    },

    // Маркер направления, откуда был выстрел
    "damageIndicator": {
      "enabled": true,
      "updateEvent": "PY(ON_DAMAGE_INDICATOR)",
      "x": 0,
      "y": 65,
      "width": 363,
      "height": 90,
      "alpha": "{{py:xvm.damageIndicator}}",
      "screenHAlign": "center",
      "format": "<img src='cfg://dar/img/{{py:xvm.damageIndicator_aim|CentrDI}}.png'>"
    },

    // Таймеры починки модулей в панели повреждений
    "repairTime": {
      "defaultItem": {
        "width": 47, "height": 40, "screenVAlign": "bottom",
        "shadow": { "alpha": 90, "angle": 90, "blur": 5, "color": "0x000000", "distance": 1, "strength": 3 },
        "textFormat": { "align": "center", "color": "0xF0F0F0", "bold": true, "font": "$FieldFont", "size": 17, "valign": "center" }
      },
      "engine": {
        "$ref": { "path": "def.repairTime.defaultItem" },
        "updateEvent": "PY(ON_ENGINE_UPDATE)",
        "x": 4, "y": -147,
        "format": "{{py:repairTimeEngine}}"
      },
      "gun": {
        "$ref": { "path": "def.repairTime.defaultItem" },
        "updateEvent": "PY(ON_GUN_UPDATE)",
        "x": 4, "y": -69,
        "format": "{{py:repairTimeGun}}"
      },
      "turret": {
        "$ref": { "path": "def.repairTime.defaultItem" },
        "updateEvent": "PY(ON_TURRETROTATOR_UPDATE)",
        "x": 4, "y": -30,
        "format": "{{py:repairTimeTurret}}"
      },
      "tracks": {
        "$ref": { "path": "def.repairTime.defaultItem" },
        "updateEvent": "PY(ON_COMPLEX_UPDATE)",
        "x": 177, "y": -147,
        "format": "{{py:repairTimeComplex}}"
      },
      "surveying": {
        "$ref": { "path": "def.repairTime.defaultItem" },
        "updateEvent": "PY(ON_SURVEYINGDEVICE_UPDATE)",
        "x": 177, "y": -108,
        "format": "{{py:repairTimeSurveying}}"
      },
      "radio": {
        "$ref": { "path": "def.repairTime.defaultItem" },
        "updateEvent": "PY(ON_RADIO_UPDATE)",
        "x": 177, "y": -69,
        "format": "{{py:repairTimeRadio}}"
      }
    },

    // УГН
    "angleAimingLeft": {
      "enabled": true,
      "updateEvent": "PY(ON_ANGLES_AIMING), PY(ON_AIM_MODE)",
      "x": "{{py:anglesAiming.left}}",
      "y": "{{py:aim.y}}",
      "width": 282,
      "height": 126,
      "screenHAlign": "center",
      "screenVAlign": "center",
      "format": "{{py:anglesAiming.left>-5?<img src='cfg://dar/img/angles/Left_limit.png'>|<img src='cfg://dar/img/angles/Left.png'>}}"
    },
    "angleAimingRight": {
      "enabled": true,
      "updateEvent": "PY(ON_ANGLES_AIMING), PY(ON_AIM_MODE)",
      "x": "{{py:anglesAiming.right}}",
      "y": "{{py:aim.y}}",
      "width": 282,
      "height": 126,
      "screenHAlign": "center",
      "screenVAlign": "center",
      "format": "{{py:anglesAiming.right<5?<img src='cfg://dar/img/angles/Right_limit.png'>|<img src='cfg://dar/img/angles/Right.png'>}}"
    }
  }
}