﻿/**
  * Log of the received damage.
  * For additional settings see battleLabelsTemplates.xc
  * Лог полученного урона.
  * Дополнительные настройки см. в battleLabelsTemplates.xc

  Macros used in damageLog:
  Макросы используемые в damageLog:

    {{number}}         - line number / номер строки.
    {{dmg}}            - received damage / полученный урон.
    {{dmg-kind}}       - kind of the received damage (attack, fire, ramming, ...) / тип полученного урона (атака, пожар, таран, ...).
    {{c:dmg-kind}}     - color by damage kind / цвет по типу урона.
    {{hit-effects}}    - hit kind (with damage, ricochet, not penetrated, no damage) / тип попадания (с уроном, рикошет, не пробито, без урона).
    {{c:hit-effects}}  - color by hit kind / цвет по типу попадания.
    {{type-shell}}     - shell kind / тип снаряда.
    {{c:type-shell}}   - color by shell kind / цвет по типу снаряда.
    {{vtype}}          - vehicle type / тип техники.
    {{c:vtype}}        - color by vehicle type / цвет по типу техники.
    {{team-dmg}}       - team attachment of the attacker (ally , enemy, self damage) / командная принадлежность атакующего (союзник, противник, урон по себе).
    {{c:team-dmg}}     - color by team attachment of the attacker (ally , enemy, self damage) / цвет по командной принадлежности атакующего (союзник, противник, урон по себе).
    {{costShell}}      - shell currency (gold, credits) / валюта снаряда (золото, кредиты).
    {{c:costShell}}    - color by shell currency / цвет по валюте снаряда.
    {{vehicle}}        - attacker vehicle name / название техники атакующего.
    {{name}}           - attacker nickname / никнейм атакующего.
    {{critical-hit}}   - critical hit / критическое попадание.
    {{comp-name}}      - vehicle part that was hit (turret, hull, chassis, gun) / часть техники, в которую было попадание (башня, корпус, ходовая, орудие).
    {{clan}}           - clan name with brackets (empty if no clan) / название клана в скобках (пусто, если игрок не в клане).
    {{level}}          - vehicle level / уровень техники.
    {{clannb}}         - clan name without brackets / название клана без скобок.
    {{clanicon}}       - macro with clan emblem image path value / макрос со значением пути эмблемы клана.
    {{squad-num}}      - number of squad (1 ,2, ...), empty if not in squad / номер взвода (1, 2, ...), пусто - если игрок не во взводе.
    {{dmg-ratio}}      - received damage percent / полученный урон в процентах.
    {{splash-hit}}     - text for damage with shell splinters (HE/HESH) / текст при уроне осколками снаряда (ОФ/ХФ).
    {{my-alive}}       - value 'al' for alive own vehicle, '' for dead one / возвращает 'al', для живой собственной техники, '' для мертвой.
    {{reloadGun}}      - gun reloading time / время перезарядки орудия.
    {{gun-caliber}}    - gun caliber / калибр орудия.
    {{wn8}}, {{xwn8}}, {{wtr}}, {{xwtr}}, {{eff}}, {{xeff}}, {{wgr}}, {{xwgr}}, {{xte}}, {{r}}, {{xr}} - statistics macros (see macros.txt) / макросы статистики (смотрите macros_ru.txt).
    {{c:wn8}}, {{c:xwn8}}, {{c:wtr}}, {{c:xwtr}}, {{c:eff}}, {{c:xeff}}, {{c:wgr}}, {{c:xwgr}}, {{c:xte}}, {{c:r}}, {{c:xr}} - color according to the corresponding statistics macro (see macros.txt) / цвет по соответствующему макросу статистики (смотрите macros_ru.txt).
    {{fire-duration}}  - duration of fire ("groupDamagesFromFire" must be enabled to work) / продолжительность пожара (работает только при включенной опции "groupDamagesFromFire").
    {{diff-masses}}    - vehicles weights difference during collision / разность масс техники при столкновении.
    {{nation}}         - vehicle nation / нация техники.
    {{my-blownup}}     - value 'blownup' if own vehicle's ammunition have been blown up, '' otherwise / возвращает 'blownup', если взорван боекомплект собственной техники, иначе ''.
    {{stun-duration}}  - stun duration / продолжительность оглушения.
    {{crit-device}}    - damaged module or shell-shocked crew member / поврежденный модуль или контуженный член экипажа.
    {{type-shell-key}} - shell kind table key value / название ключа таблицы типа снаряда.
    {{hitTime}}        - time of the received (blocked) damage in "mm:ss" format / время полученного (заблокированного) урона в формате "мм:сс".
    {{vehiclename}}    - vehicle system name (usa-A34_M24_Chaffee) / название техники в системе (usa-A34_M24_Chaffee).
*/

{
  "damageLog": {
    // true - отключить стандартный детальный урон.
    "disabledDetailStats": true,
    // true - отключить стандартный суммарный урон.
    "disabledSummaryStats": true,
    "log": {
      // true - разрешить перемещение лога в бою и запретить макросы в настройках "x" и "y".
      // false - запретить перемещение лога в бою и разрешить макросы в настройках "x" и "y".
      "moveInBattle": false,
      // true - отображать попадания без урона.
      "showHitNoDamage": true,
      // true - суммировать повреждения от пожара.
      "groupDamagesFromFire": true,
      // true - суммировать повреждения от тарана, столкновения, падения (если больше одного повреждения в секунду).
      "groupDamagesFromRamming_WorldCollision": true,
      // Тип полученного урона (макрос {{dmg-kind}}).
      "dmg-kind": {
        // shot / попадание.
        "shot": "{{l10n:{{hit-effects}}}}\t\t{{.damageLog.log.name.{{type-shell-key}}}}\t",
        // fire / пожар.
        "fire": "{{l10n:fire}}\t&#x7E;{{fire-duration%2.01f}} {{l10n:s}}\t\t",
        // ramming / таран.
        "ramming": "{{l10n:ramming}}\t\t{{l10n:n/a}}\t",
        // world collision / столкновение с объектами, падение.
        "world_collision": "{{l10n:strike}}\t\t{{l10n:n/a}}\t",
        // overturn / опрокидывание.
        "overturn": "{{l10n:overturn}}\t\t{{l10n:n/a}}\t",
        // drowning / утопление.
        "drowning": "{{l10n:drowning}}\t\t{{l10n:n/a}}\t",
        // art attack / артиллерийская поддержка.
        "art_attack": "{{l10n:{{hit-effects}}}}\t\t{{l10n:HE}}\t<font alpha='#E6' face='xvm' size='19'>&#x110;</font>\t{{l10n:Artattack}}",
        // air strike / поддержка авиации.
        "air_strike": "{{l10n:{{hit-effects}}}}\t\t{{l10n:PTAB}}\t<font alpha='#E6' face='xvm' size='19'>&#x111;</font>\t{{l10n:Airstrike}}"
      },
      "c:dmg-kind": {
        "shot": "{{my-blownup|{{c:type-shell}}}}",
        "fire": "fire",
        "ramming": "ramming",
        "world_collision": "strike",
        "drowning": "drowning",
        "overturn": "overturn",
        "art_attack": "{{my-blownup|{{c:type-shell}}}}",
        "air_strike": "{{my-blownup|{{c:type-shell}}}}"
      },
      // Урон осколками снаряда (ОФ/ХФ). (макрос {{splash-hit}}).
      "splash-hit": {
        "splash": "splash",
        "no-splash": ""
      },
      "name": {
        "armor_piercing": "{{l10n:AP}}",
        "high_explosive": "{{l10n:HE}}",
        "armor_piercing_cr": "{{l10n:APCR}}",
        "armor_piercing_he": "{{l10n:HESH}}",
        "hollow_charge": "{{l10n:HEAT}}",
        "not_shell": ""
      },
      // Тип снаряда (макрос {{type-shell}}).
      "type-shell": {
        "armor_piercing": "pierced",
        "high_explosive": "{{splash-hit|debris}}",
        "armor_piercing_cr": "pierced",
        "armor_piercing_he": "{{splash-hit|debris}}",
        "hollow_charge": "pierced",
        "not_shell": "{{splash-hit|debris}}"
      },
      "c:type-shell": {
        "armor_piercing": "{{dmg=0?nullpierced|pierced}}",
        "high_explosive": "{{splash-hit|{{dmg=0?nulldebris|debris}}}}",
        "armor_piercing_cr": "{{dmg=0?nullpierced|pierced}}",
        "armor_piercing_he": "{{splash-hit|{{dmg=0?nulldebris|debris}}}}",
        "hollow_charge": "{{dmg=0?nullpierced|pierced}}",
        "not_shell": "{{splash-hit|{{dmg=0?nulldebris|debris}}}}"
      },
      // Тип техники (макрос {{vtype}}).
      "vtype": {
        "HT": "<font alpha='#E6' face='xvm'>&#x3F;</font>",
        "MT": "<font alpha='#E6' face='xvm'>&#x3B;</font>",
        "LT": "<font alpha='#E6' face='xvm'>&#x3A;</font>",
        "TD": "<font alpha='#E6' face='xvm'>&#x2E;</font>",
        "SPG": "<font alpha='#E6' face='xvm'>&#x2D;</font>",
        "not_vehicle": ""
      },
      // Цвет по типу техники (макрос {{c:vtype}}).
      "c:vtype": {
        "HT":  "#DDD0C6",        // heavy tank / тяжёлый танк.
        "MT":  "#FFF198",        // medium tank / средний танк.
        "LT":  "#A2FF9A",        // light tank / лёгкий танк.
        "TD":  "#A0CFFF",        // tank destroyer / ПТ-САУ.
        "SPG": "#FFACAC",        // SPG / САУ.
        "not_vehicle": "#CCCCCC" // another source of damage / другой источник урона.
      },
      // Тип попадания (макрос {{hit-effects}}).
      "hit-effects": {
        // penetrated / пробито.
        "armor_pierced": "{{my-blownup|{{type-shell}}}}",
        "intermediate_ricochet": "ricochet",
        "final_ricochet": "ricochet",
        "armor_not_pierced": "bounce",
        "armor_pierced_no_damage": "{{splash-hit|blocked}}",
        "unknown": "{{splash-hit|blocked}}"
      },
      "critical-hit": {
        "critical": "*",
        "no-critical": ""
      },
      "crit-device": {
        "engine_crit": "{{l10n:critical}}",
        "ammo_bay_crit": "{{l10n:critical}}",
        "fuel_tank_crit": "{{l10n:critical}}",
        "radio_crit": "{{l10n:critical}}",
        "left_track_crit": "{{l10n:critical}}",
        "right_track_crit": "{{l10n:critical}}",
        "gun_crit": "{{l10n:critical}}",
        "turret_rotator_crit": "{{l10n:critical}}",
        "surveying_device_crit": "{{l10n:critical}}",
        "engine_destr": "{{l10n:critical}}",
        "ammo_bay_destr": "{{l10n:critical}}",
        "fuel_tank_destr": "{{l10n:critical}}",
        "radio_destr": "{{l10n:critical}}",
        "left_track_destr": "{{l10n:critical}}",
        "right_track_destr": "{{l10n:critical}}",
        "gun_destr": "{{l10n:critical}}",
        "turret_rotator_destr": "{{l10n:critical}}",
        "surveying_device_destr": "{{l10n:critical}}",
        "commander": "{{l10n:critical}}",
        "driver": "{{l10n:critical}}",
        "radioman": "{{l10n:critical}}",
        "gunner": "{{l10n:critical}}",
        "loader": "{{l10n:critical}}",
        "no-critical": ""
      },
      "c:team-dmg": {
        "ally-dmg": "#00EAFF",
        "enemy-dmg": "{{c:costShell}}",
        "player": "#B9FFA1",
        "unknown": "#F0F0F0"
      },
      "costShell": {
        "gold-shell": "<img src='cfg://dar/img/log/gold.png' width='47' height='88' vspace='-18'>",
        "silver-shell": "<img src='cfg://dar/img/log/none.png' width='1' height='88' vspace='-18'>",
        "unknown": "<img src='cfg://dar/img/log/none.png' width='1' height='88' vspace='-18'>"
      },
      "c:costShell": {
        "gold-shell": "#FFD582",
        "silver-shell": "#F0F0F0",
        "unknown": "#F0F0F0"
      },
      "formatHistory": "<font color='{{c:team-dmg}}'><textformat tabstops='[26,58,114,121,145,164]'>{{my-alive?<font face='mono'>{{number%02d}}</font>{{critical-hit}}|<font alpha='#E6' face='xvm' size='15'>&#x2B;</font>}}\t{{dmg=0?<img src='cfg://dar/img/log/{{c:team-dmg}}/arrow.png' width='27' height='19' vspace='-5'>|-{{dmg}}}}\t{{dmg-kind}}<font size='19'>{{vtype}}</font>\t{{vehicle}}</textformat></font>"
    },
    
    "logBackground": {
      "$ref": { "path": "damageLog.log" },
      "formatHistory": "<textformat leading='-14'><img src='cfg://dar/img/log/bg{{dmg=0?No|}}Damage.png' width='172' height='34'></textformat>"
    },

    "logAlt": {
      "$ref": { "path": "damageLog.log" },
      "dmg-kind": {
        "shot": "{{{{gun-caliber%.1f}}={{gun-caliber%.0f}}?{{gun-caliber%.0f}}|{{gun-caliber%.1f}}}} {{l10n:mm}}\t\t{{.damageLog.log.name.{{type-shell-key}}}}\t",
        "fire": "{{l10n:fire}}\t&#x7E;{{fire-duration%2.01f}} {{l10n:s}}\t\t",
        "ramming": "{{l10n:ramming}}\t\t{{l10n:n/a}}\t",
        "world_collision": "{{l10n:strike}}\t\t{{l10n:n/a}}\t",
        "overturn": "{{l10n:overturn}}\t\t{{l10n:n/a}}\t",
        "drowning": "{{l10n:drowning}}\t\t{{l10n:n/a}}\t",
        "art_attack": "{{{{gun-caliber%.1f}}={{gun-caliber%.0f}}?{{gun-caliber%.0f}}|{{gun-caliber%.1f}}}} {{l10n:mm}}\t\t{{l10n:HE}}\t<font alpha='#E6' face='xvm' size='19'>&#x110;</font>\t{{l10n:Artattack}}",
        "air_strike": "{{{{gun-caliber%.1f}}={{gun-caliber%.0f}}?{{gun-caliber%.0f}}|{{gun-caliber%.1f}}}} {{l10n:mm}}\t\t{{l10n:PTAB}}\t<font alpha='#E6' face='xvm' size='19'>&#x111;</font>\t{{l10n:Airstrike}}"
      },
      "formatHistory": "<font color='{{c:team-dmg}}'><textformat tabstops='[26,58,140,147,171,190]'>{{my-alive?<font face='mono'>{{number%02d}}</font>{{critical-hit}}|<font alpha='#E6' face='xvm' size='15'>&#x2B;</font>}}\t{{dmg=0?<img src='cfg://dar/img/log/{{c:team-dmg}}/arrow.png' width='27' height='27' vspace='-9'>|-{{dmg-ratio%d%%}}}}\t<img src='cfg://dar/img/log/{{c:team-dmg}}/{{c:dmg-kind}}.png' width='27' height='27' vspace='-9'>{{dmg-kind}}<font size='21'>{{vtype}}</font>\t{{name%.13s~..}}</textformat></font>"
    },
    "logAltBackground": {
      "$ref": { "path": "damageLog.logBackground" }
    },
    "lastHit": {
      "$ref": { "path": "damageLog.log" },
      "timeDisplayLastHit": "{{c:dmg-kind}}",
      "dmg-kind": {
        "shot": "<font color='{{c:vtype}}'>{{vtype}} {{vehicle}}</font>\n<font color='{{c:costShell}}'>{{hit-effects}}\n{{my-alive?|{{l10n:{{my-blownup?Blown-Up|Vehicle destroyed}}}}}}</font>",

        // Пожар
        "fire": "<textformat leading='-1'><font color='{{c:costshell}}'>{{my-alive?<font color='#FF6600'>{{l10n:Vehicle on fire}}</font>|{{l10n:Vehicle burnt out}}}}</font>\n</textformat><textformat leading='-12'>-{{dmg}}\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font></textformat>",

        // Таран
        "ramming": "<textformat leading='-1'><font color='{{c:costshell}}'>{{my-alive?|{{l10n:Vehicle destroyed}}}}</font>\n</textformat><textformat leading='-12'>{{dmg>0?-{{dmg}}  <font color='{{crit-device?{{c:costshell}}}}'>{{l10n:ramming}}</font>|{{l10n:Ramming critical}}}}\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font></textformat>",
        
        // Удар
        "world_collision": "<textformat leading='-1'><font color='{{c:costshell}}'>{{my-alive?|{{l10n:Vehicle crashed}}}}</font>\n</textformat><textformat leading='-12'>{{dmg>0?-{{dmg}}  <font color='{{crit-device?{{c:costshell}}}}'>{{l10n:crash}}</font>|{{l10n:Strike critical}}}}\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font></textformat>",
        
        // Опрокинут
        "overturn": "<textformat leading='-1'><font color='{{c:costshell}}'>{{l10n:Vehicle destroyed}}</font>\n</textformat><textformat leading='-12'>-{{dmg}}  {{l10n:overturn}}\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font></textformat>",
        
        // Машина затоплена
        "drowning": "<textformat leading='-1'><font color='{{c:costshell}}'>{{l10n:Vehicle drowned}}</font>\n<font color='{{c:team-dmg}}'>{{vtype}} <font size='35'>{{vehicle}}</font></font></textformat>",
        
        // Артобстрел
        "art_attack": "<font color='{{c:costshell}}'>{{my-alive?|{{l10n:{{my-blownup?Blown-Up|Vehicle destroyed}}}}}}</font>\n{{hit-effects}} {{l10n:HE}}\n<font color='{{c:team-dmg}}'><font face='xvm'>&#x110;</font>  <font size='35'>{{l10n:Artattack}}</font></font>",
        
        // Авиаудар
        "air_strike": "<font color='{{c:costshell}}'>{{my-alive?|{{l10n:{{my-blownup?Blown-Up|Vehicle destroyed}}}}}}</font>\n{{hit-effects}} {{l10n:PTAB}}\n<font color='{{c:team-dmg}}'><font face='xvm'>&#x111;</font>  <font size='35'>{{l10n:Airstrike}}</font></font>"
      },
      "c:dmg-kind": {
        "shot": "10",
        "fire": "{{my-alive?1|10}}",
        "ramming": "10",
        "world_collision": "10",
        "drowning": "10",
        "overturn": "10",
        "art_attack": "10",
        "air_strike": "10"
      },
      "critical-hit": {
        "critical": "<font color='{{c:costshell}}'>{{py:capitalize('{{l10n:critical}}')}}</font> {{splash-hit|{{l10n:module}}}}",
        "no-critical": "{{py:capitalize('{{l10n:blocked}}')}} "
      },
      "splash-hit": {
        "splash": "<font face='xvm' size='81'>&#x117;</font>",
        "no-splash": ""
      },
      "hit-effects": {
        "armor_pierced": "-{{dmg}} {{crit-device}}",
        "intermediate_ricochet": "{{py:capitalize('{{l10n:ricochet}}')}} ",
        "final_ricochet": "{{py:capitalize('{{l10n:ricochet}}')}} ",
        "armor_not_pierced": "{{py:capitalize('{{l10n:bounce}}')}} ",
        "armor_pierced_no_damage": "{{critical-hit}}",
        "unknown": "{{critical-hit}}"
      },
      "c:team-dmg": {
        "ally-dmg": "#00EAFF",
        "enemy-dmg": "{{c:vtype}}",
        "player": "#B9FFA1",
        "unknown": "#FF8518"
      },
      "formatLastHit": "{{dmg-kind}}"
    }
  }
}