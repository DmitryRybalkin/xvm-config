﻿/**
 * Специальные горячие клавиши XVM
 * Конфиг: "dar"
 */
{
  "hotkeys": {
    "minimapZoom":         { "enabled": false },
    "minimapAltMode":      { "enabled": true, "keyCode": 56, "onHold": true},
    "markersAltMode":      { "enabled": true, "keyCode": 56, "onHold": true },
    "playersPanelAltMode": { "enabled": true, "keyCode": 56, "onHold": true },
    "damageLogAltMode":    { "enabled": true, "keyCode": 56, "onHold": true },
    "hitLogAltMode":       { "enabled": true, "keyCode": 56, "onHold": true },
    "assistLogAltMode":    { "enabled": true, "keyCode": 56, "onHold": true } 
  }
}