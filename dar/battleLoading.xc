﻿/**
 * Параметры экрана загрузки боя
 * Конфиг: "dar"
 */
{
  "battleLoading": {
    "darkenNotReadyIcon": false,
    "removeVehicleLevel": true,
    "removeVehicleTypeIcon": true,
    "removeRankBadgeIcon": true,
    "squadIconOffsetXLeft": 1,
    "squadIconOffsetXRight": 0,
    "vehicleIconOffsetXLeft": 1,
    "vehicleIconOffsetXRight": -2,
    "formatLeftNick": "",
    "formatRightNick": "",
    "formatLeftVehicle": "",
    "formatRightVehicle": "",
    "extraFieldsLeft": [
      ${ "def.rankBadgeIcon" },
      ${ "def.nickNameClanLeft" },
      //${ "def.statisticsWinrate" },
      ${ "def.statisticsRatingLeft" },
      ${ "def.vehicleNameLeft" },
      ${ "def.vehicleType" },
      ${ "def.friendIcon" },
      ${ "def.bgLeft" }
    ],
    "extraFieldsRight": [
      ${ "def.rankBadgeIcon" },
      ${ "def.nickNameClanRight" },
      //${ "def.statisticsWinrate" },
      ${ "def.statisticsRatingRight" },
      ${ "def.vehicleNameRight" },
      ${ "def.vehicleType" },
      ${ "def.friendIcon" },
      ${ "def.bgRight" }
    ]
  },
  "def": {
    "defaultItem": {
      "bindToIcon": true,
      "shadow": { "enabled": false },
      "textFormat": { "color": "#{{player?FFB964|{{squad?FFB964|АААААА}}}}", "font": "$FieldFont", "size": 14 }
    },
    "rankBadgeIcon": {
      "$ref": { "path": "def.defaultItem" },
      "x": -379, "y": -2, "width": 30, "height": 30,
      "format": "<img src='img://gui/maps/icons/library/badges/24x24/badge_{{rankBadgeId}}.png' width='24' height='24'>"
    },
    "nickNameClanLeft": {
      "$ref": { "path": "def.defaultItem" },
      "x": -161, "y": 2, "width": 200, "height": 24,
      "format": "<font face='$FieldFont' color='#E5E5E5' alpha='{{alive?{{ready?#FF|#FF}}|#80}}'><font size='14' color='{{player?#FFCC66|{{squad?#FFB964|{{tk?#00EAFF}}}}}}'>{{.texts.nicknames.{{name}}|{{name%.18s~..}}}}  </font><font size='12' color='{{topclan?#FFCC66|#E5D39A}}'>{{clannb}}</font></font>"
    },
    "nickNameClanRight": {
      "$ref": { "path": "def.nickNameClanLeft" },
      "format": "<font face='$FieldFont' color='#E5E5E5' alpha='{{alive?{{ready?#FF|#80}}|#80}}'><font size='12' color='{{topclan?#FFCC66|#E5D39A}}'>{{clannb}}</font><font size='13'>  {{.texts.nicknames.{{name}}|{{name%.18s~..}}}}</font></font>"
    },
    "statisticsWinrate": {
      "$ref": { "path": "def.defaultItem" },
      "x": -63, "y": 3, "width": 34, "height": 24,
      "textFormat": { "align": "center", "size": "{{.options.battleLoadingShowStats=true?{{xvm-stat?13|0}}|0}}" },
      "format": "<font color='{{c:xr}}'>{{winrate%2d|--}}%</font>"
    },
    "statisticsRatingLeft": {
      "$ref": { "path": "def.statisticsWinrate" },
      "x": -3,
      "format": "<font color='{{c:xr}}'>{{r}}</font>"
    },
    "statisticsRatingRight": {
      "$ref": { "path": "def.statisticsRatingLeft" },
      "x": -3
    },
    "vehicleNameLeft": {
      "$ref": { "path": "def.defaultItem" },
      "x": "{{.options.battleLoadingShowStats=true?{{xvm-stat?-238|-181}}|-181}}", "y": 3, "width": 160, "height": 24,
      "format": "<font size='12'><p align='right'>{{vehicle}}</p></font>"
    },
    "vehicleNameRight": {
      "$ref": { "path": "def.vehicleNameLeft" },
      "format": "<font size='12'><p align='left'>{{vehicle}}</p></font>"
    },
    "vehicleType": {
      "$ref": { "path": "def.defaultItem" },
      "x": "{{.options.battleLoadingShowStats=true?{{xvm-stat?-76|-19}}|-19}}", "y": -2, "alpha": 80, "width": 24, "height": 24,
      "format": "<font face='xvm' size='21'>{{vtype}}</font>"
    },
    "friendIcon": {
      "$ref": { "path": "def.defaultItem" },
      "x": -2, "y": 2, "width": "{{friend?30|0}}", "height": 30,
      "format": "<img src='cfg://dar/img/shared/friend/{{tk?tk-}}{{alive|dead}}.png' width='19' height='17'>"
    },
    "bgLeft": {
      "x": -397, "y": -3, "width": 510, "height": "{{position=15?39|29}}", "bindToIcon": true, "layer": "bottom",
      "format": "<img src='cfg://dar/img/shared/battleLoading/{{player|left}}.png' width='500' height='36'>"
    },
    "bgRight": {
      "$ref": { "path": "def.bgLeft" },
      "format": "<img src='cfg://dar/img/shared/battleLoading/right.png' width='500' height='36'>"
    }
  }
}