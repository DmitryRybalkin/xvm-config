﻿/**
 * Надписи на миникарте.
 * Конфиг: H_E_K_P_O_M_A_H_T_
 */
{
  "labels": {
    "formats": [
      ${ "minimapLabelsTemplates.xc":"def.vehicleSpottedCompany" },
      ${ "minimapLabelsTemplates.xc":"def.nickSpottedCompany" },
      ${ "minimapLabelsTemplates.xc":"def.xmqpEvent" },

      ${ "minimapLabelsTemplates.xc":"def.hpSpotted" },
      ${ "minimapLabelsTemplates.xc":"def.hpSpottedDiagram" },

      ${ "minimapLabelsTemplates.xc":"def.hpLost" },
      ${ "minimapLabelsTemplates.xc":"def.hpLostDiagram" },
      ${ "minimapLabelsTemplates.xc":"def.vehicleLost" },
      ${ "minimapLabelsTemplates.xc":"def.vtypeLost" },

      ${ "minimapLabelsTemplates.xc":"def.vtypeDead" },
      ${ "minimapLabelsTemplates.xc":"def.vehicleDead" },

      ${ "minimapLabelsTemplates.xc":"def.rating" },
      ${ "minimapLabelsTemplates.xc":"def.nickLost" }
    ]
  }
}