﻿/**
 * Minimap labels.
 * Надписи на миникарте.
 */
{
  // Textfields for units on minimap.
  // Текстовые поля юнитов на миникарте.
  // Definitions.
  // Шаблоны.
 "def": {
    /** Формат поля по умолчанию. */
    "defaultItem": {
      "enabled": true,
      "x": 0, "y": 0,
      "width": 100,
      "height": 40,
      "alpha": 100,
      "align": "left",
      "valign": "top",
      "flags": [ "player", "ally", "squadman", "enemy", "teamKiller", "lost", "spotted", "alive", "dead" ], //"antiAliasType": "normal",
      "shadow": {
        "alpha": 80,
        "angle": 90,
        "blur": 2,
        "color": "0x000000",
        "distance": 0,
        "strength": 4
      }
    },

    /** Тип техники. */
    "vtypeSpotted": {
      "$ref": { "path": "def.defaultItem" },
      "align": "center",
      "valign": "center",
      "layer": "top",
      "flags": [ "ally", "enemy", "squadman", "teamKiller", "spotted", "alive" ],
      "textFormat": {
        "font": "xvm",
        "color": "{{.minimap.labelsData.colors.dot.{{sys-color-key}}}}",
        "size": 13,
        "align": "center",
        "valign": "center"
      },
      "format": "{{.minimap.labelsData.vtype.{{vtype-key}}}}"
    },
    /** Точка рейтинга */
    "rating": {
      "$ref": { "path": "def.defaultItem" },
      "x": 5, "y": 0,
      "align": "center",
      "valign": "center",
      "layer": "top",
      "flags": [ "ally", "enemy", "squadman", "teamKiller", "alive" ],
      "textFormat": {
        "font": "$FieldFont",
        "bold": true,
        "color": "{{c:xr}}",
        "size": 7,
        "align": "center",
        "valign": "center"
      },
      "format": "•"
    },

    "lowHp": {
      "x": 1,
      "y": 1,
      "width": 30,
      "height": 30,
      "align": "center",
      "valign": "center",
      "layer": "bottom",
      "alpha": "{{alive?{{hp-ratio<20?100|0}}|0}}",
      /* "alpha": "{{py:mul({{py:sight.damageShell}}, 0.75)}}", */
      /* "alpha": "{{py:mul(py:sight.damageShell, 0.75)}}", */
      "flags": [ "ally", "enemy", "squadman", "teamKiller", "spotted", "alive" ],
      "format": "<img src='cfg://dar/img/minimap/lowHp{{ally?Ally}}.png' width='24' height='24'>"
    },

    /** Название техники. */
    "vehicleSpotted": {
      "$ref": { "path": "def.defaultItem" },
      "x": 2,
      "y": "{{squad?5|-1}}",
      "flags": [ "ally", "enemy", "squadman", "teamKiller", "spotted", "alive" ],
      "textFormat": {
        "font": "$FieldFont",
        "bold": true,
        "color": "{{.minimap.labelsData.colors.txt.{{sys-color-key}}}}",
        "size": 6
      },
      "format": "{{vehicle}}"
    },

    /** Название техники, ротный конфиг. */
    "vehicleSpottedCompany": {
        "$ref": { "path": "def.vehicleSpotted" }
    },
    /** Ник игрока. */
    "nickSpotted": {
        "$ref": { "path": "def.defaultItem" },
        "x": 2,
        "y": "{{ally?{{battletype?5|{{squad?7|-1}}}}|-1}}",
        "flags": [ "squadman", "spotted", "alive" ],
        "textFormat": { "font": "$FieldFont", "bold": true, "color": "{{.minimap.labelsData.colors.txt.{{sys-color-key}}}}", "size": 6, "italic": true },
        "format": "{{name%.7s~..}}"
    },
    /** Ник игрока, ротный конфиг. */
    "nickSpottedCompany": {
        "$ref": { "path": "def.nickSpotted" },
        "flags": [ "ally", "squadman", "teamKiller", "spotted", "alive" ],
        "textFormat": {
            "font": "$FieldFont",
            "bold": true,
            "color": "{{squad?{{.minimap.labelsData.colors.txt.{{sys-color-key}}}}|{{tk?{{.minimap.labelsData.colors.txt.{{sys-color-key}}}}|#BFBFBF}}}}",
            "size": "{{battletype?6|{{squad?6|0}}}}"
        },
        "format": "{{name%.7s~..}}"
    },
    /** Маркер события XMQP (танк перевёрнут, в засвете). */
    "xmqpEvent": {
        "$ref": { "path": "def.defaultItem" },
        "x": 3, "y": -9, "flags": [ "ally", "squadman", "teamKiller", "spotted", "alive" ],
        "textFormat": { "font": "xvm", "color": "0xFFBB00", "size": 10 },
        "format": "{{x-spotted?&#x70;}}{{x-overturned?&#x112;}}"
    },

    /** Значок типа техники пропавшей из застета */
    "vtypeLost": {
      "$ref": { "path": "def.defaultItem" },
      "y": -0.4,
      "alpha": 75,
      "align": "center",
      "valign": "center",
      "flags": [ "ally", "enemy", "squadman", "teamKiller", "lost", "alive" ],
      "layer": "top",
      "textFormat": { "font": "xvm", "color": "{{.minimap.labelsData.colors.lostDot.{{sys-color-key}}}}", "size": 11, "align": "center", "valign": "center" },
      "format": "{{.minimap.labelsData.vtype.{{vtype-key}}}}"
    },

    /** Название техники пропавшей из застета */
    "vehicleLost": {
        "$ref": { "path": "def.defaultItem" },
        "x": 2, "y": -1, "alpha": 85, "flags": [ "ally", "enemy", "squadman", "teamKiller", "lost", "alive" ], "layer": "bottom",
        "textFormat": { "font": "$FieldFont", "bold": true, "color": "{{.minimap.labelsData.colors.lostDot.{{sys-color-key}}}}", "size": 6 },
        "format": "{{vehicle}}"
    },

    // Тип техники, мертвый.
    "vtypeDead": {
      "$ref": { "path":"def.defaultItem" },
      "alpha": 70,
      "align": "center",
      "valign": "center",
      "flags": [ "ally", "enemy", "squadman", "teamKiller", "dead" ],
      "layer": "bottom",
      "textFormat": {
        "font": "xvm",
        "size": 13,
        "align": "center",
        "valign": "center"
      },
      /* "format": "<font color='{{.minimap.labelsData.colors.lostDot.{{sys-color-key}}}}'>{{.minimap.labelsData.vtype.{{vtype-key}}}}</font>", */
      "format": "<img src='cfg://dar/img/minimap/dead/{{ally?ally|enemy}}-{{vtype-key}}.png' width='16' height='16'>",
      "shadow": {
        "$ref": { "path":"def.defaultItem.shadow" },
        "strength": 3
      }
    },

    /** Убитая техника. */
    "vehicleDead": {
      "$ref": { "path": "def.defaultItem" },
      "x": 2,
      "y": -1,
      "alpha": 100,
      "flags": [ "ally", "enemy", "squadman", "teamKiller", "dead" ],
      "layer": "bottom",
      "textFormat": {
        "font": "$FieldFont",
        "bold": true,
        "color": "{{ally?0x152008|0x260900}}",
        "size": 6
      },
      "shadow": {
        "alpha": 80,
        "angle": 90,
        "blur": 1,
        "color": "{{ally?0xddff99|0xff7c7c}}",
        "distance": 0,
        "strength": 2
      },
      "format": "{{vehicle}}"
    },

    /** Ник игрока. */
    "nickLost": {
      "$ref": { "path": "def.defaultItem" },
      "x": 2, "y": -9, "alpha": 85, "flags": [ "squadman", "lost", "alive" ], "layer": "bottom",
      "textFormat": { "font": "$FieldFont", "bold": true, "color": "{{.minimap.labelsData.colors.txt.{{sys-color-key}}}}", "size": 8, "italic": true },
      "format": "{{name%.7s~..}}"
    },

    /** ХП потерянного игрока. */
    "hpLost": {                    // ХП
      "enabled": true,
      "alpha": 100,
      "x": 0,
      "y": -3,
      "align": "center",
      "valign": "center",
      "flags": [ "ally", "enemy", "teamKiller", "squadman", "lost", "alive" ],
      "layer": "top",
      "textFormat": {
        "align": "left",
        "size": 5,
        "color": "{{.minimap.labelsData.colors.lostDot.{{sys-color-key}}}}"
      },
      "shadow": { "enabled": true, "distance": 0, "alpha": 100, "blur": 1.3, "strength": 2 },
      "format": "{{hp}}"
      //"format": "{{hp%-1s|----}}"
    },

    /** Круг с ХП потерянного игрока. */
    "hpLostDiagram": {
        "enabled": true,
        "x": 0, "y": 0.5, "alpha": "55", "align": "center", "valign": "center",
        "flags": [ "ally", "enemy", "squadman", "teamKiller", "lost", "alive" ],
        "layer": "bottom",
        "shadow": { "color": "0x000000", "alpha": 50, "blur": 2, "strength": 4 },
        "textFormat": {
          "font": "dynamic",
          "color": "{{.minimap.labelsData.colors.lostDot.{{sys-color-key}}}}",
          "size": "{{vtype-key=HT?18|16}}",
          "align": "center",
          "valign": "center"
        },
        "format": "{{hp-ratio%.335a|&#x1B3;}}"
    },

    // ХП
    "hpSpotted": {                    // ХП
      "enabled": true,
      "alpha": 100,
      "x": 0,
      "y": -3,
      "align": "center",
      "valign": "center",
      "flags": [ "ally", "enemy", "teamKiller", "squadman", "spotted", "alive" ],
      //"antiAliasType": "normal",
      "layer": "top",
      "textFormat": { "align": "left", "size": 5, "color": "{{.minimap.labelsData.colors.txt.{{sys-color-key}}}}" },
      "shadow": { "enabled": true, "distance": 0, "alpha": 100, "blur": 1.3, "strength": 2 },
      "format": "{{hp}}"
      //"format": "{{hp%-1s|----}}"
    },

    /** Круг с ХП игрока. */
    "hpSpottedDiagram": {
        "enabled": true,
        "x": 0,
        "y": 0.5,
        "alpha": "50",
        "align": "center",
        "valign": "center",
        "flags": [ "ally", "squadman", "enemy", "teamKiller", "spotted", "alive" ], "layer": "bottom",
        "shadow": { "color": "0x000000", "alpha": 50, "blur": 1, "strength": 1 },
        "textFormat": { "font": "dynamic", "color": "{{.minimap.labelsData.colors.dot.{{sys-color-key}}}}", "size": "{{vtype-key=HT?18|16}}", "align": "center", "valign": "center" },
        "format": "{{hp-ratio%.335a|&#x1B3;}}"
    }
  }
}
