﻿/**
 * Параметры ангара
 * Конфиг: "dar"
 */
{
  "hangar": {
    "restoreBattleType": true,
    "barracksShowSkills": true,
    "crewReturnByDefault": true,
    "enableCrewAutoReturn": true,
    "enableEquipAutoReturn": true,
    "enableGoldLocker": true,
    "enableFreeXpLocker": true,
    "enableCrystalLocker": true,
    "notificationsButtonType": "none",
    "showCreateSquadButtonText": false,
    "showReferralButton": false,
    "showPremiumShopButton": false,
    "showPromoPremVehicle": false,
    "hideActiveBooster": true,
    "combatIntelligence": {
      "showPopUpMessages": true,
      "showUnreadCounter": false
    },
    // Счетчик уведомлений в верхней панели - Header notifications count
    // true - показать (по умолчанию), false - убрать с глаз долой
    "notificationCounter": {
      "hangar" :           false, // Ангар
      "shop" :             false, // Магазин
      "missions" :         false, // Задачи
      "personalMissions" : false, // Кампании
      "profile" :          false, // Достижения
      "techtree" :         false, // Исследования
      "barracks" :         false, // Казарма
      "stronghold" :       false  // Укрепрайон
    },
    "hideMessagePatterns": [
      "Добро пожаловать на сервер",
      "Уважаемый пользователь! Рекомендуем выполнить привязку вашего аккаунта к",
      "Уважаемый пользователь! Вы не меняли пароль уже 30 дней. Рекомендуем сменить",
      "Ремонт:",
      "Куплено:",
      "Продано:",
      "Кастомизация:",
      "Исследовано:",
      "Демонтировано:",
      "Модуль",
      "Оборудование",
      "Высадка экипажа в Казарму выполнена успешно",
      "Высадка члена экипажа в Казарму выполнена успешно",
      "Пересадка экипажа выполнена успешно",
      "Пересадка члена экипажа выполнена успешно",
      "Замена члена экипажа выполнена успешно",
      "Член экипажа демобилизован успешно",
      "Ускоренное обучение экипажа",
      "Новое умение успешно изучено",
      "Скриншот сохранён",
      "Жалоба на",
      "Приглашение принято",
      "Приглашение отклонено",
      "Приглашение недействительно",
      "Время действия личного резерва",
      "Изменены элементы внешнего вида"
    ],
    "carousel": ${ "carousel.xc": "carousel" },
    "widgets": ${ "widgets.xc": "widgets.lobby" }
  }
}