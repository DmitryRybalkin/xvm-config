﻿# imports

from xfw import *
from xfw_actionscript.python import *
import BigWorld
import xvm_battle.python.fragCorrelationPanel as panel

# constants

actual_arenaUniqueID = None
max_hp = 0
section = 25
percent_filling = 100. / section #4

# events

@registerEvent(panel, 'update_hp')
def update_hp(vehicleID, hp):
    as_event('ON_UPDATE_HP')

# outer

@xvm.export('current_hp', deterministic=False)
def current_hp(current_team):
    return panel.teams_totalhp[current_team]

@xvm.export('max_hp_symbols', deterministic=True)
def max_hp_symbols(symbol):
    return str(symbol) * section

@xvm.export('current_hp_symbols', deterministic=False)
def current_hp_symbols(current_team, symbol):
    return percent_hp_section(current_team) * str(symbol) if percent_hp_section(current_team) is not None else section

@xvm.export('total_hp.superiority_text', deterministic=False)
def superiority_text():
    al = panel.teams_totalhp[0]
    en = panel.teams_totalhp[1]
    max_hp_now = max(panel.teams_totalhp)
    return "<font color='#%s'>%s&#037;</font>" % (color(), (al - en) * 100 / max_hp_now) if max_hp_now != 0 else None

# internal

#@xvm.export('percent_hp', deterministic=False)
def percent_hp(current_team):
    global actual_arenaUniqueID, max_hp
    arenaUniqueID = BigWorld.player().arenaUniqueID
    if actual_arenaUniqueID != arenaUniqueID:
      actual_arenaUniqueID = arenaUniqueID
      max_hp = 0
    if panel.teams_totalhp[current_team] > max_hp:
        max_hp = panel.teams_totalhp[current_team]
    return current_hp(current_team) / float(max_hp) * 100 if max_hp != 0 else None

#@xvm.export('percent_hp_section', deterministic=False)
def percent_hp_section(current_team):
    return int(round(percent_hp(current_team) / percent_filling)) if percent_hp(current_team) is not None else None

#@xvm.export('total_hp.color', deterministic=False)
def color():
    return panel.total_hp_color
