import BigWorld
from gui.Scaleform.daapi.view.lobby.messengerBar.VehicleCompareCartButton import VehicleCompareCartButton

# Authors:
# night_dragon_on <https://kr.cm/f/p/14897/>

import xvm_main.python.utils as utils
from xfw import registerEvent
from xfw_actionscript.python import as_event, as_callback

buttonsState = None
url = 'https://stats.modxvm.com/ru/stat/players/'

def _stats_down(data):
    if data['buttonIdx'] == 0:
        accountDBID = utils.getAccountDBID()
        BigWorld.wg_openWebBrowser(url + str(accountDBID))
        return

as_callback('stats_mouseDown', _stats_down)

@registerEvent(VehicleCompareCartButton, '_populate')
def _populate(self):
    global buttonsState
    count = self.comparisonBasket.getVehiclesCount()
    if count > 0:
        buttonsState = True
        as_event('ON_VC_BST')

@registerEvent(VehicleCompareCartButton, '_VehicleCompareCartButton__onCountChanged')
def onCountChanged(self, _):
    global buttonsState
    count = self.comparisonBasket.getVehiclesCount()
    if count > 0:
        buttonsState = True
    else:
        buttonsState = None
    as_event('ON_VC_BST')

@xvm.export('vc.buttonsState', deterministic=False)
def vc_buttonsState():
    return buttonsState