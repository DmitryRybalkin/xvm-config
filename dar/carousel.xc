﻿/**
 * Параметры карусели танков
 * Конфиг: "dar"
 */
{
  "def": {
    // Тень текстовых полей.
    "textFieldShadow": { "enabled": true, "color": "0x000000", "alpha": 80, "blur": 2, "strength": 2, "distance": 0, "angle": 0 }
  },
  "carousel": {
    "backgroundAlpha": 100,
    "edgeFadeAlpha": 45,
    "showTotalSlots": true,
    "enableLockBackground": true,
    "nations_order": [],
    "types_order":   ["heavyTank", "mediumTank", "lightTank", "AT-SPG", "SPG"],
    "sorting_criteria": ["-level", "type", "nation"],
    "rows": 3,
    "filtersPadding": {
      "horizontal": 11,   // по горизонтали
      "vertical": 13      // по вертикали
    },
    "normal": {
      "fields": {
      // Флаг нации.
      "flag": { "enabled": false },
      // Иконка танка.
      "tankIcon": { "enabled": false, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Иконка типа техники.
      "tankType": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Уровень техники
      "level":    { "enabled": false, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Иконка не сбитого опыта за первую победу в день.
      "xp":       { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Название танка.
      "tankName": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1, "textFormat": {}, "shadow": {} },
      // Инфо текст аренды танка.
      "rentInfo": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1, "textFormat": {}, "shadow": {} },
      // Инфо текст (Неполный экипаж, Требуется ремонт).
      "info":     { "enabled": true, "dx": 0, "dy": 2, "alpha": 100, "scale": 1, "textFormat": { "color": "0xFFE0B6" }, "shadow": { "color": "0x650E12" } },
      // Инфо иконка
      "infoImg":  { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1 },
      // Инфо текст для слотов "Купить машину" и "Купить слот".
      "infoBuy":  { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "scale": 1, "textFormat": {}, "shadow": {} },
      // Таймер блокировки танка
      "clanLock": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100 },
      // Цена
      "price":    { "enabled": true, "dx": 0, "dy": 0, "alpha": 100 },
      // Акционная цена
      "actionPrice": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100 },
      // Маркер основной техники
      "favorite": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100 },
      // Поле статистики, отображаемое при наведении мыши
      "stats": { "enabled": true, "dx": 0, "dy": 0, "alpha": 100, "textFormat": {}, "shadow": {} }
      },

      "extraFields": [

        /* {
        "hotKeyCode": 56, "onHold": true, "visibleOnHotKey": true, "x": 0, "y": 0, "width": 160, "height": 100, "alpha": 100, "layer": "bottom",
        "format": "<img src='cfg://dar/img/lobby/carousel/flag_160x100/{{v.nation}}.png' width='160' height='100'>"
        }, */

        // Флажок
        {
          "hotKeyCode": 56, "onHold": true, "visibleOnHotKey": true, "x": 3, "y": 83, "layer": "bottom", "width": 26, "height": 16,
        "src": "cfg://dar/img/lobby/carousel/flag_26x16/{{v.nation}}.png"
	      },

        // Иконки техники
        {"enabled": true,
        "x": 1, "y": 1, "layer": "bottom", "width": 160, "height": 100,
        "src": "cfg://dar/img/vehicle/{{v.sysname}}.png"
	      },

        // Подложка слота
        { "x": 1, "y": 1, "layer": "substrate", "width": 160, "height": 100, "bgColor": "0x0A0A0A" },
        { "x": 1, "y": 1, "layer": "top", "width": 160, "height": 100, "borderColor": "{{v.selected?#60FF00|{{v.premium?#FF5500|#FFDD99}}}}", "alpha": "{{v.selected?0|{{v.premium?20|0}}}}" },

        // Тип танка фон
        {"x": 1, "y": 1, "layer": "substrate", "width": 160, "height": 100,
        "src": "cfg://dar/img/{{v.type_l}}.png"
        },

        // Уровень танка
        {
        "x": 21, "y": -1,
        "format": "<b><font face='$FieldFont' size='12' color='{{v.level=10?#FF6AFF|{{v.level=9?#4A91FF|{{v.level=8?#34FFDC|{{v.level=7?#77FF2C|{{v.level=6?#FFFF1B|#aaaaaa}}}}}}}}}}'>{{v.level}}</font></b>",
        "shadow": { "$ref": { "path": "def.textFieldShadow" }, "color": "0x000000", "blur": "3", "strength": "4" }
        },

        // Средний урон на танке
        {
        "hotKeyCode": 56, "onHold": true, "visibleOnHotKey": true,
        "x": 1, "y": 15, "align": "left",
        "format": "<b><font face='$FieldFont' size='12' color='{{v.c_tdb}}'>{{v.tdb%0.0f~}}</font></b>",
        "shadow": ${ "def.textFieldShadow" }
        },

        // Процент побед на танке
        {
        "hotKeyCode": 56, "onHold": true, "visibleOnHotKey": true,
        "x": 1, "y": 30, "align": "left",
        "format": "<b><font face='$FieldFont' size='12' color='{{v.c_winrate}}'>{{v.winrate%0.2f~%}}</font></b>",
        "shadow": ${ "def.textFieldShadow" }
        },

        // Рейтинг на танке
        {
        "hotKeyCode": 56, "onHold": true, "visibleOnHotKey": true,
        "x": 1, "y": 45, "align": "left",
        "format": "<b><font face='$FieldFont' size='12' color='{{v.c_xte}}'>{{v.xte}} xte</font></b>",
        "shadow": ${ "def.textFieldShadow" }
        },

        // Число боев
        {
        "hotKeyCode": 56, "onHold": true, "visibleOnHotKey": true,
        "x": 1, "y": 60, "align": "left", "alpha": "60",
        "format": "<b><font face='$FieldFont' size='12'>{{v.battles}} боёв</font></b>",
        "shadow": ${ "def.textFieldShadow" }
        },

        // Отметки на стволе
        {
        "hotKeyCode": 56, "onHold": true, "visibleOnHotKey": false,
        "x": 133, 
        "y": "{{v.xpToEliteLeft?47|62}}",
        "layer": "substrate", 
        "width": 29, "height": 16,
        "src": "cfg://dar/img/playersPanel/marks/mark_{{v.marksOnGun}}.png"
        },

        // Рейтинг отметки на стволе
        {
          "hotKeyCode": 56, "onHold": true, "visibleOnHotKey": true,
          "x": 158,
          /* "y": 45,  */
          "y": "{{v.xpToEliteLeft?45|60}}",
          "align": "right",
          "alpha": "50",
          "format": "<b><font face='$FieldFont'  size='12' color='{{v.c_damageRating}}'>{{v.damageRating%2.02f~%}}</font></b>",
          "shadow": ${ "def.textFieldShadow" }
        },

        // Опыт цветной
        {
          "x": 158,
          "y": 60,
          "align": "right",
          "format": "<b><font face='$FieldFont' size='12' color='{{v.xpToEliteLeft<25000?#DC72F6|{{v.xpToEliteLeft<50000?#42D7C6|{{v.xpToEliteLeft<75000?#88FF40|{{v.xpToEliteLeft<100000?#FAF740|{{v.xpToEliteLeft<125000?#FF9B42|{{v.xpToEliteLeft<150000?#FF4B40|#FF4B40}}}}}}}}}}}}' alpha='{{v.premium?#00|#ff}}'>{{v.xpToEliteLeft}}</font></b>",
          "shadow": {
            "$ref": { "path": "def.textFieldShadow" },
            "alpha": "{{v.premium?#00|#ff}}",
            "color": "0x000000", "blur":
            "{{v.premium?12|12}}",
            "strength": "{{v.premium?2.5|9}}"
          }
        },

        // Рамка выбранного слота
        { "layer": "top", "x": 1, "y": 1, "width": 160, "height": 1, "bgColor": "{{v.selected?#FFCE5D}}", "alpha": 100 },
        { "layer": "top", "x": 1, "y": 100, "width": 160, "height": 1, "bgColor": "{{v.selected?#FFCE5D}}", "alpha": 100 },
        { "layer": "top", "x": 1, "y": 1, "width": 1, "height": 100, "bgColor": "{{v.selected?#FFCE5D}}", "alpha": 100 },
        { "layer": "top", "x": 160, "y": 1, "width": 1, "height": 100, "bgColor": "{{v.selected?#FFCE5D}}", "alpha": 100 }
      ]
    },
    "small": {
        "fields": {
        "tankType": { "enabled": false },
        "level":    { "enabled": false },
        "tankName": { "enabled": false },
        "infoImg":  { "enabled": false },
        "favorite": { "enabled": false },
        "stats":    { "enabled": false }
      },
      "extraFields": [
        /* {
        "x": -2, "y": -2, "width": 166, "height": 40,
        "shadow": { "alpha": 90, "blur": 3, "color": "0x000000", "strength": 1 },
        "format": "<textformat leading='-7'><img src='cfg://dar/img/lobby/carousel/vtype/{{v.xpToEliteLeft?normal|{{v.elite|normal}}}}/small/{{.texts.event.{{v.sysname}}=event?empty|{{v.c_type}}}}.png' width='23' height='23' vspace='-8'><font color='#FFFFFF' face='$TextFont' size='10'>{{.texts.event.{{v.sysname}}=event?&nbsp;|{{v.selected?{{v.battletiermin}}-{{v.battletiermax}}|{{v.rlevel}}}}}}</font>\n<img src='cfg://dar/img/lobby/carousel/mastery/small/{{.texts.event.{{v.sysname}}|{{v.mastery|0}}}}.png' width='23' height='20' vspace='-6'><font color='{{v.c_winrate|#C8C8B5}}' face='$FieldFont' size='12'><b>{{v.winrate%2d%%}}</b></font></textformat>"
        }, */
        /* {
        "x": -2, "y": -2, "width": 166, "height": 40, "layer": "top",
        "shadow": { "alpha": 60, "blur": 6, "color": "#{{v.premium?FF0000|78776F}}", "strength": 3 },
        "format": "<textformat leading='-22' rightMargin='9'><img src='cfg://dar/img/lobby/carousel/cover/small/{{v.selected?{{.texts.xp.{{v.sysname}}|{{.texts.event.{{v.sysname}}|{{v.premium|basic}}}}}}|empty}}.png' width='162' height='37'>\n<p align='right'><font color='#{{v.premium?FFC364|D6D6D6}}' face='$FieldFont' size='15'>{{.texts.event.{{v.sysname}}=event?{{v.fullname}}|{{v.name}}}}</font></p></textformat>"
        } */
      ]
    }
  }
}