import Keys
import BigWorld
from Vehicle import Vehicle
from ResMgr import openSection
from Avatar import PlayerAvatar
# from gui.app_loader import g_appLoader
from helpers import dependency
from skeletons.gui.app_loader import IAppLoader
from gui.battle_control.controllers.feedback_adaptor import BattleFeedbackAdaptor
from gui.battle_control.battle_constants import FEEDBACK_EVENT_ID as _FET

from xfw import *
import xvm_main.python.config as config
from xvm_main.python.logger import *

settingsPM = {'enable': True, 'key': Keys.KEY_N, 'marker': False, 'proxy': None, 'arenaDP': None}


def initPM():
    global settingsPM
    settingsPM = {'enable': False, 'key': Keys.KEY_N, 'marker': False, 'proxy': None, 'arenaDP': None}
    # cfg = openSection('scripts/client/mods/PlayerMarker.xml')
    # if cfg:
    #     if cfg.has_key('EnableMarker'):
    #         settingsPM['enable'] = cfg.readInt('EnableMarker', 0) != 0
    #     if cfg.has_key('HotKey'):
    #         settingsPM['key'] = getattr(Keys, cfg.readString('HotKey'))
    settingsPM['enable'] = config.get('markers/playerMarkers/onStart', False)
    settingsPM['key'] = config.get('markers/playerMarkers/keyCode', Keys.KEY_N)


@registerEvent(BattleFeedbackAdaptor, 'startVehicleVisual')
def BattleFeedbackAdaptor_startVehicleVisual(self, vProxy, isImmediate=False):
    if vProxy.isPlayerVehicle:
        initPM()
        settingsPM['proxy'] = vProxy
        settingsPM['arenaDP'] = self._BattleFeedbackAdaptor__arenaDP
        vInfo = settingsPM['arenaDP'].getVehicleInfo(vProxy.id)
        if settingsPM['enable']:
            self.onVehicleMarkerAdded(vProxy, vInfo, settingsPM['arenaDP'].getPlayerGuiProps(vProxy.id, vInfo.team))
            settingsPM['marker'] = True



@registerEvent(BattleFeedbackAdaptor, 'stopVehicleVisual')
def BattleFeedbackAdaptor_stopVehicleVisual(self, vehicleID, isPlayerVehicle):
    if isPlayerVehicle and settingsPM['marker']:
        self.onVehicleMarkerRemoved(vehicleID)
        settingsPM['marker'] = False
    return



@registerEvent(Vehicle, '_Vehicle__onVehicleDeath')
def Vehicle__onVehicleDeath(self, isDeadStarted=False):
    if self.isPlayerVehicle and settingsPM['marker']:
        self.guiSessionProvider.shared.feedback.onVehicleFeedbackReceived(12, self.id, isDeadStarted) # 12 = _GUI_EVENT_ID.VEHICLE_DEAD



@registerEvent(Vehicle, 'onHealthChanged')
def Vehicle_onHealthChanged(self, newHealth, attackerID, attackReasonID):
    if self.isPlayerVehicle and settingsPM['marker']:
        self.guiSessionProvider.shared.feedback.setVehicleNewHealth(self.id, newHealth, attackerID, attackReasonID)


@registerEvent(PlayerAvatar, 'handleKey')
def PlayerAvatar_handleKey(self, isDown, key, mods):
    if mods == 0 and isDown and settingsPM['proxy'] is not None:
        app = dependency.instance(IAppLoader)
        if key == settingsPM['key'] and app.getDefBattleApp() and self.inputHandler.isGuiVisible:
            if settingsPM['marker']:
                self.guiSessionProvider.shared.feedback.onVehicleMarkerRemoved(settingsPM['proxy'].id)
                settingsPM['marker'] = False
            else:
                vInfo = settingsPM['arenaDP'].getVehicleInfo(settingsPM['proxy'].id)
                self.guiSessionProvider.shared.feedback.onVehicleMarkerAdded(settingsPM['proxy'], vInfo, settingsPM['arenaDP'].getPlayerGuiProps(settingsPM['proxy'].id, vInfo.team))
                settingsPM['marker'] = True
            #return True


