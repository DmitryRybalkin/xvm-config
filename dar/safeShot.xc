﻿/**
 * Блокировка выстрела по союзнику или уничтоженной технике
 * Конфиг: "dar"
 */

{
  "safeShot": {
    "enabled": true,

    // блокировка выстрелов по уничтоженным.
    "deadShotBlock": true,

    // блокировка по истечению заданного времени (в секундах) после уничтожения цели.
    "deadShotBlockTimeOut": 2000,

    // "teamKillerShotUnblock" - разблокировка выстрелов по союзникам-тимкиллерам.
    "teamKillerShotUnblock": true,

    // "teamShotBlock" - блокировка выстрелов по союзникам.
    "teamShotBlock": true,

    // блокировка выстрелов в никуда.
    "wasteShotBlock": false,
    "clientMessages": {

      // "wasteShotBlockedMessage" - сообщение, выводимое при блокировке выстрела в никуда (видит только игрок). Пустое значение - сообщение выводиться не будет.
      "wasteShotBlockedMessage": "",

      // "teamShotBlockedMessage" - сообщение, выводимое при блокировке выстрела по союзникам (видит только игрок). Пустое значение - сообщение выводиться не будет.
      "teamShotBlockedMessage": "",

      // "deadShotBlockedMessage" - сообщение, выводимое при блокировке выстрела по уничтоженным (видит только игрок). Пустое значение - сообщение выводиться не будет.
      "deadShotBlockedMessage": ""
    },

    "chatMessages": {
      // "teamShotBlockedMessage" - сообщение, выводимое при блокировке выстрела по союзникам (отправляется в чат). Пустое значение - сообщение выводиться не будет.
      "teamShotBlockedMessage": "({{target-vehicle}}), {{l10n:you're blocking my line of fire}}!"
    },

    // "disableKey" - клавиша отключения аддона (список клавиш можно посмотреть в hotkeys.xc). 0 - отключает возможность переключения.
    "disableKey": 56,

    // "onHold" - отключение блокировок по зажатию клавиши.
    "onHold": true,

    // "triggerMessage" - отображение сообщения при включении\отключении действия аддона.
    "triggerMessage": true,

    "triggerText": {
      // "enabled" - текст сообщения при включении действия аддона.
      "enabled": "",
      // "disabled" - текст сообщения при отключении действия аддона.
      "disabled": ""
    }
    /* "triggerText": {
      "enabled": "{{l10n:SafeShot enabled}}",
      "disabled": "{{l10n:SafeShot disabled}}"
    } */
  }
}