﻿/**
 * Настройки дополнительных звуков
 * Конфиг: "dar"
 */
{
  "sounds": {
    "enabled": true,
    "logSoundEvents": false,
    "remote_communication": false,
    "soundBanks": {
      "battle": [ "xvm://audioww/xvm.bnk",
                  "cfg://dar/audioww/SM_battleEnd.bnk", 
                  "cfg://dar/audioww/SM_enemy_killed.bnk",
                  "cfg://dar/audioww/SM_gun_reloaded.bnk",
                  "cfg://dar/audioww/SM_crit_damaged.bnk",
                  /* "cfg://dar/audioww/SM_sixthSense.bnk", */
                  "cfg://dar/audioww/SM_enemySighted.bnk" 
      ],
      "hangar": [ "xvm://audioww/xvm.bnk" ]
    },
    "soundMapping": {
      // Отключение таймера перед боем
      "timer": "emptyEvent",
      // Отключение Уничтожение союзников (шипение)
      "expl_ally_NPC": "emptyEvent",
      // Отключение Звук клика по миникарте
      "minimap_attention": "emptyEvent",
      // Отключение Артобстрел - карта "Заполярье"
      "amb_map_38_mannerheim_line_flight_missiles": "emptyEvent",
      "amb_map_38_mannerheim_line_rocket_expl": "emptyEvent",
      // Отключение Звук пролетающих вертолетов/истребителей - карта "Минск"
      "amb_map_90_minsk_helicopter": "emptyEvent",
      "amb_map_90_minsk_aircraft_fast": "emptyEvent",
      "amb_map_90_minsk_aircraft_med": "emptyEvent",
      "amb_map_90_minsk_aircraft_low": "emptyEvent",

      "amb_map_13_erlenberg_aircraft": "emptyEvent",
      "amb_map_13_erlenberg_anti_aircraft": "emptyEvent",
      "amb_map_38_mannerheim_line_rocket_expl": "emptyEvent",
      "amb_map_38_mannerheim_line_flight_missiles": "emptyEvent",
      "amb_map_90_minsk_helicopter": "emptyEvent",
      "amb_map_90_minsk_aircraft_med": "emptyEvent",
      "amb_map_90_minsk_aircraft_low": "emptyEvent",
      "amb_map_90_minsk_aircraft_fast": "emptyEvent",
      "eb_global_message": "emptyEvent",
      "lightbulb": "lightbulb",
      "lightbulb_02": "lightbulb_02",
      "pm_flag_appearance": "emptyEvent",
      "pm_flag_disappearance": "emptyEvent",


      "sixthSense": "sixthSense",
      "xvm_sixthSense": "",
      /* "xvm_sixthSense": "SM_sixthSense_timer", */
      "xvm_sixthSenseRudy": "",
      "xvm_enemySighted": "SM_enemySighted",
      "xvm_fireAlert": "",
      "xvm_ammoBay": "",
      "xvm_gunReloaded": "SM_gun_reloaded",

      // Отключение стандартных критов
      // Somebody else's vehicle critical damages bells
      // Звонки критических повреждений чужого танка
      // AP / ББ
      "imp_auto_critical_AP_PC_NPC": "imp_auto_pierce_AP_PC_NPC",
      "imp_main_critical_AP_PC_NPC": "imp_main_pierce_AP_PC_NPC",
      "imp_small_critical_AP_PC_NPC": "imp_small_pierce_AP_PC_NPC",
      "imp_medium_critical_AP_PC_NPC": "imp_medium_pierce_AP_PC_NPC",
      "imp_large_critical_AP_PC_NPC": "imp_large_pierce_AP_PC_NPC",
      "imp_huge_critical_AP_PC_NPC": "imp_huge_pierce_AP_PC_NPC",
      // APCR / ПК
      "imp_auto_critical_APCR_PC_NPC": "imp_auto_pierce_APCR_PC_NPC",
      "imp_main_critical_APCR_PC_NPC": "imp_main_pierce_APCR_PC_NPC",
      "imp_small_critical_APCR_PC_NPC": "imp_small_pierce_APCR_PC_NPC",
      "imp_medium_critical_APCR_PC_NPC": "imp_medium_pierce_APCR_PC_NPC",
      "imp_large_critical_APCR_PC_NPC": "imp_large_pierce_APCR_PC_NPC",
      "imp_huge_critical_APCR_PC_NPC": "imp_huge_pierce_APCR_PC_NPC",
      // HC / КС
      "imp_auto_critical_HC_PC_NPC": "imp_auto_pierce_HC_PC_NPC",
      "imp_main_critical_HC_PC_NPC": "imp_main_pierce_HC_PC_NPC",
      "imp_small_critical_HC_PC_NPC": "imp_small_pierce_HC_PC_NPC",
      "imp_medium_critical_HC_PC_NPC": "imp_medium_pierce_HC_PC_NPC",
      "imp_large_critical_HC_PC_NPC": "imp_large_pierce_HC_PC_NPC",
      "imp_huge_critical_HC_PC_NPC": "imp_huge_pierce_HC_PC_NPC",
      // HE / ОФ
      "imp_auto_critical_HE_PC_NPC": "imp_auto_pierce_HE_PC_NPC",
      "imp_main_critical_HE_PC_NPC": "imp_main_pierce_HE_PC_NPC",
      "imp_small_critical_HE_PC_NPC": "imp_small_pierce_HE_PC_NPC",
      "imp_medium_critical_HE_PC_NPC": "imp_medium_pierce_HE_PC_NPC",
      "imp_large_critical_HE_PC_NPC": "imp_large_pierce_HE_PC_NPC",
      "imp_huge_critical_HE_PC_NPC": "imp_huge_pierce_HE_PC_NPC",
      // Own vehicle critical damages bells
      // Звонки критических повреждений своего танка
      // AP / ББ
      "imp_auto_critical_AP_NPC_PC": "imp_auto_pierce_AP_NPC_PC",
      "imp_main_critical_AP_NPC_PC": "imp_main_pierce_AP_NPC_PC",
      "imp_small_critical_AP_NPC_PC": "imp_small_pierce_AP_NPC_PC",
      "imp_medium_critical_AP_NPC_PC": "imp_medium_pierce_AP_NPC_PC",
      "imp_large_critical_AP_NPC_PC": "imp_large_pierce_AP_NPC_PC",
      "imp_huge_critical_AP_NPC_PC": "imp_huge_pierce_AP_NPC_PC",
      // APCR / ПК
      "imp_auto_critical_APCR_NPC_PC": "imp_auto_pierce_APCR_NPC_PC",
      "imp_main_critical_APCR_NPC_PC": "imp_main_pierce_APCR_NPC_PC",
      "imp_small_critical_APCR_NPC_PC": "imp_small_pierce_APCR_NPC_PC",
      "imp_medium_critical_APCR_NPC_PC": "imp_medium_pierce_APCR_NPC_PC",
      "imp_large_critical_APCR_NPC_PC": "imp_large_pierce_APCR_NPC_PC",
      "imp_huge_critical_APCR_NPC_PC": "imp_huge_pierce_APCR_NPC_PC",
      // HC / КС
      "imp_auto_critical_HC_NPC_PC": "imp_auto_pierce_HC_NPC_PC",
      "imp_main_critical_HC_NPC_PC": "imp_main_pierce_HC_NPC_PC",
      "imp_small_critical_HC_NPC_PC": "imp_small_pierce_HC_NPC_PC",
      "imp_medium_critical_HC_NPC_PC": "imp_medium_pierce_HC_NPC_PC",
      "imp_large_critical_HC_NPC_PC": "imp_large_pierce_HC_NPC_PC",
      "imp_huge_critical_HC_NPC_PC": "imp_huge_pierce_HC_NPC_PC",
      // HE / ОФ
      "imp_auto_critical_HE_NPC_PC": "imp_auto_pierce_HE_NPC_PC",
      "imp_main_critical_HE_NPC_PC": "imp_main_pierce_HE_NPC_PC",
      "imp_small_critical_HE_NPC_PC": "imp_small_pierce_HE_NPC_PC",
      "imp_medium_critical_HE_NPC_PC": "imp_medium_pierce_HE_NPC_PC",
      "imp_large_critical_HE_NPC_PC": "imp_large_pierce_HE_NPC_PC",
      "imp_huge_critical_HE_NPC_PC": "imp_huge_pierce_HE_NPC_PC",
      // Somebody else's vehicle critical damages bells (the observer mode)
      // Звонки критических повреждений чужого танка (в режиме наблюдателя)
      // AP / ББ
      "imp_auto_critical_AP_NPC_NPC": "imp_auto_pierce_AP_NPC_NPC",
      "imp_main_critical_AP_NPC_NPC": "imp_main_pierce_AP_NPC_NPC",
      "imp_small_critical_AP_NPC_NPC": "imp_small_pierce_AP_NPC_NPC",
      "imp_medium_critical_AP_NPC_NPC": "imp_medium_pierce_AP_NPC_NPC",
      "imp_large_critical_AP_NPC_NPC": "imp_large_pierce_AP_NPC_NPC",
      "imp_huge_critical_AP_NPC_NPC": "imp_huge_pierce_AP_NPC_NPC",
      // APCR / ПК 
      "imp_auto_critical_APCR_NPC_NPC": "imp_auto_pierce_APCR_NPC_NPC",
      "imp_main_critical_APCR_NPC_NPC": "imp_main_pierce_APCR_NPC_NPC",
      "imp_small_critical_APCR_NPC_NPC": "imp_small_pierce_APCR_NPC_NPC",
      "imp_medium_critical_APCR_NPC_NPC": "imp_medium_pierce_APCR_NPC_NPC",
      "imp_large_critical_APCR_NPC_NPC": "imp_large_pierce_APCR_NPC_NPC",
      "imp_huge_critical_APCR_NPC_NPC": "imp_huge_pierce_APCR_NPC_NPC",
      // HC / КС
      "imp_auto_critical_HC_NPC_NPC": "imp_auto_pierce_HC_NPC_NPC",
      "imp_main_critical_HC_NPC_NPC": "imp_main_pierce_HC_NPC_NPC",
      "imp_small_critical_HC_NPC_NPC": "imp_small_pierce_HC_NPC_NPC",
      "imp_medium_critical_HC_NPC_NPC": "imp_medium_pierce_HC_NPC_NPC",
      "imp_large_critical_HC_NPC_NPC": "imp_large_pierce_HC_NPC_NPC",
      "imp_huge_critical_HC_NPC_NPC": "imp_huge_pierce_HC_NPC_NPC",
      // HE / ОФ
      "imp_auto_critical_HE_NPC_NPC": "imp_auto_pierce_HE_NPC_NPC",
      "imp_main_critical_HE_NPC_NPC": "imp_main_pierce_HE_NPC_NPC",
      "imp_small_critical_HE_NPC_NPC": "imp_small_pierce_HE_NPC_NPC",
      "imp_medium_critical_HE_NPC_NPC": "imp_medium_pierce_HE_NPC_NPC",
      "imp_large_critical_HE_NPC_NPC": "imp_large_pierce_HE_NPC_NPC",
      "imp_huge_critical_HE_NPC_NPC": "imp_huge_pierce_HE_NPC_NPC",

      "vo_enemy_hp_damaged_by_projectile_and_chassis_damaged_by_player": "SM_crit_damaged",
      "vo_enemy_hp_damaged_by_projectile_and_gun_damaged_by_player": "SM_crit_damaged",
      "vo_enemy_no_hp_damage_at_attempt_and_chassis_damaged_by_player": "SM_crit_damaged",
      "vo_enemy_no_hp_damage_at_attempt_and_gun_damaged_by_player": "SM_crit_damaged",
      "vo_enemy_no_hp_damage_at_no_attempt_and_chassis_damaged_by_player": "SM_crit_damaged",
      "vo_enemy_no_hp_damage_at_no_attempt_and_gun_damaged_by_player": "SM_crit_damaged",



      "xvm_battleEnd_5_min": "SM_battleEnd_5_min",
      "xvm_battleEnd_3_min": "SM_battleEnd_3_min",
      "xvm_battleEnd_2_min": "SM_battleEnd_2_min",
      "xvm_battleEnd_1_min": "SM_battleEnd_1_min",
      "xvm_battleEnd_30_sec": "SM_battleEnd_30_sec",
      "xvm_battleEnd_5_sec": "SM_battleEnd_5_sec"
    }
  }
}