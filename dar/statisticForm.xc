﻿/**
 * Параметры окна статистики по клавише Tab
 * Конфиг: "dar"
 */
{
  "statisticForm": {
    "fragsFieldShowBorder": false,
    "fragsFieldOffsetXLeft": 37,
    "fragsFieldOffsetXRight": 37,
    "fragsFieldWidthLeft": 40,
    "fragsFieldWidthRight": 40,
    "formatLeftFrags": "{{frags}}",
    "formatRightFrags": "{{frags}}",

    "vehicleIconOffsetXLeft": "{{xvm-stat?40|3}}",
    "vehicleIconOffsetXRight": "{{xvm-stat?40|0}}",

    "removeVehicleLevel": true,
    "removeVehicleTypeIcon": true,
    "removeSquadIcon": false,
    "removeRankBadgeIcon": true,
    "removePlayerStatusIcon": "{{xvm-stat?true}}",

    "squadIconOffsetXLeft": -1,
    "squadIconOffsetXRight": -1,
    
    "formatLeftVehicle": "",
    "formatRightVehicle": "",
    "formatLeftNick": "",
    "formatRightNick": "",

    "extraFieldsLeft": [
      ${ "def.xvmUserIcon" },
      ${ "def.nickNameClanLeft" },
      ${ "def.statisticsLeft" },
      ${ "def.statisticsKeyLeft" },
      ${ "def.clanIcon" },
      ${ "def.vehicleName" },
      ${ "def.level" },
      ${ "def.mark" }
    ],
    "extraFieldsRight": [
      ${ "def.xvmUserIcon" },
      ${ "def.nickNameClanRight" },
      ${ "def.statisticsRight" },
      ${ "def.statisticsKeyRight" },
      ${ "def.clanIcon" },
      ${ "def.vehicleName" },
      ${ "def.level" },
      ${ "def.mark" }
    ]
  },

  "def": {
    "defaultItem": {
      "alpha": "{{alive?100|50}}", 
      "bindToIcon": true,
      "shadow": {
        "alpha": 50, 
        "angle": 90, 
        "blur": 1, 
        "color": "0x000000", 
        "distance": 1, 
        "strength": 1.5 
      },
      "textFormat": { "color": "#{{tk?00EAFF|{{player?FFB964|{{squad?FFB964|F4EFE8}}}}}}", "font": "$FieldFont", "size": 13 }
    },

    "rankBadgeIcon": {
      "$ref": { "path": "def.defaultItem" },
      "x": "{{xvm-stat?-456|-374}}", "y": -1, "width": 30, "height": 30,
      "format": "<img src='img://gui/maps/icons/library/badges/24x24/badge_{{rankBadgeId}}.png' width='24' height='24'>"
    },

    "flagIcon": {
      "$ref": { "path": "def.defaultItem" },
      "x": -404, "y": 4, "width": 24, "height": 24,
      "format": "<img src='xvm://res/icons/flags/{{flag}}.png' width='16' height='13'>"
    },

    "xvmUserIcon": {
      //"borderColor": "0xFF00FF",
      "$ref": { "path": "def.defaultItem" },
      "x": -360, 
      "y": 3, 
      "width": 24, 
      "height": 24,
      "format": "<img src='cfg://dar/img/xvm/Tab/xvm-user-{{xvm-user}}.png'>"
    },

    "nickNameClanLeft": {
      //"borderColor": "0xFF00FF",
      "$ref": { "path": "def.defaultItem" },
      "x": "-341", 
      "y": 3, 
      "width": 160, 
      "height": 24,
      //"antiAliasType": "normal",
      "textFormat": {
        "font": "$FieldFont",
        "bold": "false",
        "size": 14,
        "color": "0xC8C8B5"
      },
      "format": "<font face='$FieldFont' alpha='{{alive?{{ready?#FF|#80}}|#80}}'><font size='14' color='{{player?#FFCC66|{{squad?#FFB964|{{tk?#00EAFF}}}}}}'>{{.texts.nicknames.{{name}}|{{name%.18s~..}}}}  </font><font size='12' color='{{topclan?#FFCC66|#E5D39A}}'>{{clannb}}</font></font>"
    },

    "nickNameClanRight": {
      "$ref": { "path": "def.nickNameClanLeft" },
      "format": "<font face='$FieldFont' color='#E5E5E5' alpha='{{alive?{{ready?#FF|#80}}|#80}}'><font size='12' color='{{topclan?#FFCC66|#E5D39A}}'>{{clannb}}</font><font size='13'>  {{.texts.nicknames.{{name}}|{{name%.18s~..}}}}</font></font>"
    },


    "statisticsLeft": {
      //"borderColor": "0xFF00FF",
      "$ref": { "path": "def.defaultItem" },
      "x": -255, 
      "y": 3, 
      "width": 250,
      "height": 24, 
      "hotKeyCode": 56, 
      "onHold": true, 
      "visibleOnHotKey": false,
      //"antiAliasType": "normal",
      "textFormat": { 
        "font": "Mono",
        "bold": true,
        "align": "right", 
        "color": "0xA6A6A6", 
        "size": "{{xvm-stat?14|0}}" 
      },
      "format": "<font color='#7B725A'>{{t-battles%5d|--}}</font> <font color='{{c:t-winrate}}'>{{xte|--}}</font> <font color='{{c:t-winrate}}'>{{t-winrate%2d|--}}%</font>   <font color='#7B725A'>{{kb>99?{{kb%3d}}|{{kb%2d|--}}k}}</font> <font color='{{c:xr}}'>{{r_size=2?{{r%s|----}}|{{r>9999?XXXX|{{r%4d|----}}}}}}</font> <font color='{{c:winrate}}'>{{winrate%2d|--}}%</font>"
    },

    "statisticsRight": {
      "$ref": { "path": "def.statisticsLeft" },
      "x": -253,
      "textFormat": { 
        "font": "Mono", 
        "align": "left", 
        "color": "0xA6A6A6", 
        "size": "{{xvm-stat?14|0}}" 
      },
      "format": "<font color='{{c:winrate}}'>{{winrate%2d|--}}%</font> <font color='{{c:winrate}}'>{{r_size=2?{{r%s|--}}|{{r>9999?XXXX|{{r%4d|----}}}}}}</font> <font color='#7B725A'>{{kb>99?{{kb%3d}}|{{kb%2d|--}}k}}</font>   <font color='{{c:t-winrate}}'>{{t-winrate%2d|--}}%</font> <font color='{{c:t-winrate}}'>{{xte|--}}</font> <font color='#7B725A'>{{t-battles|--}}</font>"
    },

    "statisticsKeyLeft": {
      "$ref": { "path": "def.statisticsLeft" },
      "visibleOnHotKey": true,
      "format": "<font color='{{c:t-winrate}}'>{{t-winrate%2d|--}}%</font>  <font color='{{c:kb}}'>{{kb>99?{{kb%3d}}|{{kb%2d|--}}k}}</font>  <font color='{{c:tdb}}'>{{tdb%4d|----}}</font>  <font color='{{c:avglvl}}'>{{avglvl%2d|--}}&nbsp;</font>"
    },

    "statisticsKeyRight": {
      "$ref": { "path": "def.statisticsKeyLeft" },
      "format": "<font color='{{c:avglvl}}'>{{avglvl%2d|--}}&nbsp;</font>  <font color='{{c:tdb}}'>{{tdb%4d|----}}</font>  <font color='{{c:kb}}'>{{kb>99?{{kb%3d}}|{{kb%2d|--}}k}}</font>  <font color='{{c:t-winrate}}'>{{t-winrate%2d|--}}%</font>"
    },










    // Название танка

    "vehicleName": {
      "bindToIcon": true,
      "x": "{{ally?4|-29}}",
      "y": 1, 
      "width": 105, 
      "height": 24,
      //"antiAliasType": "normal",
      "alpha": "{{alive?90|30}}",
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 12,
        "color": "0xFFFFFF",
        "align": "left",
        "valign": "center"
      },
      "shadow": {
        "alpha": 50, 
        "angle": 90, 
        "blur": 1, 
        "color": "0x000000", 
        "distance": 1, 
        "strength": 1.5 
      },
      "format": "{{vehicle}}"
    },

    // Уровень техники
    "level": {
      "bindToIcon": true,
      "x": "{{ally?80|80}}",
      "y": -5, 
      "width": 24, 
      "height": 24,
      "alpha": "{{alive?100|30}}",
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 11,
        "color": "{{level=10?0xFF00FE|{{level=9?0x197AFF|{{level=8?0x01FFCD|{{level=7?0x65FF00|{{level=6?0xFEFF01|0xFE0000}}}}}}}}}}",
        "align": "{{ally?left|right}}",
        "valign": "center"
      },
      "format": "{{level}}"
    },


    "vehicleType": {
      "$ref": { "path": "def.defaultItem" },
      "x": -19, "y": -1, "width": 24, "height": 24, "alpha": "{{alive?80|40}}",
      "format": "<font face='xvm' size='21'>{{vtype}}</font>"
    },

    "clanIcon": {
      "$ref": { "path": "def.defaultItem" },
      "x": -400, 
      "y": -4, 
      "width": 30, 
      "height": 30,
      "format": "{{topclan}}"
    },

        // Отметка на стволе
    "mark": {
      //"antiAliasType": "normal",
      //"borderColor": "0xFF0000",
      "bindToIcon": true,
      "alpha": "{{alive?100|30}}",
      "x": "{{ally?54|0}}",
      "y": 4,
      "width": 26,
      "height": 19, 
      "textFormat": {
        "font": "$TitleFont",
        "bold": "true",
        "size": 13,
        "color": "0xF7BD60",
        "align": "right",
        "valign": "center"
      },
      "src": "cfg://dar/img/playersPanel/marks/mark_{{marksOnGun}}.png"
    }
  }
}