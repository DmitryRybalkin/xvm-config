﻿{
  "def": {
    // Тень текстовых полей.
    "textFieldShadow": {
      "alpha": 50, 
      "angle": 90, 
      "blur": 1, 
      "color": "0x000000", 
      "distance": 1, 
      "strength": 1.5 
    },

    // Название танка
    "vehicleName": {
      "bindToIcon": true,
      "x": "{{ally?4|-4}}",
      "y": 1, 
      "width": 80, 
      "height": 24,
      //"antiAliasType": "normal",
      "alpha": "{{alive?100|30}}",
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 12,
        "color": "0x{{premium?F9D190|FFFFFF}}",
        "align": "left",
        "valign": "center"
      },
      "format": "{{vehicle}}",
      "shadow": ${ "@xvm.xc": "mainShadow" }
    },

    // Отметка на стволе
    "mark": {
      //"antiAliasType": "normal",
      //"borderColor": "0xFF0000",
      "bindToIcon": true,
      "alpha": "{{alive?100|30}}",
      "x": "{{ally?54|0}}",
      "y": 4,
      "width": 26,
      "height": 19, 
      "textFormat": {
        "font": "$TitleFont",
        "bold": "true",
        "size": 13,
        "color": "0xF7BD60",
        "align": "right",
        "valign": "center"
      },
      "src": "cfg://dar/img/playersPanel/marks/mark_{{marksOnGun=1?0|{{marksOnGun}}}}.png"
    },

    // Уровень техники
    "level": {
      "bindToIcon": true,
      "x": "{{ally?79|79}}",
      "y": -2, 
      "width": 24, 
      "height": 24,
      "alpha": "{{alive?100|30}}",
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 11,
        "color": "{{level=10?0xFF00FE|{{level=9?0x197AFF|{{level=8?0x01FFCD|{{level=7?0x65FF00|{{level=6?0xFEFF01|0xFE0000}}}}}}}}}}",
        "align": "{{ally?left|right}}",
        "valign": "center"
      },
      "shadow": { 
        "alpha": 50, 
        "angle": 90, 
        "blur": 1, 
        "color": "0x000000", 
        "distance": 1, 
        "strength": 1.5  
      },
      "format": "{{level}}"
    },

    // Топ клан
    "topClan": {
      "name": "top clan",
      "bindToIcon": true,
      "enabled": true,
      "x": "{{ally?100|100}}",
      "y": -1,
      "alpha": 100,
      "align": "center",
      "textFormat": {
        "font": "xvm",
        "size": 26,
        "color": "0xFFCC66",
        "bold": false,
        "italic": false
      },
      "format": "{{topclan}}",
      "shadow": { "$ref": { "path": "def.textFieldShadow" }, "  strength": 0.5 }
    },

    // Индикатор HP (цветная полоса)
    "hpBar": {
      "x": "{{py:add({{hp-ratio:200}}, {{ally?-513|-511}})}}",
      "y": 1,
      "bindToIcon": false,
      "width": "520",
      "height": "{{player?24|24}}",

      //"bgColor": "{{player?#624C2E|{{squad?#9a8170|{{ally?{{tk?#2f8f98|#363F2D}}|#582D2C}}}}}}",
      "alpha": "{{alive?{{ready?{{spotted!=0?100|{{player?100|0}}}}|0}}|0}}",
      "src": "cfg://dar/img/playersPanel/{{player?player|{{ally?{{tk?tk|left}}|right}}}}Bg.png"
      //"src": "cfg://dar/img/playersPanel/greenBar.png"
    },

    // Оставшиеся HP союзников
    "hp": {
      //"antiAliasType": "normal",
      "bindToIcon": true,
      "alpha": "{{alive?{{ready?100|0}}|0}}",
      "x": "{{ally?-40|-40}}",
      "y": 1,
      "width": 35,
      "height": 24, 
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 13,
        "color": "0xE5D39A",
        "align": "{{ally?right|left}}",
        "valign": "center"
      },
      "format": "{{hp}}"
    },

    // [Ник игрока] левая панель
    "nick": {
      //"borderColor": "0xFF00FF",
      "bindToIcon": false,
      "alpha": "{{alive?{{ready?{{spotted!=0?100|{{player?100|0}}}}|0}}|30}}",
      "x": "{{ally?46|46}}",
      "y": 1, 
      "width": 116, 
      "height": 24,
      //"antiAliasType": "normal",
      "shadow": { "$ref": { "path": "def.textFieldShadow" }},
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 13,
        "color": "{{player?#FFCC66|{{squad?#FFB964|{{tk?#00EAFF|{{c:xr}}}}}}}}",
        /* "color": "{{player?{{c:xr}}|{{squad?#FFB964|{{c:xr}}}}}}", */
        "align": "{{ally?|left}}",
        "valign": "center"
      },
      "format": "{{name%.13s~..}} <font size='12' alpha='#77' color='#E5D39A'>{{clannb}}</font>"
    },

    // Время перезарядки
    "vehicleReload": {
      "bindToIcon": true,
      "x": "{{ally?4|-4}}",
      "y": 1, 
      "width": 80, 
      "height": 24,
      //"antiAliasType": "normal",
      "alpha": "{{alive?90|30}}",
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 13,
        "color": "0xFFFFFF",
        "align": "left",
        "valign": "center"
      },
      "format": "{{py:reloadVehicle('{{name}}')%.1f}}",
      "shadow": { "$ref": { "path": "def.textFieldShadow" }}
    },

    // Время перезарядки
    "vehicleDamage": {
      "bindToIcon": true,
      "x": "{{ally?-6|6}}",
      "y": 1, 
      "width": 80,
      "height": 24,
      //"antiAliasType": "normal",
      "alpha": "{{alive?90|30}}",
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 13,
        "color": "0xFFFFFF",
        "align": "right",
        "valign": "center"
      },
      "format": "{{py:shellDamage('{{name}}')}}",
      "shadow": { "$ref": { "path": "def.textFieldShadow" }}
    },

    "frags": {
      //"borderColor": "0xFF00FF",
      "bindToIcon": false,
      "x": "{{ally?26|26}}",      
      "y": 1, 
      "width": 20, 
      "height": 24,
      //"antiAliasType": "normal",
      "shadow": { "$ref": { "path": "def.textFieldShadow" }},
      "textFormat": {
        "font": "$FieldFont",
        "bold": "true",
        "size": 13,
        "color": "0xE5D39A",
        "align": "center",
        "valign": "center"
      },
      "format": "{{frags?{{frags}}}}", 
      "alpha": "{{alive?100|50}}", 
      "shadow": { "$ref": { "path": "def.textFieldShadow" }, "  strength": 0.5 } 
    },

    "winrate": {
      "enabled": "{{xvm-stat?true|false}}",
      "bindToIcon": false,
      //"borderColor": "0xFFFF00",
      //"antiAliasType": "normal",
      "alpha": "{{alive?{{ready?{{spotted!=0?{{r?100|50}}|{{player?100|0}}}}|0}}|30}}",
      "y": 1, 
      "x": "{{ally?46|46}}", 
      "width": 116, 
      "height": 24,
      "textFormat": {
        "font": "mono",
        "bold": "true",
        "size": 13,
        "align": "{{ally?left|left}}",
        "valign": "center"
      },
      "shadow": { "$ref": { "path": "def.textFieldShadow" }},

      "format": "<font color='{{c:xr}}'>{{r>=10000?9999|{{r%4d|----}}}}</font> <font color='{{c:xte}}'>({{xte|--}})</font> <font color='{{c:winrate}}'>{{winrate%2d~%|--%}}</font> <font color='#7B725A'>{{kb%2d~k|--k}}</font>" 
    }
  }
}
