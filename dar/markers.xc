﻿/**
 * Маркеры над танками.
 * Конфиг: "dar"
 */
{
  "markers": {
    "enabled": true,
    "damageMarkers": {
      "ally_alive":       "<img src='cfg://dar/img/markers/damage/{{c:dmg-kind}}.png' width='32' height='32' vspace='-10'>",
      "teamKiller_alive": "<img src='cfg://dar/img/markers/damage/{{c:dmg-kind}}.png' width='32' height='32' vspace='-10'>",
      "enemy_alive":      "<img src='cfg://dar/img/markers/damage/{{c:dmg-kind}}.png' width='32' height='32' vspace='-10'>"
    },
    "turretMarkers": {
      /** Сток башня и топ орудие не может быть установлено. */
      "highVulnerability": "<img src='cfg://dar/img/markers/turret/{{c:system}}-high.png' width='28' height='24' vspace='-3'>",
      /** Сток башня и топ орудие может быть установлено. */
      "lowVulnerability": "<img src='cfg://dar/img/markers/turret/{{c:system}}-low.png' width='28' height='24' vspace='-3'>"
    },

    "playerMarkers": {
      //true - включен при старте
      "onStart": true,
      //клавиша включения/выключения маркера
      "keyCode": 73
    },

    // союзники
    "ally": {
      "alive": {
        "normal": {
          // Контурная иконка танка.
          "contourIcon":         { "enabled": false },
          // Уровень танка.
          "levelIcon":           { "enabled": false },
          // Индикатор здоровья.
          "healthBar":          ${ "def.healthBarAlly" },
          // Всплывающий урон для союзника, игрока, взводного.
          "damageText":         ${ "def.damageTextAliveAlly" },
          "damageTextPlayer":   ${ "def.damageTextPlayerAlive" },
          "damageTextSquadman": ${ "def.damageTextAliveAlly" },
          // Иконка типа танка (ТТ/СТ/ЛТ/ПТ/Арта).
          "vehicleIcon":        ${ "def.vehicleIcon" },
          // Маркеры "Нужна помощь" и "Атакую".
          "actionMarker":       ${ "def.actionMarker" },
          "stunMarker":         ${ "def.stunMarker" },
          // Индикатор урона (рикошет, критический урон, ...).
          "damageIndicator":     ${ "def.damageIndicator" },
          // Маркер оглушения и маркер боевого снаряжения в режиме "Линия фронта" (дымовая завеса, воодушевление, инженерный отряд).
          "vehicleStatusMarker": ${ "def.vehicleStatusMarker" },
          "textFields": [
            ${ "def.tankName" },
            ${ "def.lowHP" },
            ${ "def.hp" },
            ${ "def.squad" },
            ${ "def.xmqpEvent" },
            //${ "def.mark" },
            ${ "def.healthBorder" }
          ]
        },
        "extended": {
          "contourIcon":         { "enabled": false },
          "levelIcon":           { "enabled": false },
          "healthBar":          ${ "def.healthBarAlly" },
          "damageText":         ${ "def.damageText" },
          "damageTextPlayer":   ${ "def.damageText" },
          "damageTextSquadman": ${ "def.damageText" },
          "actionMarker":       ${ "def.actionMarker" },
          "stunMarker":         ${ "def.stunMarker" },
          "textFields": [
            //${ "def.tankName" },
            ${ "def.playerName" },
            //${ "def.hp" },
            ${ "def.winRate" },
            ${ "def.xte" },
            ${ "def.lowHP" },
            ${ "def.rating" },
            ${ "def.squad" },
            ${ "def.damagePenetrationVehicle" },
            ${ "def.reloadVehicleAlly" }
          ]
        }
      },
      "dead": {
        "normal": {
          "levelIcon":           ${ "def.isDisabled" },
          "healthBar":           ${ "def.isDisabled" },
          "contourIcon":         ${ "def.isDisabled" },
          "actionMarker":        ${ "def.isDisabled" },
          "damageIndicator":     ${ "def.isDisabled" },
          "vehicleStatusMarker": ${ "def.isDisabled" },
          "damageText":          ${ "def.damageTextDeadAlly" },
          "damageTextPlayer":    ${ "def.damageTextPlayerDead" },
          "damageTextSquadman":  ${ "def.damageTextDeadAlly" },
          "vehicleIcon":         ${ "def.isDisabled" },
          "textFields": [
            ${ "def.deadIcon" }
          ]
        },
        "extended": {
          "levelIcon":           ${ "def.isDisabled" },
          "healthBar":           ${ "def.isDisabled" },
          "contourIcon":         ${ "def.isDisabled" },
          "actionMarker":        ${ "def.isDisabled" },
          "damageIndicator":     ${ "def.isDisabled" },
          "vehicleStatusMarker": ${ "def.isDisabled" },
          "damageText":          ${ "def.damageTextDeadAlly" },
          "damageTextPlayer":    ${ "def.damageTextPlayerDead" },
          "damageTextSquadman":  ${ "def.damageTextDeadAlly" },
          "vehicleIcon":         ${ "def.isDisabled" },
          "textFields": [
            ${ "def.deadIcon" }
          ]
        }
      }
    },

    /** Противники */
    "enemy": {
      "$ref": { "path": "markers.ally" },
      "alive": {
        "normal": {
          "contourIcon":         { "enabled": false },
          "levelIcon":           { "enabled": false },
          "healthBar":          ${ "def.healthBarEnemy" },
          "damageText":         ${ "def.damageTextAliveEnemy" },
          "damageTextPlayer":   ${ "def.damageTextPlayerAlive" },
          "damageTextSquadman": ${ "def.damageTextAliveEnemy" },
          "actionMarker":       ${ "def.actionMarker" },
          "stunMarker":         ${ "def.stunMarker" },
          "damageIndicator":     ${ "def.damageIndicator" },
          "vehicleStatusMarker": ${ "def.vehicleStatusMarker" },
          "textFields": [
            ${ "def.tankName" },
            ${ "def.damageVehicle" },
            ${ "def.lowHP" },
            ${ "def.hp" },
            ${ "def.squad" },
            ${ "def.xmqpEvent" },
            //${ "def.mark" },
            ${ "def.reloadVehicleEnemy" }
          ]
        },
        "extended": {
          "contourIcon":         { "enabled": false },
          "levelIcon":           { "enabled": false },
          "healthBar":          ${ "def.healthBarEnemy" },
          "damageText":         ${ "def.damageText" },
          "damageTextPlayer":   ${ "def.damageText" },
          "damageTextSquadman": ${ "def.damageText" },
          "actionMarker":       ${ "def.actionMarker" },
          "stunMarker":         ${ "def.stunMarker" },
          "textFields": [
            // ${ "def.tankName" },
            ${ "def.playerName" },
            ${ "def.winRate" },
            ${ "def.xte" },
            ${ "def.lowHP" },
            ${ "def.rating" },
            ${ "def.squad" },
            ${ "def.damagePenetrationVehicle" },
            ${ "def.reloadVehicleEnemy" }
          ]
        }
      },
      "dead": {
        "normal": {
          "levelIcon":           ${ "def.isDisabled" },
          "healthBar":           ${ "def.isDisabled" },
          "contourIcon":         ${ "def.isDisabled" },
          "actionMarker":        ${ "def.isDisabled" },
          "damageIndicator":     ${ "def.isDisabled" },
          "vehicleStatusMarker": ${ "def.isDisabled" },
          "damageText":          ${ "def.damageTextDeadEnemy" },
          "damageTextPlayer":    ${ "def.damageTextPlayerDead" },
          "damageTextSquadman":  ${ "def.damageTextDeadEnemy" },
          "vehicleIcon":         ${ "def.isDisabled" },
          "textFields": [
            ${ "def.deadIcon" }
          ]
        },
        "extended": {
          "levelIcon":           ${ "def.isDisabled" },
          "healthBar":           ${ "def.isDisabled" },
          "contourIcon":         ${ "def.isDisabled" },
          "actionMarker":        ${ "def.isDisabled" },
          "damageIndicator":     ${ "def.isDisabled" },
          "vehicleStatusMarker": ${ "def.isDisabled" },
          "damageText":          ${ "def.damageTextDeadEnemy" },
          "damageTextPlayer":    ${ "def.damageTextPlayerDead" },
          "damageTextSquadman":  ${ "def.damageTextDeadEnemy" },
          "vehicleIcon":         ${ "def.isDisabled" },
          "textFields": [
            ${ "def.deadIcon" }
          ]
        }
      }
    }
  },

  // Шаблоны
  "def": {
    "isDisabled": {
      "enabled": false
    },
    // Иконка типа танка (ТТ/СТ/ЛТ/ПТ/Арта).
    "vehicleIcon": {
      "x": 0,
      "y": -16,
      "alpha": 100,
      "maxScale": 100,
      "showSpeaker": false
    },

    "vehicleStatusMarker": {
      "x": "{{x-sense-on?-22|0}}", "y": "{{topclan?{{squad?-82|-68}}|{{squad?-74|-60}}}}"
    },
    "damageIndicator": {
      "enabled": true,
      "x": "{{hp-ratio<20?66|56}}", "y": -24, "alpha": "{{dmg>0?0|90}}", "showText": false
    },
    // Тень текстовых полей
    "textFieldShadow": {
      "alpha": 50,
      "angle": 90,
      "blur": 1,
      "color": "0x000000",
      "distance": 1,
      "strength": 1.5
    },
    // Имя игрока
    "playerName": {
      "name": "Player name",
      "enabled": true,
      "x": "{{topclan?{{turret?4|-10}}|{{turret?14|0}}}}",
      "y": "{{topclan?-44|{{turret?-44|-36}}}}",
      "alpha": 100,
      "align": "center",
      "textFormat": {
        "font": "$FieldFont",
        "size": 14,
        "color": "0xffffff",
        "bold": true,
        "italic": false
      },
      "shadow": ${ "def.textFieldShadow" },
      "format": "{{name}}"
    },

    // Название танка
    "tankName": {
      "name": "Tank name",
      "enabled": true,
      "x": "{{topclan?{{turret?4|-10}}|{{turret?14|0}}}}",
      "y": "{{topclan?-44|{{turret?-44|-36}}}}",
      "alpha": 100,
      "align": "center",
      "textFormat": {
        "font": "$TitleFont",
        "size": 14,
        "color": "{{c:xr}}",
        "bold": false,
        "italic": false
      },
      /* "shadow": ${ "def.textFieldShadow" }, */
      "shadow": {
        "alpha": 80,
        "angle": 90,
        "blur": 3,
        "color": "0x000000",
        "distance": 1,
        "strength": 1.5
      },
      "format": "{{topclan}}{{vehicle}}{{turret}}"
    },

    // Название танка для мертвых танков
    "tankNameAllySystemColor": {
      "name": "Tank name",
      "enabled": true,
      "x": "{{topclan?{{turret?4|-10}}|{{turret?14|0}}}}",
      "y": "{{topclan?-26|{{turret?-26|-18}}}}",
      "alpha": 100,
      "align": "center",
      "textFormat": {
        "font": "$FieldFont",
        "size": 14,
        "color": "#96FF00",
        "bold": true,
        "italic": false
      },
      "shadow": {
        "enabled": true,
        "distance": 0,
        "angle": 0,
        "color": "0x000000",
        "alpha": 90,
        "blur": 10,
        "strength": 28
      },
      "format": "{{topclan}}{{vehicle}}{{turret}}"
    },

    // Название танка для мертвых танков противников
    "tankNameEnemySystemColor": {
      "$ref": { "path":"def.tankNameAllySystemColor" },
      "textFormat": {
        "color": "#F50800"
      }
    },


    // Рейтинг wn8
    "rating": {
      "name": "Rating",
      "enabled": true,
      "x": -15,
      "y": -51,
      "alpha": 100,
      "align": "right",
      "textFormat": {
        "font": "$FieldFont",
        "size": 13,
        "color": "{{c:xr}}",
        "bold": true,
        "italic": false
      },
      "shadow": { "$ref": { "path":"def.textFieldShadow" } },
      "format": "{{r}}"
    },

    // Процент побед
    "winRate": {
      "name": "Win Rate",
      "enabled": true,
      "x": 15,
      "y": -51,
      "alpha": 100,
      "align": "left",
      "textFormat": {
        "font": "$FieldFont",
        "size": 13,
        "color": "{{c:winrate}}",
        "bold": true,
        "italic": false
      },
      "shadow": { "$ref": { "path":"def.textFieldShadow" } },
      "format": "{{winrate%2d~%}}"
    },

    // xte
    "xte": {
      "name": "xte",
      "enabled": true,
      "x": 0,
      "y": -51,
      "alpha": 100,
      "align": "center",
      "textFormat": {
        "font": "$FieldFont",
        "size": 13,
        "color": "{{c:xte}}",
        "bold": true,
        "italic": false
      },
      "shadow": { "$ref": { "path":"def.textFieldShadow" } },
      "format": "({{xte}})"
    },

    // Количесво килобоев
    "kB": {
      "name": "kb",
      "enabled": true,
      "x": -15,
      "y": -51,
      "alpha": 90,
      "align": "right",
      "textFormat": {
        "font": "$TitleFont",
        "size": 12,
        "color": "{{c:kb}}",
        "bold": true,
        "italic": false
      },
      "shadow": { "$ref": { "path":"def.textFieldShadow" } },
      "format": "{{kb%2d~k|--k}}"
    },

    // Всплывающий урон.
    "damageText": {
      "enabled": true,
      "x": "{{c:dmg=#FFB01B?0|55}}",
      "y": "{{c:dmg=#FFB01B?-67|-37}}",
      /* "alpha": "{{c:dmg=#FFB01B?100|0}}", */
      "alpha": 100,
      "align": "center",
      "textFormat": {
        "font": "$TitleFont",
        "size": "{{c:dmg=#FFB01B?22|16}}",
        "color": "{{c:dmg=#FFB01B?0xFFC375|c:dmg}}",
        "bold": false,
        "italic": false
      },
      "shadow": {
        "$ref": { "path":"def.textFieldShadow" },
        "color": "0x{{c:dmg=#FFB01B?730700|000000}}",
        "alpha": 100,
        "blur": "{{c:dmg=#FFB01B?7|2}}",
        "strength": "{{c:dmg=#FFB01B?3|2}}",
        "distance": 0
      },
      "speed": 3,
      "maxRange": "{{c:dmg=#FFB01B?70|0}}",
      "damageMessage": "{{dmg}}",
      "blowupMessage": "{{l10n:blownUp}}\n{{dmg}}"
    },

    // Полоска со шкалой опасности игрока
    "skill": {
      "name": "skill",
      "enabled": true,
      "x": 1,
      "y": -20,
      "alpha": "70",
      "color": "0xFFFF00",
      "align": "center",
      "textFormat": {
        "font": "$FieldFont",
        "size": 13,
        "bold": false,
        "italic": false
      },
      "shadow": {
        // false - без тени
        "enabled": true,
        "distance": 0,
        "angle": 90,
        "color": null,
        "alpha": 30,
        "blur": 6,
        "strength": 2
      },
      "format": "{{xvm-stat?<img src='cfg://dar/img/skill/{{c:xte}}.png' width='82' height='3'>}}"
    },

    // Текстовое поле с оставшимся здоровьем.
    "hp": {
      "name": "Tank HP",
      "enabled": true,
      "x": 0,
      "y": -18,
      "alpha": 100,
      "align": "center",
      "antiAliasType": "normal",
      "textFormat": {
        "font": "$TitleFont",
        "size": 13,
        "color": "0xFFFFFF",
        "bold": false,
        "italic": false
      },
      "shadow": {
        "$ref": { "file": "@xvm.xc", "path": "mainShadow" }
      },
      "format": "{{hp}}"
    },

    // Маркер "Низкого показателя здоровья"
    "lowHP": {
      "x": 0,
      "y": -110,
      "width": 90,
      "height": 90,
      /* "alpha": "{{{{py:mul({{py:sight.damageShell}}, 0.75)}}>{{hp}}?100|0}}", */
      "alpha": "{{alive?{{{{py:mul({{py:sight.damageShell|0}}, 0.75)}}>{{hp}}?100|0}}|0}}",
      "format": "<img src='cfg://dar/img/lowhp{{ally?Ally}}.png'>"
    },

    // Маркер "Низкого показателя здоровья"
    "deadIcon": {
      "x": 0,
      "y": -10,
      "alpha": 100,
      /* "alpha": "{{{{py:mul({{py:sight.damageShell}}, 0.75)}}>{{hp}}?100|0}}", */
      "format": "<img src='cfg://dar/img/markers/dead/{{ally?ally|enemy}}-{{vtype-key}}.png' width='32' height='32'>"
    },

    // Отметки на стволе
    "mark": {
      "name": "mark",
      "enabled": true,
      "x": 30,
      "y": -18,
      "alpha": 100,
      "align": "center",
      "src": "cfg://dar/img/playersPanel/marks/markerMark_{{marksOnGun=1?0|{{marksOnGun}}}}.png"
    },

    // Полоска здоровья
    "healthBarAlly": {
      "enabled": true,
      "x": -40,
      "y": -30,
      "alpha": 100,
      "color": null,
      "color": "#{{tk?06C0CC|{{squad?A77C45|6DA800}}}}",
      "lcolor": null,
      "width": 80,
      "height": 13,
      "border": {
        "alpha": 80,
        "color": "0x000000",
        "size": 1
      },
      "fill": {
        "alpha": 90
      },
      "damage": {
        "alpha": 100,
        "color": "{{c:dmg}}",
        "fade": 1
      },
      "shadow": ${ "def.textFieldShadow" }
    },

    "healthBarEnemy": {
      "$ref": { "path": "def.healthBarAlly" },
      "color": "#A50200"
    },

    "healthBorder": {
      "enabled": false,
      "x": -40,
      "y": -30,
      "alpha": 100,
      "color": null,
      "lcolor": null,
      "width": 80,
      "height": 15,
      "bgColor": "{{c:xr|#999999}}",
      "border": {
        "alpha": 80,
        "color": "0x000000",
        "size": 5
      }
    },

    // Время перезарядки союзника
    "reloadVehicleAlly": {
      "name": "reload Vehicle",
      "x": -40,                 //  положение по оси X
      "y": -17,                  //  положение по оси Y
      "alpha": 100,
      "align": "left",
      "antiAliasType": "normal",
      "textFormat": {
        "font": "$TitleFont",   //  название
        "size": 11,             //  размер
        "color": "0xffffff",
        "bold": false,           //  обычный (false) или жирный (true)
        "italic": false         //  обычный (false) или курсив (true)
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{py:reloadVehicle('{{name}}')%.1f}}"
    },

    // Время перезарядки противника
    "reloadVehicleEnemy": {
      "name": "reload Vehicle",
      "x": -40,                 //  положение по оси X
      "y": -17,                  //  положение по оси Y
      "alpha": 100,
      "align": "left",
      "antiAliasType": "normal",
      "textFormat": {
        "font": "$TitleFont",   //  название
        "size": 11,             //  размер
        "color": "0x{{py:reloadVehicle('{{name}}')>{{py:sight.reloadTime}}?FF6F6A|FFFFFF}}",
        "bold": false,           //  обычный (false) или жирный (true)
        "italic": false         //  обычный (false) или курсив (true)
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{py:reloadVehicle('{{name}}')%.1f}}"
    },


    // Дамаг противника
    "damageVehicle": {
      "name": "damage Vehicle",
      "enabled": true,                    //  false - не отображать
      "x": 40,                           //  положение по оси X
      "y": -17,                            //  положение по оси Y
      "alpha": 100,
      "align": "right",                    //  выравнивание текста
      "textFormat": {
          "font": "$TitleFont",           //  название
          "size": 11,                     //  размер
          /* "color": "0xFFB01B",            //  цвет */
          "color": "0xFF6F6A",
          "bold": false,                   //  обычный (false) или жирный (true)
          "italic": false                 //  обычный (false) или курсив (true)
      },
      /* "shadow": { "$ref": { "path":"def.textFieldShadow" } }, */
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{py:shellDamage('{{name}}')}}"
    },


    // Бронепробитие и дамаг противника
    "damagePenetrationVehicle": {
      "name": "damage PenetrationVehicle",
      "enabled": true,                    //  false - не отображать
      "x": 40,                           //  положение по оси X
      "y": -17,                            //  положение по оси Y
      "alpha": 100,                       //  прозрачность
      "align": "right",                    //  выравнивание текста
      "textFormat": {
        "font": "$TitleFont",           //  название
        "size": 12,                     //  размер
        /* "color": "0xFFB01B",            //  цвет */
        "color": "0xFFFFFF",    //  цвет
        "bold": false,                   //  обычный (false) или жирный (true)
        "italic": false                 //  обычный (false) или курсив (true)
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{py:piercingPower('{{name}}')}} / {{py:shellDamage('{{name}}')}}"
    },

    // Взвод иконка
    "squad": {
      "x": 48,
      "y": -18,
      "src": "cfg://dar/img/squad/{{squad-num?{{squad-num}}}}.png"
    },

    // Текстовое поле с маркером события XMQP
    "xmqpEvent": {
      "x": 0,
      "y": "{{battletype?-70|{{squad?-70|-56}}}}",
      "width": 40,
      "height": 40,
      "alpha": "{{x-spotted?|50}}",
      "align": "center",
      "hotKeyCode": 56,
      "onHold": true,
      "visibleOnHotKey": false,
      "shadow": {
        "color":  "{{x-spotted?#FFBB00}}",
        "blur":   "{{x-spotted?20}}",
        "strength": "{{x-spotted?3}}"
      },
      "textFormat": {
        "font":   "xvm",
        "color":  "{{x-spotted?#FFE08A}}",
        "size":   "24",
        "align":  "center"
      },
      "format": "{{x-spotted?&#x70;|{{x-sense-on?&#x70;}}}}"
    },

    // Маркеры "Нужна помощь" и "Атакую"
    "actionMarker": {
      "enabled": true,
      "x": 0,
      "y": "{{squad?-81|-67}}",
      "y": -83,
      "alpha": 100
    },

    // Маркер оглушения
    "stunMarker": {
      "x": "{{x-sense-on?-22|0}}",
      "y": "{{topclan?{{squad?-82|-68}}|{{squad?-74|-60}}}}"
    },


    // Рейтинг xte на конкретном танке
    "ratingTank": {
      "name": "Rating",
      "enabled": true,
      "x": 0,
      "y": -36,
      "alpha": 100,
      "align": "center",
      "textFormat": {
        "font": "$FieldFont",
        "size": 13,
        "color": "{{c:xte}}",
        "bold": true,
        "italic": false
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{xte}}"
    },

    // Процент побед на конкретном танке
    "winRateTank": {
      "name": "Win Rate",
      "enabled": true,
      "x": -44,
      "y": -36,
      "alpha": 100,
      "align": "left",
      "textFormat": {
        "font": "$FieldFont",
        "size": 13,
        "color": "{{c:t-winrate}}",
        "bold": true,
        "italic": false
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{t-winrate%2d~%}}"
    },

    // Количесво килобоев
    "kBTank": {
      "name": "kb",
      "enabled": true,
      "x": 44,
      "y": -36,
      "alpha": 100,
      "align": "right",
      "textFormat": {
        "font": "$FieldFont",
        "size": 13,
        "color": "{{c:t-battles}}",
        "bold": true,
        "italic": false
      },
      "shadow": ${ "@xvm.xc": "mainShadow" },
      "format": "{{t-battles|--}}"
    },








    "damageTextAliveAlly": {
            "x": 0,
            "y": -100,
            "speed": 5,
            "maxRange": 80,
            "shadow": {
              "alpha": 60,
              "angle": 90,
              "blur": 5,
              "color": "0x75d700",
              "distance": 0,
              "strength": 3
            },
            "textFormat": {
              "color": "0xDDFF99",
              "leading": -3,
              "size": 14
            },
            "damageMessage": "<font face='xvm' size='32'>{{c:dmg-kind=#f4efe4?<font color='#FFB01B'>\u0051</font>| }}</font>\n<font size='22' color='#ffffff'><b>{{dmg}}</b></font>\n<font size='19' face='xvm'>{{vtype}}</font> <b>{{vehicle}}</b>"

            /* "damageMessage": "<font size='22' color='{{c:system}}'><b>{{dmg}}</b>\n<font face='xvm'>{{vtype}}</font> <b>{{vehicle}}</b></font>" */
    },

    "damageTextAliveEnemy": {
            "$ref": { "path": "def.damageTextAliveAlly" },
            "shadow": {
              "alpha": 60,
              "blur": 5,
              "color": "0xec0000",
              "strength": 3
            },
            "textFormat": {
              "color": "0xF5CD90",
              "leading": -3,
              "size": 14
            }
    },




    "damageTextPlayerAlive": {
            "x": 145,
            "y": -70,
            "speed": 3,
            "maxRange": "{{c:dmg-kind=#f4efe2?0|90}}",
            "shadow": {
              "alpha": 90,
              "angle": 45,
              "blur": 5,
              "strength": 3,
              "color": "0xb90000"
            },
            "textFormat": {
              "align": "left",
              "bold": true,
              "color": "{{c:dmg}}",
              "font": "mono",
              "size": 22
            },
            "damageMessage": "<font face='xvm' size='32'>{{c:dmg-kind=#f4efe4?<font color='#FFB01B'>\u0051</font>| }}</font>\n<font size='22'><b>{{dmg}}</b></font>"
    },




    "damageTextDeadAlly": {
            "x": 0,
            "y": -80,
            "speed": 5,
            "maxRange": 80,
            "shadow": {
              "angle": 45,
              "blur": 5,
              "color": "0xddff99",
              "strength": 3
            },
            "textFormat": {
              "color": "0x152008",
              "font": "$FieldFont",
              "leading": -3,
              "size": 14
            },
            "damageMessage": "<font face='xvm' size='32'>v</font>\n<font size='22'><b>{{dmg}}</b></font>\n<font size='19' face='xvm'>{{vtype}}</font> <b>{{vehicle}}</b>",

            "blowupMessage": "<font face='xvm' size='32'>  |</font>\n<font size='22'><b>{{dmg}}</b></font>\n<font size='19' face='xvm'>{{vtype}}</font> <b>{{vehicle}}</b>"
    },

    "damageTextDeadEnemy": {
            "$ref": { "path": "def.damageTextDeadAlly" },
            "shadow": {
              "angle": 45,
              "blur": 5,
              "color": "0xff7c7c",
              "strength": 3
            },
            "textFormat": {
              "color": "0x260900",
              "font": "$FieldFont",
              "leading": -3,
              "size": 14
            }
    },

    "damageTextPlayerDead": {
            "x": 0,
            "y": -105,
            "speed": 3,
            "maxRange": 30,
            "maxRange": 0,
            "shadow": {
              "alpha": 90,
              "angle": 45,
              "blur": 5,
              "strength": 3,
              "color": "0xb90000"
            },
            "textFormat": {
              "color": "{{c:dmg}}",
              "font": "$FieldFont",
              "leading": -3,
              "size": 14 },

            "damageMessage": "<font face='xvm' size='32'>v</font>\n<font size='22'><b>{{dmg}}</b></font>\n<font size='19' face='xvm'>{{vtype}}</font> <b>{{vehicle}}</b>",

            "blowupMessage": "<font face='xvm' size='32'>  |</font>\n<font size='22'><b>{{dmg}}</b></font>\n<font size='19' face='xvm'>{{vtype}}</font> <b>{{vehicle}}</b>"
    }
  }
}