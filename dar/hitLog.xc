﻿/**
 * Лог попаданий (счетчик своих попаданий)
 * Конфиг: "dar"
 */

/* Macros used in hitLog:
  Макросы используемые в hitLog:
   
    {{dmg}}                - последний нанесенный урон.
    {{dmg-kind}}           - тип нанесенного урона (атака, пожар, таран, ...).
    {{c:dmg-kind}}         - цвет по типу урона.
    {{type-shell}}         - тип снаряда.
    {{c:type-shell}}       - цвет по типу снаряда.
    {{vtype}}              - тип техники.
    {{c:vtype}}            - цвет по типу техники.
    {{team-dmg}}           - командная принадлежность цели (союзник, противник, урон по себе).
    {{c:team-dmg}}         - цвет по командной принадлежности цели (союзник, противник, урон по себе).
    {{costShell}}          - валюта снаряда (золото, кредиты).
    {{c:costShell}}        - цвет по валюте снаряда.
    {{vehicle}}            - название техники цели (для огневых точек название берется из файлов локализации, параметр "pillbox").
    {{name}}               - никнейм цели.
    {{comp-name}}          - часть техники, в которую было попадание (башня, корпус, ходовая, орудие).
    {{clan}}               - название клана в скобках (пусто, если игрок не в клане).
    {{level}}              - уровень техники.
    {{clannb}}             - название клана без скобок.
    {{clanicon}}           - макрос со значением пути эмблемы клана.
    {{squad-num}}          - номер взвода (1,2,...), пусто - если игрок не во взводе.
    {{dmg-ratio}}          - последний нанесенный урон в процентах.
    {{splash-hit}}         - возвращает 'splash', если урон нанесен осколками снаряда (ОФ/ХФ), иначе пусто.
    {{critical-hit}}       - возвращает 'crit', если было нанесено критическое повреждение, иначе пусто.    
    {{alive}}              - возвращает 'al', если техника после атаки не разрушена, пусто для разрушенной.
    {{wn8}}, {{xwn8}}, {{wtr}}, {{xwtr}}, {{eff}}, {{xeff}}, {{wgr}}, {{xwgr}}, {{xte}}, {{r}}, {{xr}} - макросы статистики (смотрите macros_ru.txt).
    {{c:wn8}}, {{c:xwn8}}, {{c:wtr}}, {{c:xwtr}}, {{c:eff}}, {{c:xeff}}, {{c:wgr}}, {{c:xwgr}}, {{c:xte}}, {{c:r}}, {{c:xr}} - цвет по соответствующему макросу статистики (смотрите macros_ru.txt).
    {{diff-masses}}        - разность масс техники при столкновении.
    {{nation}}             - нация техники.
    {{blownup}}            - возвращает 'blownup', если взорван боекомплект цели, иначе ''.
    {{type-shell-key}}     - название ключа таблицы типа снаряда.
    {{n-player}}           - число повреждений по каждому игроку.
    {{dmg-player}}         - суммарный урон по цели.
    {{dmg-ratio-player}}   - суммарный урон по цели в процентах.
    {{c:dmg-ratio-player}} - цвет по суммарному урону по цели (задается в colors.xc).
    {{dmg-kind-player}}    - все типы нанесенного урона по цели (атака, пожар, таран, ...).
    {{dmg-deviation}}      - TODO / отклонение нанесенного урона от номинального урона снаряда в процентах. Возвращает 0.0, если техника была уничтожена выстрелом, или выстрел был фугасом и отклонение составило больше 25%.
    {{vehiclename}}        - название техники в системе (usa-A34_M24_Chaffee).
*/ 

{
  "hitLog": {
    // true - отображать урон по союзникам.
    "showAllyDamage": true,
    // true - отображать урон по себе.
    "showSelfDamage": false,
    // Лог нанесенного урона.
    "log": {
      "addToEnd": false,
      "groupHitsByPlayer": true,
      "lines": 15,
      "moveInBattle": false,
      "dmg-kind": {
        "shot": "{{type-shell}}\t",
        "fire": "{{l10n:fire}}\t{{l10n:n/a}}\t",
        "ramming": "{{l10n:ramming}}\t{{l10n:n/a}}\t",
        "world_collision": "{{l10n:strike}}\t{{l10n:n/a}}\t",
        "overturn": "{{l10n:overturn}}\t{{l10n:n/a}}\t",
        "drowning": "{{l10n:drowning}}\t{{l10n:n/a}}\t",
        "art_attack": "{{l10n:Artattack}}\t{{l10n:n/a}}\t<font alpha='#E6' face='xvm' size='19'>&#x110;</font>",
        "air_strike": "{{l10n:Airstrike}}\t{{l10n:n/a}}\t<font alpha='#E6' face='xvm' size='19'>&#x111;</font>"
      },
      "c:dmg-kind": {
        "shot": "{{c:type-shell}}",
        "fire": "fire",
        "ramming": "ramming",
        "world_collision": "strike",
        "drowning": "drowning",
        "overturn": "overturn",
        "art_attack": "{{c:type-shell}}",
        "air_strike": "{{c:type-shell}}"
      },
      "vtype": {
        "HT": "&#x3F;",
        "MT": "&#x3B;",
        "LT": "&#x3A;",
        "TD": "&#x2E;",
        "SPG": "&#x2D;",
        "not_vehicle": ""
      },
      "c:team-dmg": {
        "ally-dmg": "#00EAFF",
        "enemy-dmg": "{{alive?#FFFFFF|#FF5757}}",
        "unknown": "{{alive?#FFFFFF|#FF5757}}"
      },
      "type-shell": {
        "armor_piercing": "{{l10n:{{blownup|pierced}}}}\t{{l10n:AP}}",
        "high_explosive": "{{l10n:{{splash-hit?close|{{blownup|debris}}}}}}\t{{l10n:HE}}",
        "armor_piercing_cr": "{{l10n:{{blownup|pierced}}}}\t{{l10n:APCR}}",
        "armor_piercing_he": "{{l10n:{{splash-hit?close|{{blownup|debris}}}}}}\t{{l10n:HESH}}",
        "hollow_charge": "{{l10n:{{blownup|pierced}}}}\t{{l10n:HEAT}}",
        "not_shell": ""
      },
      "c:type-shell": {
        "armor_piercing": "{{blownup|pierced}}",
        "high_explosive": "{{splash-hit|{{blownup|debris}}}}",
        "armor_piercing_cr": "{{blownup|pierced}}",
        "armor_piercing_he": "{{splash-hit|{{blownup|debris}}}}",
        "hollow_charge": "{{blownup|pierced}}",
        "not_shell": ""
      },
      "formatHistory": "<textformat leading='-2' tabstops='[23,55]'>&#xD7;{{n-player}}:\t{{dmg-player%-4d}}\t<font size='17' face='xvm'>{{vtype}}</font> {{vehicle}}<font face='xvm' size='17' color='#FF5757'>{{alive? |&#x77;}}</font></textformat>"
      /* "formatHistory": "<font color='{{c:team-dmg}}'><textformat tabstops='[26,58,144,168,187]'>&#xD7;{{n-player}}:\t-{{dmg-player%-4d}}\t<img src='cfg://dar/img/log/{{c:team-dmg}}/{{c:dmg-kind}}.png' width='27' height='27' vspace='-9'>{{dmg-kind}}<font alpha='#E6' face='xvm' size='21'>{{vtype}}</font>\t{{vehicle}}</textformat></font>" */
    },
    "logAlt": {
      "$ref": { "path": "hitLog.log" }
    },

    // Первая строчка с общими показателями
    "logBackground": {
      "$ref": { "path": "hitLog.log" },
      "lines": 1,
      "c:team-dmg": {
        "ally-dmg": "#00EAFF",
        "enemy-dmg": "#{{alive?FFFFFF|FFB01B}}",
        "unknown": "#{{alive?FFFFFF|FFB01B}}"
      },
      "formatHistory": "<textformat tabstops='[23,55]'><img src='cfg://dar/img/log/damage.png' width='16' height='16' vspace='-2'>\t<font color='{{py:xvm.totalDamageColor}}'>{{py:xvm.totalDamage}} ({{py:xvm.total_hp.avgDamage('',{{py:xvm.totalDamage}})}})</font></textformat>"
    },
    "logAltBackground": {
      "$ref": { "path": "hitLog.logBackground" }
    }
  }
}