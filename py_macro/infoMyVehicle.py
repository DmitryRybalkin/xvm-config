from Vehicle import Vehicle

from xfw import *
from xvm_main.python.logger import *


isWheeledTech = False
hasSiegeMode = False


@registerEvent(Vehicle, 'onEnterWorld')
def Vehicle_onEnterWorld(self, prereqs):
    global isWheeledTech, hasSiegeMode
    if self.isPlayerVehicle:
        isWheeledTech = self.isWheeledTech
        hasSiegeMode = self.typeDescriptor.hasSiegeMode and not isWheeledTech


@xvm.export('isWheeledTech', deterministic=False)
def export_isWheeledTech():
    return 'wheel' if isWheeledTech else None


@xvm.export('hasSiegeMode', deterministic=False)
def export_hasSiegeMode():
    return 'siege' if hasSiegeMode else None
