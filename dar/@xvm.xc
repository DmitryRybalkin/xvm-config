/**
 * Главный конфигурационный файл
 * Конфиг: "dar"
 */
{
  "autoReloadConfig": true,
  "language": "dar",
  "definition": {
    "author": "dar",
    "date": "16.05.2019",
    "gameVersion": "1.5.0.2",
    "modMinVersion": "7.9.2"
  },

  // Основная тень
  "mainShadow": {
    "alpha": 35,
    "angle": 90,
    "blur": 1,
    "color": "0x000000",
    "distance": 1,
    "strength": 1.5
  },

  // Светлая тень
  "lightShadow": {
    "alpha": 30,
    "angle": 90,
    "blur": 3,
    "color": "{{py:sight.goldShell?0xFFA070|0xB7FF71}}",
    "distance": 0,
    "strength": 1.5
  },

  "options": {
    "fragCorShowMarkers": false        // true - показывать маркеры техники на панели счёта
  },
  "module-off": {
    "fragCorrelation": true,            // false - отключить панель счёта, если используется альтернативная панель
    "lastHit": true,                    // false - отключить сообщение о полученном попадании
    "markers": true,                    // false - отключить маркеры над танком
    "minimap": true,                    // false - отключить миникарту
    "playersPanel": true                // false - отключить панели игроков (уши)
  },
  "alpha":             ${ "alpha.xc":             "alpha" },
  "battle":            ${ "battle.xc":            "battle" },
  "battleLabels":      ${ "battleLabels.xc":      "labels" },
  "battleLoading":     ${ "battleLoading.xc":     "battleLoading" },
  "battleEfficiency":  ${ "battleEfficiency.xc":  "battleEfficiency" },
  "battleLoading":     ${ "battleLoading.xc":     "battleLoading" },
  "battleLoadingTips": ${ "battleLoadingTips.xc": "battleLoadingTips" },
  "battleMessages":    ${ "battleMessages.xc":    "battleMessages" },
  "battleResults":     ${ "battleResults.xc":     "battleResults"},
  "boosters":          ${ "boosters.xc":          "boosters"},
  "captureBar":        ${ "captureBar.xc":        "captureBar" },
  "colors":            ${ "colors.xc":            "colors" },
  "damageLog":         ${ "damageLog.xc":         "damageLog" },
  "expertPanel":       ${ "battle.xc":            "battle.expertPanel" },
  "fragCorrelation":   ${ "fragCorrelation.xc":   "fragCorrelation" },
  "hangar":            ${ "hangar.xc":            "hangar" },
  "hitLog":            ${ "hitLog.xc":            "hitLog" },
  "hotkeys":           ${ "hotkeys.xc":           "hotkeys" },
  "iconset":           ${ "iconset.xc":           "iconset" },
  "infoPanel":         ${ "infoPanel.xc":         "infoPanel" },
  "login":             ${ "login.xc":             "login" },
  "minimap":           ${ "minimap.xc":           "minimap" },
  "minimapAlt":        ${ "minimapAlt.xc":        "minimap" },
  "markers":           ${ "markers.xc":           "markers" },
  "playersPanel":      ${ "playersPanel.xc":      "playersPanel" },
  "safeShot":          ${ "safeShot.xc":          "safeShot" },
  "sight":             ${ "sight.xc":             "sight" },
  "sounds":            ${ "sounds.xc":            "sounds" },
  "statisticForm":     ${ "statisticForm.xc":     "statisticForm" },
  "assistLog":         ${ "assistLog.xc":         "assistLog" },
  "texts":             ${ "texts.xc":             "texts" }
}